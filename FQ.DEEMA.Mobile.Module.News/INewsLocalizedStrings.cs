﻿namespace FQ.DEEMA.Mobile.Module.News
{
    public interface INewsLocalizedStrings
    {
        string Comments { get; }
        string DateFormat { get; }
        string Reply { get; }
        string CommentPlaceholder { get; }
        string Delete { get; }
        string News { get; }
        string Unread { get; }
        string Read { get; }
        string All { get; }
        string NewPost { get; }
        string DateFrom { get; }
        string DateTo { get; }
        string NewPostTitlePlaceholder { get; }
        string NewPostContentPlaceholder { get; }
        string DateFormatPart { get; }
        string Channels { get; }
        string PostErrorMessage { get; }
        string Ok { get; }
        string PostValidationErrorMessage { get; }
        string NoNews { get; }
        string EmptyArticle { get; }
        string Subscribe { get; }
        string Unsubscribe { get; }
        string Edit { get; }
        string MyNews { get; }
        string EnableSocialFeatures { get; }
        string CommentDeletedMessage { get; }
        string Undo { get; }
        string SecondsAgoFormatted { get; }
        string MinutesAgoFormatted { get; }
        string Yesterday { get; }
        string Report { get; }
        string Cancel { get; }
        string ReportSent { get; }
        string PostingCommentErrorTitle { get; }
        string PostingCommentErrorMessage { get; }
    }
}