﻿using Autofac;
using FQ.DEEMA.Mobile.Module.News.Services;
using FQ.DEEMA.Mobile.Module.News.Services.Remote;
using FQ.DEEMA.Mobile.Module.News.ViewModels;
using FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails;

namespace FQ.DEEMA.Mobile.Module.News
{
    public static class Bootstrapper
    {
        public static void Configure(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterType<RemoteNewsService>().As<IRemoteNewsService>().InstancePerDependency();
            containerBuilder.RegisterType<NewsService>().As<INewsService>().InstancePerDependency();
            containerBuilder.RegisterType<MentionService>().As<IMentionService>().SingleInstance();
            containerBuilder.RegisterType<NewsAnalyticsService>().As<INewsAnalyticsService>().InstancePerDependency();
            containerBuilder.RegisterType<VideoPlayerService>().As<IVideoPlayerService>().InstancePerLifetimeScope();
            containerBuilder.RegisterType<ReportService>().As<IReportService>().InstancePerLifetimeScope();

            // ViewModels
            containerBuilder.RegisterType<NewsHeaderViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<NewsViewModel>().As<INewsViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<NewsRootFrameViewModel>().As<INewsRootFrameViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<BodyViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<CommentViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<ReplyViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<NewPostViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<NewsListViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<NewsTabsViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<VideoPlayerViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<NewsChannelsViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<NewsChannelViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<NewsDetailsPageViewModel>().As<INewsDetailsPageViewModel>().InstancePerDependency();
        }
    }
}