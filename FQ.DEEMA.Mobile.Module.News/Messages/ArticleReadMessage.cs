﻿namespace FQ.DEEMA.Mobile.Module.News.Messages
{
    public class ArticleReadMessage
    {
        public ArticleReadMessage(string articleId, bool isRead)
        {
            ArticleId = articleId;
            IsRead = isRead;
        }

        public string ArticleId { get; }

        public bool IsRead { get; }
    }
}
