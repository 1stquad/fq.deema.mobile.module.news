﻿namespace FQ.DEEMA.Mobile.Module.News.Messages
{
    public class DeleteCommentMessage
    {
        public DeleteCommentMessage(int index)
        {
            Index = index;
        }

        public int Index { get; }
    }
}