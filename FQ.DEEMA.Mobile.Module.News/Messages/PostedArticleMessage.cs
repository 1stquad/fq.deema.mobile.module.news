﻿namespace FQ.DEEMA.Mobile.Module.News.Messages
{
    public class PostedArticleMessage
    {
        public PostedArticleMessage(string articleId, string articleTitle)
        {
            ArticleId = articleId;
            ArticleTitle = articleTitle;
        }

        public string ArticleId { get; }

        public string ArticleTitle { get; }
    }
}
