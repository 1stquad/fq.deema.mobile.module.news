﻿using System.Windows.Input;
using Softeq.XToolkit.WhiteLabel.Mvvm;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels
{
    public interface INewsRootFrameViewModel : IViewModelBase
    {
        ICommand RightCommand { get; set; }
        ICommand LeftCommand { get; set; }
    }
}