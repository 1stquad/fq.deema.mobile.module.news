﻿using System.Linq;
using System.Collections.Generic;
using Softeq.XToolkit.WhiteLabel.Interfaces;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using Softeq.XToolkit.Common.Interfaces;
using FQ.DEEMA.Mobile.Module.News.Messages;
using FQ.DEEMA.Mobile.Module.News.Models;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels
{
    public class NewsTabsViewModel : ObservableObject
    {
        private readonly IViewModelFactoryService _viewModelFactoryService;
        private readonly ILogger _logger;
        private readonly Dictionary<NewsType, NewsListViewModel> _newsGroups =
            new Dictionary<NewsType, NewsListViewModel>();

        private NewsType _activeTab;
        private NewsType _initTab;

        public NewsTabsViewModel(
            IViewModelFactoryService viewModelFactoryService,
            ILogManager logManager)
        {
            _viewModelFactoryService = viewModelFactoryService;
            _logger = logManager.GetLogger<NewsTabsViewModel>();
        }

        public NewsListViewModel ActiveTabList
        {
            get
            {
                if (!_newsGroups.ContainsKey(ActiveTab))
                {
                    _logger.Error(new KeyNotFoundException($"Not found tab for: {ActiveTab}"));
                    return _newsGroups[_initTab];
                }
                return _newsGroups[ActiveTab];
            }
        }

        public NewsType ActiveTab
        {
            get => _activeTab;
            set
            {
                if (_activeTab == value)
                {
                    return;
                }

                _activeTab = value;

                if (_activeTab != NewsType.Unknown)
                {
                    if (!ActiveTabList.HasData)
                    {
                        ActiveTabList.LoadDataWithOptions(new NewsOptions { NewsType = _activeTab });
                    }
                }
            }
        }

        public void OnInitialize(NewsType[] newsTabTypes, NewsType initTab)
        {
            foreach (var newsTabType in newsTabTypes)
            {
                var viewModel = _viewModelFactoryService.ResolveViewModel<NewsListViewModel>();
                viewModel.OnInitialize();

                _newsGroups.Add(newsTabType, viewModel);
            }

            ActiveTab = _initTab = initTab;
        }

        public void SilentRefreshActiveTab()
        {
            ActiveTabList.LoadDataWithOptions(ActiveTabList.NewsOptions);
        }

        public void HandleReadArticle(ArticleReadMessage message)
        {
            var article = ActiveTabList.News.FirstOrDefault(x => x.NewsHeader.Id == message.ArticleId);

            if (article == null)
            {
                return;
            }

            if (message.IsRead && ActiveTab == NewsType.Unread)
            {
                ActiveTabList.RemoveArticle(article);
                return;
            }

            if (!message.IsRead && ActiveTab == NewsType.All)
            {
                _newsGroups[NewsType.Unread].AddArticleWithSort(article);
            }
        }
    }
}
