﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using FQ.DEEMA.Mobile.Module.News.Models;
using Softeq.XToolkit.Common.Collections;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Common.Extensions;
using Softeq.XToolkit.WhiteLabel.Interfaces;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using Softeq.XToolkit.WhiteLabel.Navigation;
using Softeq.XToolkit.WhiteLabel.Threading;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels
{
    public class NewsChannelsViewModelParameter
    {
        public Channel Channel { get; set; }
        public Type MainPageViewModelType { get; set; }
    }

    public class NewsChannelsViewModel : ViewModelBase, IViewModelParameter<NewsChannelsViewModelParameter>
    {
        private readonly INewsLocalizedStrings _newsLocalizedStrings;
        private readonly IPageNavigationService _pageNavigationService;

        private Channel _currentNode;
        private ICommand _navigateToMainPageCommand;
        private Channel _root;
        private ICommand _setNodeCommand;
        private string _title;

        public NewsChannelsViewModel(
            INewsLocalizedStrings newsLocalizedStrings,
            IPageNavigationService pageNavigationService)
        {
            _newsLocalizedStrings = newsLocalizedStrings;
            _pageNavigationService = pageNavigationService;
        }

        public ICommand BackCommand { get; private set; }

        public string Title
        {
            get => _title;
            set => Set(ref _title, value);
        }

        public ObservableRangeCollection<ChannelViewModel> Items { get; } =
            new ObservableRangeCollection<ChannelViewModel>();

        public NewsChannelsViewModelParameter Parameter
        {
            get => null;
            set
            {
                _navigateToMainPageCommand = new RelayCommand(() =>
                {
                    _pageNavigationService.NavigateToViewModel(value.MainPageViewModelType, true);
                });
                _root = value.Channel;
                _root.Name = _newsLocalizedStrings.Channels;
                _currentNode = _root;
            }
        }

        public override void OnInitialize()
        {
            base.OnInitialize();

            BackCommand = new RelayCommand(() =>
            {
                if (_currentNode?.ParentChannel == null && _pageNavigationService.CanGoBack)
                {
                    _pageNavigationService.GoBack();
                }
                else
                {
                    SetNode(_currentNode?.ParentChannel);
                }
            });
            _setNodeCommand = new RelayCommand<Channel>(x =>
            {
                //Delay for better UX
                Task.Delay(300).ContinueWith(t => { SetNode(x); });
            });

            Title = null;
            Items.Clear();

            LoadData();
        }

        private void LoadData()
        {
            if (_currentNode == null)
            {
                return;
            }

            DisplayData();
        }

        private void SetNode(Channel channel)
        {
            if (channel == null)
            {
                return;
            }

            _currentNode = channel;
            DisplayData();
        }

        private void DisplayData()
        {
            var models = _currentNode.Channels;
            var viewModels = models.EmptyIfNull()
                .OrderBy(x => x.Name)
                .Select(x => new ChannelViewModel(
                    x,
                    _pageNavigationService,
                    _navigateToMainPageCommand,
                    _setNodeCommand)).ToList();

            Execute.BeginOnUIThread(() =>
            {
                Items.Clear();
                Items.AddRange(viewModels);
                Title = _currentNode.Name;
            });
        }
    }
}