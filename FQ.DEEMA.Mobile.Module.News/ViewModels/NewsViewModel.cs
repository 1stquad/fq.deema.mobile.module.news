﻿using System.Windows.Input;
using Softeq.XToolkit.Auth;
using Softeq.XToolkit.WhiteLabel.Messenger;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using FQ.DEEMA.Mobile.Module.News.Messages;
using FQ.DEEMA.Mobile.Module.News.Models;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels
{
    public class NewsViewModel : ViewModelBase, INewsViewModel
    {
        private readonly string ViewModelRegisterKey = nameof(NewsViewModel);
        private readonly INewsLocalizedStrings _newsLocalizedStrings;
        private readonly NewsTabsViewModel _newsTabsViewModel;
        private readonly IAccountService _accountService;

        public NewsViewModel(
            NewsTabsViewModel newsTabsViewModel,
            INewsLocalizedStrings newsLocalizedStrings,
            IAccountService accountService)
        {
            _newsLocalizedStrings = newsLocalizedStrings;
            _newsTabsViewModel = newsTabsViewModel;
            _accountService = accountService;
        }

        public NewsListViewModel NewsListViewModel => _newsTabsViewModel.ActiveTabList;

        public ICommand LeftCommand { get; private set; }

        public ICommand RightCommand { get; private set; }

        public string UserName => _accountService.UserName;

        public string UserPhotoUrl => _accountService.UserPhotoUrl;

        public string[] Labels => new[]
        {
            _newsLocalizedStrings.Unread,
            _newsLocalizedStrings.All
        };

        public NewsType NewsType
        {
            get => _newsTabsViewModel.ActiveTab;
            set => _newsTabsViewModel.ActiveTab = value;
        }

        public ICommand[] Parameter
        {
            get => null;
            set
            {
                LeftCommand = value[0];
                RightCommand = value[1];
            }
        }

        public override void OnInitialize()
        {
            base.OnInitialize();

            _newsTabsViewModel.OnInitialize(new[] { NewsType.Unread, NewsType.All }, NewsType.Unread);

            Messenger.Default.Unregister<ArticleReadMessage>(ViewModelRegisterKey, OnArticleReadMessage);
            Messenger.Default.Register<ArticleReadMessage>(ViewModelRegisterKey, OnArticleReadMessage);
        }

        public override void OnAppearing()
        {
            base.OnAppearing();

            Messenger.Default.Register<SubscriptionChangedMessage>(this, OnSubscriptionChangedMessage);
        }

        public override void OnDisappearing()
        {
            base.OnDisappearing();

            Messenger.Default.Unregister<SubscriptionChangedMessage>(this);
        }

        private void OnSubscriptionChangedMessage(SubscriptionChangedMessage subscriptionChangedMessage)
        {
            _newsTabsViewModel.SilentRefreshActiveTab();
        }

        private void OnArticleReadMessage(ArticleReadMessage message)
        {
            _newsTabsViewModel.HandleReadArticle(message);
        }
    }
}