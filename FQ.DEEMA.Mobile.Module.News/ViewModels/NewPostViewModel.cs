﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.Services;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Permissions;
using Softeq.XToolkit.WhiteLabel.Interfaces;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using Softeq.XToolkit.WhiteLabel.Navigation;
using Softeq.XToolkit.WhiteLabel.Threading;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels
{
    public class NewPostViewModelParameter
    {
        public string ChannelId { get; set; }
        public NewsArticle NewsArticle { get; set; }
    }

    public class NewPostViewModel : DialogViewModelBase, IViewModelParameter<NewPostViewModelParameter>
    {
        private readonly IDialogsService _dialogsService;
        private readonly INewsService _newsService;

        private bool _socialFeaturesEnabled = true;
        private string _channelId;
        private string _content;
        private NewsArticle _newsArticle;
        private bool _posting;
        private string _title;

        public NewPostViewModel(
            INewsLocalizedStrings newsLocalizedStrings,
            INewsService newsService,
            IDialogsService dialogsService,
            IPermissionsManager permissionsManager)
        {
            NewsLocalizedStrings = newsLocalizedStrings;
            _newsService = newsService;
            _dialogsService = dialogsService;
            PermissionsManager = permissionsManager;
        }

        public IPermissionsManager PermissionsManager { get; }

        public INewsLocalizedStrings NewsLocalizedStrings { get; }

        public ICommand CancelCommand { get; private set; }

        public RelayCommand<Func<(Task<Stream> Stream, string Extension)>> PostCommand { get; private set; }

        public RelayCommand EndEditingCommand { get; set; }

        public string Title
        {
            get => _title;
            set => Set(ref _title, value);
        }

        public string Content
        {
            get => _content;
            set => Set(ref _content, value);
        }

        public bool SocialFeaturesEnabled
        {
            get => _socialFeaturesEnabled;
            set => Set(ref _socialFeaturesEnabled, value);
        }

        public DateTimeOffset DateFrom { get; set; }

        public DateTimeOffset MinDateFrom { get; set; }

        public DateTimeOffset DateEnd { get; set; }

        public DateTimeOffset MinDateEnd { get; set; }

        public bool IsInEditMode { get; private set; }

        public string Header { get; private set; }

        public string ImageUrl { get; set; }

        public NewPostViewModelParameter Parameter
        {
            get => null;
            set
            {
                _channelId = value.ChannelId;
                _newsArticle = value.NewsArticle;
            }
        }

        public NewsHeader Result { get; private set; }

        public override void OnInitialize()
        {
            base.OnInitialize();

            IsInEditMode = _newsArticle != null;

            MinDateFrom = DateTimeOffset.Now;
            DateFrom = MinDateFrom;

            MinDateEnd = MinDateFrom;
            DateEnd = DateTimeOffset.Now.AddMonths(1);

            Header = NewsLocalizedStrings.NewPost;

            if (IsInEditMode)
            {
                _title = _newsArticle.ArticleBody.Title;
                _content = _newsArticle.ArticleBody.Description;

                DateFrom = _newsArticle.ArticleBody.PublishDate < MinDateFrom
                    ? MinDateFrom
                    : new DateTimeOffset(_newsArticle.ArticleBody.PublishDate);

                if (_newsArticle.ArticleBody.ExpirationDate >= DateFrom)
                {
                    DateEnd = new DateTimeOffset(_newsArticle.ArticleBody.ExpirationDate);
                }

                Header = _title;
                ImageUrl = _newsArticle.ArticleBody.ImageUrl;
                SocialFeaturesEnabled = _newsArticle.ArticleBody.IsLikesEnabled || _newsArticle.ArticleBody.IsCommentsEnabled;
            }

            CancelCommand = new RelayCommand(() => { DialogComponent.CloseCommand.Execute(false); });
            PostCommand = new RelayCommand<Func<(Task<Stream> Stream, string Extension)>>(Post);
        }

        private async void Post(Func<(Task<Stream> Stream, string Extension)> getStreamTask)
        {
            if (_posting)
            {
                return;
            }
            _posting = true;

            EndEditingCommand?.Execute(this);

            if (CheckDataValidation())
            {
                await ShowValidationErrorAsync().ConfigureAwait(false);
                _posting = false;
                return;
            }

            IsBusy = true;

            Result = await InternalPostAsync(getStreamTask).ConfigureAwait(false);

            if (Result == null)
            {
                Execute.BeginOnUIThread(() => { IsBusy = false; });

                await ShowPostErrorAsync().ConfigureAwait(false);
                _posting = false;
                return;
            }

            Execute.BeginOnUIThread(() =>
            {
                IsBusy = false;
                _posting = false;
                DialogComponent.CloseCommand.Execute(true);
            });
        }

        private async Task<NewsHeader> InternalPostAsync(Func<(Task<Stream> Stream, string Extension)> getStreamTask)
        {
            var file = getStreamTask();

            using (var stream = await file.Stream.ConfigureAwait(false))
            {
                var model = CreatePostModel(stream, file.Extension);

                var task = IsInEditMode
                    ? _newsService.UpdateArticleAsync(model)
                    : _newsService.PostArticleAsync(model);

                return await task.ConfigureAwait(false);
            }
        }

        private PostArticle CreatePostModel(Stream stream, string extension)
        {
            var model = new PostArticle
            {
                PostImage = stream,
                Content = Content,
                Title = Title,
                DateFrom = DateFrom,
                DateEnd = DateEnd,
                ChannelId = _channelId,
                Extension = extension,
                SocialFeaturesEnabled = SocialFeaturesEnabled
            };

            if (IsInEditMode)
            {
                model.Id = _newsArticle.ArticleId;
                model.ImageUrl = ImageUrl;
            }

            return model;
        }

        private bool CheckDataValidation()
        {
            var isValid = true;

            isValid &= !string.IsNullOrEmpty(Title);
            isValid &= !string.IsNullOrEmpty(Content);

            return !isValid;
        }

        private Task ShowValidationErrorAsync()
        {
            return _dialogsService.ShowDialogAsync(null,
                NewsLocalizedStrings.PostValidationErrorMessage,
                NewsLocalizedStrings.Ok);
        }

        private Task ShowPostErrorAsync()
        {
            return _dialogsService.ShowDialogAsync(null,
                NewsLocalizedStrings.PostErrorMessage,
                NewsLocalizedStrings.Ok);
        }
    }
}