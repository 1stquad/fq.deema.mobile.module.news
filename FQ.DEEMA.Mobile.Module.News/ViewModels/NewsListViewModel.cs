using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.Services;
using FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails;
using Softeq.XToolkit.Common.Collections;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Common.Extensions;
using Softeq.XToolkit.Common.Models;
using Softeq.XToolkit.WhiteLabel.Interfaces;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using Softeq.XToolkit.WhiteLabel.Navigation;
using Softeq.XToolkit.WhiteLabel.Threading;
using Softeq.XToolkit.WhiteLabel.Messenger;
using FQ.DEEMA.Mobile.Module.News.Messages;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels
{
    public class NewsListViewModel : ViewModelWithPagingBase<NewsHeader>
    {
        private readonly IDialogsService _dialogsService;
        private readonly INewsLocalizedStrings _newsLocalizedStrings;
        private readonly ILaunchVideoService _launchVideoService;
        private readonly INewsService _newsService;
        private readonly IPageNavigationService _pageNavigationService;
        private readonly SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1);
        private readonly IViewModelFactoryService _viewModelFactoryService;

        private long _busyEntries;
        private CancellationTokenSource _cancellationTokenSource;
        private bool _canPostNews;
        private bool _hasData;
        private bool _isRefreshing;

        public NewsListViewModel(
            IViewModelFactoryService viewModelFactoryService,
            INewsService newsService,
            IPageNavigationService pageNavigationService,
            INewsLocalizedStrings newsLocalizedStrings,
            IDialogsService dialogsService,
            ILaunchVideoService launchVideoService)
        {
            _viewModelFactoryService = viewModelFactoryService;
            _newsService = newsService;
            _pageNavigationService = pageNavigationService;
            _newsLocalizedStrings = newsLocalizedStrings;
            _launchVideoService = launchVideoService;
            _dialogsService = dialogsService;

            ActionsViewModel = new NewsSharedActionsViewModel(newsService, pageNavigationService);
        }

        public NewsSharedActionsViewModel ActionsViewModel { get; }

        public ObservableRangeCollection<NewsHeaderViewModel> News { get; } =
            new ObservableRangeCollection<NewsHeaderViewModel>();

        public ICommand RefreshCommand { get; private set; }

        public ICommand NewPostCommand { get; private set; }

        public RelayCommand<NewsHeaderViewModel> OpenPlayerCommand { get; private set; }

        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => Set(ref _isRefreshing, value);
        }

        public bool HasData
        {
            get => _hasData;
            set => Set(ref _hasData, value);
        }

        public bool CanPostNews
        {
            get => _canPostNews;
            set => Set(ref _canPostNews, value);
        }

        public string NoNewsText => _newsLocalizedStrings.NoNews;

        public int CurrentItem { get; set; }

        public NewsOptions NewsOptions { get; set; }

        public override void OnInitialize()
        {
            base.OnInitialize();

            RefreshCommand = new RelayCommand(RefreshData);
            NewPostCommand = new RelayCommand(NewPost);
            OpenPlayerCommand = new RelayCommand<NewsHeaderViewModel>(OpenPlayer);
        }

        public void NavigateToDetails(NewsHeaderViewModel newsHeaderViewModel)
        {
            CurrentItem = News.IndexOf(newsHeaderViewModel);

            _pageNavigationService.For<INewsDetailsPageViewModel>()
                .WithParam(x => x.Parameter, newsHeaderViewModel.NewsHeader.Id)
                .Navigate();
        }

        public void LoadNextPage()
        {
            LoadNextPageAsync(false, _cancellationTokenSource.Token).SafeTaskWrapper();
        }

        public void LoadDataWithOptions(NewsOptions newsOptions)
        {
            Execute.BeginOnUIThread(() => { IsBusy = true; });

            Task.Run(() =>
            {
                NewsOptions = newsOptions;
                Interlocked.Increment(ref _busyEntries);

                _cancellationTokenSource?.Cancel();
                _cancellationTokenSource?.Dispose();
                _cancellationTokenSource = new CancellationTokenSource();

                LoadFirstPageAsync(_cancellationTokenSource.Token).ContinueWith(t =>
                {
                    if (Interlocked.Decrement(ref _busyEntries) == 0)
                    {
                        Execute.BeginOnUIThread(() => { IsBusy = false; });
                    }
                });
            });
        }

        public void AddArticleWithSort(NewsHeaderViewModel article)
        {
            News.Add(article);
            News.Sort((x, y) => y.NewsHeader.PublishDate.CompareTo(x.NewsHeader.PublishDate));
            CheckIfHasData();
        }

        public void RemoveArticle(NewsHeaderViewModel article)
        {
            News.Remove(article);
            CheckIfHasData();
        }

        protected override async Task LoadFirstPageAsync(CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }

            await _semaphoreSlim.WaitAsync(cancellationToken).ConfigureAwait(false);

            if (cancellationToken.IsCancellationRequested)
            {
                _semaphoreSlim.Release();
                return;
            }

            CurrentPage = 0;

            try
            {
                await LoadNextPageAsync(true, cancellationToken).ConfigureAwait(false);
            }
            finally
            {
                _semaphoreSlim.Release();
            }
        }

        protected override Task<PagingModel<NewsHeader>> GetItems(int page, int perPage)
        {
            return _newsService.GetNewsAsync(page, perPage, NewsOptions);
        }

        protected override Task<bool> AddPage(IList<NewsHeader> data, bool shouldReset)
        {
            if (data == null)
            {
                return Task.FromResult(false);
            }

            var tcs = new TaskCompletionSource<bool>();
            Execute.BeginOnUIThread(() =>
            {
                if (shouldReset)
                {
                    News.ReplaceRange(CreateViewModels(data));
                    CheckIfHasData();
                    tcs.SetResult(true);
                }
                else
                {
                    if (data.Count > 0)
                    {
                        News.AddRange(CreateViewModels(data));
                    }

                    tcs.SetResult(true);
                }
            });
            return tcs.Task;
        }

        private void RefreshData()
        {
            IsRefreshing = true;
            LoadFirstPageAsync(_cancellationTokenSource.Token).ContinueOnUiThread(() => { IsRefreshing = false; });
        }

        private IList<NewsHeaderViewModel> CreateViewModels(IList<NewsHeader> items)
        {
            var viewModels = items
                .Select(CreateNewsHeaderViewModel)
                .ToList();
            return viewModels;
        }

        private NewsHeaderViewModel CreateNewsHeaderViewModel(NewsHeader model)
        {
            return _viewModelFactoryService.ResolveViewModel<NewsHeaderViewModel, NewsHeaderViewModelParameter>(
                new NewsHeaderViewModelParameter
                {
                    NewsHeader = model,
                    NewsLocalizedStrings = _newsLocalizedStrings,
                    OpenPlayerCommand = OpenPlayerCommand,
                    OpenChannelsCommand = ActionsViewModel.OpenChannelsCommand
                });
        }

        private async void NewPost()
        {
            var newPostViewModel = await _dialogsService.ShowForViewModel<NewPostViewModel, NewPostViewModelParameter>(
                new NewPostViewModelParameter
                {
                    ChannelId = NewsOptions.ChannelId,
                    NewsArticle = null
                });

            if (newPostViewModel?.Result is NewsHeader article)
            {
                AddPostedArticle(article);

                Messenger.Default.Send(new PostedArticleMessage(article.Id, article.Title));
            }
        }

        private void OpenPlayer(NewsHeaderViewModel newsHeaderViewModel)
        {
            _launchVideoService.OpenPlayer(newsHeaderViewModel.NewsHeader.VideoUrl);
        }

        private void AddPostedArticle(NewsHeader article)
        {
            var newsHeaderViewModel = CreateNewsHeaderViewModel(article);

            News.Insert(0, newsHeaderViewModel);
            CheckIfHasData();
        }

        private void CheckIfHasData()
        {
            HasData = News.Count > 0;
        }
    }
}