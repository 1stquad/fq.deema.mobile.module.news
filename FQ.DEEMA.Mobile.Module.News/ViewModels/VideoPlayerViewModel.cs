﻿using Softeq.XToolkit.WhiteLabel.Interfaces;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using FQ.DEEMA.Mobile.Module.News.Services;
using FQ.DEEMA.Mobile.Module.News.Models.VideoPlayer;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels
{
    public class VideoPlayerViewModel : DialogViewModelBase, IViewModelParameter<string>
    {
        private readonly IVideoPlayerService _videoPlayerService;

        public VideoPlayerViewModel(IVideoPlayerService videoPlayerService)
        {
            _videoPlayerService = videoPlayerService;
        }

        public string Parameter
        {
            get => null;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    return;
                }

                VideoPlayerModel = _videoPlayerService.TryDetectPlayerForUrl(value);
            }
        }

        public VideoPlayerModel VideoPlayerModel { get; private set; }
    }
}
