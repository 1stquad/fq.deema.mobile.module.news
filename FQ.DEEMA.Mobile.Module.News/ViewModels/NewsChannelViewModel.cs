using System.Windows.Input;
using FQ.DEEMA.Mobile.Module.News.Messages;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.Services;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.WhiteLabel.Interfaces;
using Softeq.XToolkit.WhiteLabel.Messenger;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using Softeq.XToolkit.WhiteLabel.Threading;
using Softeq.XToolkit.WhiteLabel.Navigation;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels
{
    public class NewsChannelViewModel : ViewModelBase, IViewModelParameter<Channel>
    {
        private readonly INewsService _newsService;
        private readonly IPageNavigationService _pageNavigationService;
        private readonly INewsLocalizedStrings _newsLocalizedStrings;

        private ChannelViewModel _channelViewModel;

        public NewsChannelViewModel(
            INewsService newsService,
            NewsListViewModel newsListViewModel,
            IPageNavigationService pageNavigationService,
            INewsLocalizedStrings newsLocalizedStrings)
        {
            NewsListViewModel = newsListViewModel;
            _pageNavigationService = pageNavigationService;
            _newsService = newsService;
            _newsLocalizedStrings = newsLocalizedStrings;
        }

        public NewsListViewModel NewsListViewModel { get; }

        public ICommand BackCommand => _channelViewModel.BackCommand;

        public ICommand SubscribeCommand { get; private set; }

        public string Title => _channelViewModel.Name;

        public string SubscribeActionText =>
            IsSubscribed
                ? _newsLocalizedStrings.Unsubscribe
                : _newsLocalizedStrings.Subscribe;

        public bool IsSubscribed
        {
            get => _channelViewModel.IsSubscribed;
            set
            {
                _channelViewModel.IsSubscribed = value;

                RaisePropertyChanged();
                RaisePropertyChanged(nameof(SubscribeActionText));
            }
        }

        public bool IsUnSubscribeAvailable
        {
            get => _channelViewModel.IsUnSubscribeAvailable;
            set
            {
                if (_channelViewModel.IsUnSubscribeAvailable == value)
                {
                    return;
                }

                _channelViewModel.IsUnSubscribeAvailable = value;
                RaisePropertyChanged();
            }
        }

        public Channel Parameter
        {
            get => null;
            set
            {
                _channelViewModel = new ChannelViewModel(value, _pageNavigationService, new RelayCommand(GoBack), null);
            }
        }

        public override void OnInitialize()
        {
            base.OnInitialize();

            NewsListViewModel.OnInitialize();
            NewsListViewModel.LoadDataWithOptions(new NewsOptions { ChannelId = _channelViewModel.Channel.Id });

            SubscribeCommand = new RelayCommand(TryChangeSubscription);

            _newsService.GetChannelInfoAsync(_channelViewModel.Channel.Id).ContinueOnUiThread(t =>
            {
                if (t == null)
                {
                    return;
                }

                NewsListViewModel.CanPostNews = t.IsChannelWriteAvailable;
                IsSubscribed = t.IsSubscribed;
            });
        }

        private async void TryChangeSubscription()
        {
            IsSubscribed = !IsSubscribed;
            await _newsService.SetSubscriptionAsync(_channelViewModel.Channel.Id, IsSubscribed).ConfigureAwait(false);
            Messenger.Default.Send(new SubscriptionChangedMessage());
        }

        private void GoBack()
        {
            if (_pageNavigationService.CanGoBack)
            {
                _pageNavigationService.GoBack();
            }
        }
    }
}