﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FQ.DEEMA.Mobile.Module.News.Services;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Common.Extensions;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using Softeq.XToolkit.WhiteLabel.Navigation;
using FQ.DEEMA.Mobile.Module.News.Models;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels
{
    public class NewsSharedActionsViewModel : ObservableObject
    {
        private readonly INewsService _newsService;
        private readonly IPageNavigationService _pageNavigationService;

        public NewsSharedActionsViewModel(
            INewsService newsService,
            IPageNavigationService pageNavigationService)
        {
            _newsService = newsService;
            _pageNavigationService = pageNavigationService;

            OpenChannelsCommand = new RelayCommand<IList<ChannelHeader>>(OpenChannels);
        }

        public RelayCommand<IList<ChannelHeader>> OpenChannelsCommand { get; private set; }

        private void OpenChannels(IList<ChannelHeader> channels)
        {
            OpenChannelsAsync(channels).SafeTaskWrapper();
        }

        private async Task OpenChannelsAsync(IList<ChannelHeader> channels)
        {
            // TODO YP: open only chosen channel or single (after discuss design)

            var firstNewsChannel = channels.FirstOrDefault();
            if (firstNewsChannel == null)
            {
                return;
            }

            var channel = await _newsService.FindChannelAsync(firstNewsChannel.Id).ConfigureAwait(false);
            if (channel == null)
            {
                return;
            }

            _pageNavigationService.For<NewsChannelViewModel>()
                .WithParam(x => x.Parameter, channel)
                .Navigate();
        }
    }
}
