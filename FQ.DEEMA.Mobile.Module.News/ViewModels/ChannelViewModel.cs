using System.Windows.Input;
using FQ.DEEMA.Mobile.Module.News.Models;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using Softeq.XToolkit.WhiteLabel.Navigation;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels
{
    public class ChannelViewModel : ObservableObject
    {
        private readonly IPageNavigationService _pageNavigationService;
        private readonly ICommand _setNodeCommand;

        public ChannelViewModel(
            Channel channel,
            IPageNavigationService pageNavigationService,
            ICommand backCommand,
            ICommand setNodeCommand)
        {
            Channel = channel;
            _pageNavigationService = pageNavigationService;
            BackCommand = backCommand;
            _setNodeCommand = setNodeCommand;
        }

        public string Name => Channel.Name;

        public ICommand BackCommand { get; }

        public Channel Channel { get; }

        public bool IsSubscribed
        {
            get => Channel.IsSubscribed;
            set
            {
                Channel.IsSubscribed = value;
                RaisePropertyChanged();
            }
        }

        public bool IsUnSubscribeAvailable
        {
            get => Channel.IsUnSubscribeAvailable;
            set
            {
                Channel.IsUnSubscribeAvailable = value;
                RaisePropertyChanged();
            }
        }

        public void NavigateToChannel()
        {
            _pageNavigationService.For<NewsChannelViewModel>()
                .WithParam(x => x.Parameter, Channel)
                .Navigate();
        }

        public void NavigateToChannels()
        {
            if (Channel.Channels.Count > 0)
            {
                _setNodeCommand.Execute(Channel);
            }
        }
    }
}