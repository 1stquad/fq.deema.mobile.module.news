﻿using System.Windows.Input;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using Softeq.XToolkit.WhiteLabel.Navigation;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels
{
    public class NewsRootFrameViewModel : RootFrameNavigationViewModelBase, INewsRootFrameViewModel
    {
        public NewsRootFrameViewModel(IFrameNavigationService frameNavigationService)
            : base(frameNavigationService)
        {
        }

        public ICommand RightCommand { get; set; }

        public ICommand LeftCommand { get; set; }

        public override void NavigateToFirstPage()
        {
            FrameNavigationService.NavigateToViewModel<INewsViewModel, ICommand[]>(new[] {LeftCommand, RightCommand});
        }
    }
}