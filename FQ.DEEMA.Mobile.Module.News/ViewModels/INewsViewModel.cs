﻿using System.Windows.Input;
using Softeq.XToolkit.WhiteLabel.Interfaces;
using Softeq.XToolkit.WhiteLabel.Mvvm;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels
{
    public interface INewsViewModel : IViewModelBase, IViewModelParameter<ICommand[]>
    {
    }
}