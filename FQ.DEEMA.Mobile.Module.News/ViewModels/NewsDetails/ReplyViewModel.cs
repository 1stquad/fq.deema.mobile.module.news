﻿using System.Collections.Generic;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.Services;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.WhiteLabel.Interfaces;
using Softeq.XToolkit.WhiteLabel.Mvvm;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails
{
    public class ReplyViewModelParameter
    {
        public ArticleCommentReply ArticleCommentReply { get; set; }
        public RelayCommand<IArticleItem> ReplyCommand { get; set; }
        public INewsLocalizedStrings NewsLocalizedStrings { get; set; }
        public RelayCommand<ReplyViewModel> LikeCommand { get; set; }
        public NumberToMetricConverter NumberToMetricConverter { get; set; }
        public IMentionService MentionService { get; set; }
        public ICommand<string> ReportCommand { get; set; }
    }

    public class ReplyViewModel : ObservableObject,
        IViewModelParameter<ReplyViewModelParameter>,
        IArticleItem,
        ICommentViewModel<ArticleCommentReply>
    {
        private IMentionService _mentionService;
        private ICommand<string> _reportCommand;

        public ArticleCommentReply Model { get; private set; }

        public INewsLocalizedStrings NewsLocalizedStrings { get; private set; }

        public RelayCommand<IArticleItem> ReplyCommand { get; private set; }

        public RelayCommand<ReplyViewModel> LikeCommand { get; private set; }

        public LikeViewModel LikeViewModel { get; private set; }

        public IReadOnlyList<CommandAction> Options { get; private set; }

        public string ImageUrl
        {
            get => Model.ImageUrl;
            set
            {
                Model.ImageUrl = value;
                RaisePropertyChanged();
            }
        }

        public string ImageCacheKey { get; set; }

        public ArticleItemType NewsDetailsType => Model.NewsDetailsType;

        public bool CanDelete => Model.CanDelete;

        public bool IsLikedByCurrentUser
        {
            get => LikeViewModel.IsLiked;
            set => LikeViewModel.IsLiked = value;
        }

        public int LikesCount
        {
            get => LikeViewModel.LikeCount;
            set => LikeViewModel.LikeCount = value;
        }

        public IEnumerable<MentionRange> Mentions => _mentionService.FindMentions(Model.Message);

        public ReplyViewModelParameter Parameter
        {
            get => null;
            set
            {
                Model = value.ArticleCommentReply;
                ReplyCommand = value.ReplyCommand;
                LikeCommand = value.LikeCommand;
                LikeViewModel = new LikeViewModel(Model, value.NumberToMetricConverter);
                NewsLocalizedStrings = value.NewsLocalizedStrings;
                _mentionService = value.MentionService;
                _reportCommand = value.ReportCommand;
                Options = new List<CommandAction>
                {
                    new CommandAction
                    {
                        Command = new RelayCommand(() => _reportCommand.Execute(Model.Id)),
                        Title = NewsLocalizedStrings.Report,
                        CommandActionStyle = CommandActionStyle.Destructive
                    }
                };
            }
        }
    }
}