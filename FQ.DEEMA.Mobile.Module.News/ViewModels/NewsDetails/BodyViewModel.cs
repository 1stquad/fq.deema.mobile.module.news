﻿using System.Text;
using System.Windows.Input;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.Services;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Common.Extensions;
using Softeq.XToolkit.WhiteLabel.Interfaces;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using FQ.DEEMA.Mobile.Module.News.Models.VideoPlayer;
using Softeq.XToolkit.WhiteLabel.Navigation;
using System.Collections.Generic;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails
{
    public class BodyViewModelParameter
    {
        public ArticleBody Model { get; set; }
        public INewsLocalizedStrings NewsLocalizedStrings { get; set; }
        public NumberToMetricConverter NumberToMetricConverter { get; set; }
    }

    public class BodyViewModel : ObservableObject, IViewModelParameter<BodyViewModelParameter>, IArticleItem
    {
        private readonly INewsService _newsService;
        private readonly ILauncherService _launcherService;
        private readonly IVideoPlayerService _videoPlayerService;

        public BodyViewModel(
            INewsService newsService,
            ILauncherService launcherService,
            IVideoPlayerService videoPlayerService,
            IPageNavigationService pageNavigationService,
            ILaunchVideoService launchVideoService)
        {
            _newsService = newsService;
            _launcherService = launcherService;
            _videoPlayerService = videoPlayerService;

            ActionsViewModel = new NewsSharedActionsViewModel(newsService, pageNavigationService);

            ReadCommand = new RelayCommand(Read);
            LikeCommand = new RelayCommand(Like);
            NavigateToUrlCommand = new RelayCommand<string>(NavigateToUrl);
            OpenChannelsCommand = new RelayCommand(OpenChannels);
        }

        public ArticleBody Model { get; private set; }

        public INewsLocalizedStrings NewsLocalizedStrings { get; private set; }

        public LikeViewModel LikeViewModel { get; private set; }

        public VideoPlayerModel VideoPlayerModel { get; private set; }

        public NewsSharedActionsViewModel ActionsViewModel { get; }

        public ICommand LikeCommand { get; }

        public ICommand ReadCommand { get; }

        public ICommand OpenChannelsCommand { get; }

        public bool HasImage => !string.IsNullOrEmpty(Model.ImageUrl);

        public bool HasVideo => Model.ArticleType == ArticleType.Video && !string.IsNullOrEmpty(Model.VideoUrl);

        public bool IsReadByUser
        {
            get => Model.IsReadByCurrentUser;
            set
            {
                Model.IsReadByCurrentUser = value;
                RaisePropertyChanged();
            }
        }

        public ArticleItemType NewsDetailsType => Model.NewsDetailsType;

        public bool CanDelete => Model.CanDelete;

        public bool IsLikedByCurrentUser
        {
            get => LikeViewModel.IsLiked;
            set => LikeViewModel.IsLiked = value;
        }

        public int LikesCount
        {
            get => LikeViewModel.LikeCount;
            set => LikeViewModel.LikeCount = value;
        }

        public bool IsLikesEnabled => Model.IsLikesEnabled;

        public bool IsCommentsEnabled => Model.IsCommentsEnabled;

        public string RelatedChannels => Model.ChannelName;

        public BodyViewModelParameter Parameter
        {
            get => null;
            set
            {
                Model = value.Model;
                LikeViewModel = new LikeViewModel(value.Model, value.NumberToMetricConverter);
                NewsLocalizedStrings = value.NewsLocalizedStrings;

                if (HasVideo)
                {
                    VideoPlayerModel = _videoPlayerService.TryDetectPlayerForUrl(value.Model.VideoUrl);
                }
            }
        }

        public string HtmlContent
        {
            get
            {
                var sb = new StringBuilder();
                sb.AppendLine("<html>");

                //head
                sb.AppendLine("<head>");
                sb.AppendLine("<meta charset='utf-8'>");
                sb.AppendLine("<meta name='viewport' content='user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1,width=device-width'>");
                sb.AppendLine("<style>*{font-family: -apple-system, system-ui, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial,sans-serif;} body { margin:0; }</style>");
                sb.AppendLine("</head>");
                sb.AppendLine("<body>");

                sb.AppendLine(Model.Description);

                sb.AppendLine("</body>");
                sb.AppendLine("</html>");

                return sb.ToString();
            }
        }

        public RelayCommand<string> NavigateToUrlCommand { get; private set; }

        private void Read()
        {
            _newsService.ArticleUpdateReadStatusAsync(this).SafeTaskWrapper();
        }

        private void Like()
        {
            LikeViewModel.Toggle();
            _newsService.UpdateArticleLikeValueAsync(Model).SafeTaskWrapper();
        }

        private void NavigateToUrl(string url)
        {
            _launcherService.OpenUrl(url);
        }

        private void OpenChannels()
        {
            var channels = new List<ChannelHeader>
            {
                new ChannelHeader
                {
                    Id = Model.ChannelId,
                    Name = Model.ChannelName
                }
            };

            ActionsViewModel.OpenChannelsCommand.Execute(channels);
        }
    }
}