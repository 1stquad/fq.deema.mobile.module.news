﻿using System;
using FQ.DEEMA.Mobile.Module.News.Models;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.WhiteLabel.Interfaces;
using FQ.DEEMA.Mobile.Module.News.Services;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails
{
    public class NewsViewModelFactory : INewsViewModelFactory
    {
        private readonly RelayCommand<CommentViewModel> _likeCommentCommand;
        private readonly RelayCommand<ReplyViewModel> _likeReplyCommand;
        private readonly ICommand<string> _reportCommentCommand;
        private readonly INewsLocalizedStrings _newsLocalizedStrings;
        private readonly IMentionService _mentionService;
        private readonly NumberToMetricConverter _numberToMetricConverter;
        private readonly RelayCommand<IArticleItem> _replyCommand;
        private readonly IViewModelFactoryService _viewModelFactoryService;

        public NewsViewModelFactory(
            IViewModelFactoryService viewModelFactoryService,
            INewsLocalizedStrings newsLocalizedStrings,
            IMentionService mentionService,
            NumberToMetricConverter numberToMetricConverter,
            RelayCommand<IArticleItem> replyCommand,
            RelayCommand<CommentViewModel> likeCommentCommand,
            RelayCommand<ReplyViewModel> likeReplyCommand,
            ICommand<string> reportCommentCommand)
        {
            _viewModelFactoryService = viewModelFactoryService;
            _newsLocalizedStrings = newsLocalizedStrings;
            _mentionService = mentionService;
            _numberToMetricConverter = numberToMetricConverter;
            _replyCommand = replyCommand;
            _likeCommentCommand = likeCommentCommand;
            _likeReplyCommand = likeReplyCommand;
            _reportCommentCommand = reportCommentCommand;
        }

        public IArticleItem CreateViewModel(IArticleItem newsDetailsItem)
        {
            var type = newsDetailsItem.NewsDetailsType;
            switch (type)
            {
                case ArticleItemType.Body:
                    var newsBodyViewModel =
                        _viewModelFactoryService.ResolveViewModel<BodyViewModel, BodyViewModelParameter>(
                            new BodyViewModelParameter
                            {
                                Model = newsDetailsItem as ArticleBody,
                                NewsLocalizedStrings = _newsLocalizedStrings,
                                NumberToMetricConverter = _numberToMetricConverter
                            });
                    return newsBodyViewModel;
                case ArticleItemType.Comment:
                    var commentViewModel =
                        _viewModelFactoryService.ResolveViewModel<CommentViewModel, CommentViewModelParameter>(
                            new CommentViewModelParameter
                            {
                                ArticleComment = newsDetailsItem as ArticleComment,
                                NewsLocalizedStrings = _newsLocalizedStrings,
                                LikeCommentCommand = _likeCommentCommand,
                                ReplyCommand = _replyCommand,
                                NumberToMetricConverter = _numberToMetricConverter,
                                MentionService = _mentionService,
                                ReportCommand = _reportCommentCommand
                            });
                    return commentViewModel;
                case ArticleItemType.Reply:
                    var replyViewModel =
                        _viewModelFactoryService.ResolveViewModel<ReplyViewModel, ReplyViewModelParameter>(
                            new ReplyViewModelParameter
                            {
                                ArticleCommentReply = newsDetailsItem as ArticleCommentReply,
                                NewsLocalizedStrings = _newsLocalizedStrings,
                                LikeCommand = _likeReplyCommand,
                                ReplyCommand = _replyCommand,
                                NumberToMetricConverter = _numberToMetricConverter,
                                MentionService = _mentionService,
                                ReportCommand = _reportCommentCommand
                            });
                    return replyViewModel;
                default: throw new ArgumentOutOfRangeException();
            }
        }
    }
}