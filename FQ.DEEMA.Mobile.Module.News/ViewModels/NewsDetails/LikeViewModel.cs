﻿using FQ.DEEMA.Mobile.Module.News.Models;
using Softeq.XToolkit.WhiteLabel.Mvvm;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails
{
    public class LikeViewModel : ObservableObject
    {
        private readonly IArticleItem _articleItem;
        private readonly NumberToMetricConverter _numberToMetricConverter;

        public LikeViewModel(IArticleItem articleItem, NumberToMetricConverter numberToMetricConverter)
        {
            _articleItem = articleItem;
            _numberToMetricConverter = numberToMetricConverter;
        }

        public bool IsLiked
        {
            get => _articleItem.IsLikedByCurrentUser;
            set
            {
                _articleItem.IsLikedByCurrentUser = value;
                RaisePropertyChanged();
            }
        }

        public int LikeCount
        {
            get => _articleItem.LikesCount;
            set => _articleItem.LikesCount = value;
        }

        public string LikeCountText => _numberToMetricConverter.ConvertValue(LikeCount);

        public void Toggle()
        {
            LikeCount = IsLiked ? LikeCount - 1 : LikeCount + 1;
            IsLiked = !IsLiked;
        }
    }
}