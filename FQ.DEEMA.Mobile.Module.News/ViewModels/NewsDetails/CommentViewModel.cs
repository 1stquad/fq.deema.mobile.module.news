﻿using FQ.DEEMA.Mobile.Module.News.Models;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.WhiteLabel.Interfaces;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using FQ.DEEMA.Mobile.Module.News.Services;
using System.Collections.Generic;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails
{
    public class CommentViewModelParameter
    {
        public ArticleComment ArticleComment { get; set; }
        public RelayCommand<IArticleItem> ReplyCommand { get; set; }
        public RelayCommand<CommentViewModel> LikeCommentCommand { get; set; }
        public INewsLocalizedStrings NewsLocalizedStrings { get; set; }
        public NumberToMetricConverter NumberToMetricConverter { get; set; }
        public IMentionService MentionService { get; set; }
        public ICommand<string> ReportCommand { get; set; }
    }

    public class CommentViewModel : ObservableObject,
        IViewModelParameter<CommentViewModelParameter>,
        IArticleItem,
        ICommentViewModel<ArticleComment>
    {
        private IMentionService _mentionService;
        private ICommand<string> _reportCommand;

        public ArticleComment Model { get; private set; }

        public INewsLocalizedStrings NewsLocalizedStrings { get; private set; }

        public LikeViewModel LikeViewModel { get; private set; }

        public RelayCommand<IArticleItem> ReplyCommand { get; private set; }

        public RelayCommand<CommentViewModel> LikeCommand { get; private set; }

        public IReadOnlyList<CommandAction> Options { get; private set; }

        public string ImageUrl
        {
            get => Model.ImageUrl;
            set
            {
                Model.ImageUrl = value;
                RaisePropertyChanged();
            }
        }

        public string ImageCacheKey { get; set; }

        public ArticleItemType NewsDetailsType => Model.NewsDetailsType;

        public bool CanDelete => Model.CanDelete && Model.ArticleCommentReplies.Count == 0;

        public bool IsLikedByCurrentUser
        {
            get => LikeViewModel.IsLiked;
            set => LikeViewModel.IsLiked = true;
        }

        public int LikesCount
        {
            get => LikeViewModel.LikeCount;
            set => LikeViewModel.LikeCount = value;
        }

        public IEnumerable<MentionRange> Mentions => _mentionService.FindMentions(Model.Message);

        public CommentViewModelParameter Parameter
        {
            get => null;
            set
            {
                Model = value.ArticleComment;
                ReplyCommand = value.ReplyCommand;
                LikeCommand = value.LikeCommentCommand;
                LikeViewModel = new LikeViewModel(Model, value.NumberToMetricConverter);
                NewsLocalizedStrings = value.NewsLocalizedStrings;
                _mentionService = value.MentionService;
                _reportCommand = value.ReportCommand;
                Options = new List<CommandAction>
                {
                    new CommandAction
                    {
                        Command = new RelayCommand(() => _reportCommand.Execute(Model.Id)),
                        Title = NewsLocalizedStrings.Report,
                        CommandActionStyle = CommandActionStyle.Destructive
                    }
                };
            }
        }
    }
}