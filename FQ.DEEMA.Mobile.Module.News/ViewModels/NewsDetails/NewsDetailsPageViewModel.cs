﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using FQ.DEEMA.Mobile.Module.News.Messages;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.Services;
using Softeq.XToolkit.Auth;
using Softeq.XToolkit.Common.Collections;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Common.Extensions;
using Softeq.XToolkit.Permissions;
using Softeq.XToolkit.WhiteLabel.ImagePicker;
using Softeq.XToolkit.WhiteLabel.Interfaces;
using Softeq.XToolkit.WhiteLabel.Messenger;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using Softeq.XToolkit.WhiteLabel.Navigation;
using Softeq.XToolkit.WhiteLabel.Threading;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails
{
    public class NewsDetailsPageViewModel : ViewModelBase, INewsDetailsPageViewModel
    {
        private readonly IAccountService _accountService;
        private readonly IDialogsService _dialogsService;
        private readonly INewsService _newsService;
        private readonly IReportService _reportService;
        private readonly INewsViewModelFactory _newsViewModelFactory;
        private readonly ITimeTracker _visitEventTimeTracker;
        private readonly IPageNavigationService _pageNavigationService;
        private readonly IUndoManager _undoManager;
        private readonly INewsAnalyticsService _analyticsService;
        private string _articleId;
        private bool _canEdit;
        private string _inputText = string.Empty;
        private bool? _isCommentsEnabled;
        private bool _isRefreshing;
        private NewsArticle _newsArticle;
        private IArticleItem _selectedItem;
        private string _channelName;
        private bool _isArticleEmpty;
        private bool _isInProgress;
        private bool _isArticleLoaded;

        public NewsDetailsPageViewModel(
            IPageNavigationService pageNavigationService,
            IViewModelFactoryService viewModelFactoryService,
            INewsService newsService,
            IReportService reportService,
            IAccountService accountService,
            INewsLocalizedStrings newsLocalizedStrings,
            IDialogsService dialogsService,
            IUndoManager undoManager,
            IPermissionsManager permissionsManager,
            INewsAnalyticsService analyticsService,
            ITimeTrackerFactory timeTrackerFactory,
            IMentionService mentionService)
        {
            _pageNavigationService = pageNavigationService;
            _newsService = newsService;
            _reportService = reportService;
            _accountService = accountService;
            NewsLocalizedStrings = newsLocalizedStrings;
            PermissionsManager = permissionsManager;
            _analyticsService = analyticsService;

            _dialogsService = dialogsService;
            _undoManager = undoManager;

            _newsViewModelFactory = new NewsViewModelFactory(
                viewModelFactoryService,
                newsLocalizedStrings,
                mentionService,
                new NumberToMetricConverter(),
                new RelayCommand<IArticleItem>(SelectPosition),
                new RelayCommand<CommentViewModel>(UpdateCommentLikeValue),
                new RelayCommand<ReplyViewModel>(UpdateReplyLikeValue),
                new RelayCommand<string>(ReportComment));

            _visitEventTimeTracker = timeTrackerFactory.Create();

            Items = new ObservableRangeCollection<IArticleItem>();
        }

        public string ChannelName
        {
            get => _channelName;
            set => Set(ref _channelName, value);
        }

        public ICommand BackCommand { get; private set; }

        public ICommand EditCommand { get; private set; }

        public ICommand ReportArticleCommand { get; private set; }

        public RelayCommand<ImagePickerArgs> AddCommentCommand { get; private set; }

        public RelayCommand<int> ItemAddedCommand { get; set; }

        public RelayCommand<bool> SetEditModeCommand { get; set; }

        public RelayCommand ClearInputCommand { get; set; }

        public ObservableRangeCollection<IArticleItem> Items { get; }

        public IReadOnlyList<CommandAction> MoreActions { get; private set; }

        public string InputText
        {
            get => _inputText;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _selectedItem = null;
                }

                if (_inputText != value)
                {
                    _inputText = value;
                    RaisePropertyChanged();
                }
            }
        }

        public bool? IsCommentsEnabled
        {
            get => _isCommentsEnabled;
            set => Set(ref _isCommentsEnabled, value);
        }

        public INewsLocalizedStrings NewsLocalizedStrings { get; }

        public IPermissionsManager PermissionsManager { get; }

        public ICommand RefreshCommand { get; private set; }

        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => Set(ref _isRefreshing, value);
        }

        public bool CanEdit
        {
            get => _canEdit;
            set => Set(ref _canEdit, value);
        }

        public bool IsArticleEmpty
        {
            get => _isArticleEmpty;
            set => Set(ref _isArticleEmpty, value);
        }

        public bool IsArticleLoaded
        {
            get => _isArticleLoaded;
            set => Set(ref _isArticleLoaded, value);
        }

        public string Parameter
        {
            get => null;
            set => _articleId = value;
        }

        public override void OnInitialize()
        {
            base.OnInitialize();

            Items.Clear();
            InputText = string.Empty;

            BackCommand = new RelayCommand(GoBack);
            EditCommand = new RelayCommand(Edit);
            AddCommentCommand = new RelayCommand<ImagePickerArgs>(AddComment);
            RefreshCommand = new RelayCommand(RefreshPage);
            ReportArticleCommand = new RelayCommand(ReportArticle);

            _visitEventTimeTracker.Reset();

            LoadDataAsync(true).SafeTaskWrapper();
        }

        public override void OnAppearing()
        {
            base.OnAppearing();

            Messenger.Default.Register<DeleteCommentMessage>(this, OnDeleteCommentMessage);

            _visitEventTimeTracker.Start();
        }

        public override void OnDisappearing()
        {
            base.OnDisappearing();

            Messenger.Default.Unregister<DeleteCommentMessage>(this);

            _visitEventTimeTracker.Stop();

            if (_newsArticle != null)
            {
                _analyticsService.TrackArticleVisitTime(_articleId, _visitEventTimeTracker.SummaryTime);

                Messenger.Default.Send(new ArticleReadMessage(
                    _newsArticle.ArticleId,
                    _newsArticle.ArticleBody.IsReadByCurrentUser));
            }
        }

        public void GoBack()
        {
            if (_pageNavigationService.CanGoBack)
            {
                _pageNavigationService.GoBack();
            }
        }

        private async void Edit()
        {
            var parameter = new NewPostViewModelParameter
            {
                ChannelId = null,
                NewsArticle = _newsArticle
            };

            var result = await _dialogsService.ShowForViewModel<NewPostViewModel, NewPostViewModelParameter>(parameter).ConfigureAwait(false);

            if (result != null)
            {
                await LoadDataAsync(false);
            }
        }

        private async void AddComment(ImagePickerArgs pickerArgs)
        {
            if (_isInProgress)
            {
                return;
            }

            _isInProgress = true;

            if (string.IsNullOrEmpty(InputText) && string.IsNullOrEmpty(pickerArgs.Extension))
            {
                _isInProgress = false;
                return;
            }

            var insertedItemPosition = 0;

            IArticleItem postingCommentViewModel;
            Task<bool> postingCommentTask;

            if (_selectedItem != null)
            {
                var reply = new ArticleCommentReply
                {
                    PublisherName = _accountService.UserName,
                    PublisherImageUrl = _accountService.UserPhotoUrl,
                    Message = InputText,
                    DateCreated = DateTime.Now,
                    CanDelete = true,
                    HasImage = !string.IsNullOrEmpty(pickerArgs.Extension)
                };

                var result = FindComment(_newsArticle, _selectedItem);
                insertedItemPosition = result.Index;
                result.ArticleComment.ArticleCommentReplies.Add(reply);
                insertedItemPosition++;
                var replyCommentViewModel = (ReplyViewModel)_newsViewModelFactory.CreateViewModel(reply);
                replyCommentViewModel.ImageCacheKey = pickerArgs.ImageCacheKey;

                Items.Insert(insertedItemPosition, replyCommentViewModel);

                var stream = await pickerArgs.ImageStream().ConfigureAwait(false);

                postingCommentTask = _newsService.ReplyToCommentAsync(
                        replyCommentViewModel, _newsArticle.ArticleId, result.ArticleComment.Id, stream,
                        pickerArgs.Extension);

                postingCommentViewModel = replyCommentViewModel;
            }
            else
            {
                var comment = new ArticleComment
                {
                    PublisherName = _accountService.UserName,
                    PublisherImageUrl = _accountService.UserPhotoUrl,
                    Message = InputText,
                    DateCreated = DateTime.Now,
                    CanDelete = true,
                    HasImage = !string.IsNullOrEmpty(pickerArgs.Extension)
                };

                _newsArticle.ArticleComments.Add(comment);
                var commentViewModel = (CommentViewModel)_newsViewModelFactory.CreateViewModel(comment);
                commentViewModel.ImageCacheKey = pickerArgs.ImageCacheKey;
                Items.Add(commentViewModel);
                insertedItemPosition = Items.Count - 1;

                var stream = await pickerArgs.ImageStream().ConfigureAwait(false);

                postingCommentTask = _newsService.CommentArticleAsync(
                        commentViewModel, _newsArticle.ArticleId, stream,
                        pickerArgs.Extension);

                postingCommentViewModel = commentViewModel;
            }

            Execute.BeginOnUIThread(() =>
            {
                SetEditModeCommand?.Execute(false);
                ItemAddedCommand?.Execute(insertedItemPosition);
                ClearInputCommand?.Execute(this);

                _isInProgress = false;
                _selectedItem = null;
            });

            var commentPostedSuccessfully = await postingCommentTask.ConfigureAwait(false);

            if (commentPostedSuccessfully == false)
            {
                await _dialogsService.ShowDialogAsync(
                    NewsLocalizedStrings.PostingCommentErrorTitle,
                    NewsLocalizedStrings.PostingCommentErrorMessage,
                    NewsLocalizedStrings.Ok);

                Items.Remove(postingCommentViewModel);
            }
        }

        /// <summary>
        /// Find root comment and position for insert <paramref name="articleItem"/>.
        /// </summary>
        /// <param name="newsArticle">Article model.</param>
        /// <param name="articleItem">Inserted comment.</param>
        /// <returns>
        ///     Index - index of the last comment (include root and his replies)
        ///     ArticleComment - root comment
        /// </returns>
        private static (int Index, ArticleComment ArticleComment) FindComment(NewsArticle newsArticle,
            IArticleItem articleItem)
        {
            var insertedItemPosition = 0;
            var articleComment = default(ArticleComment);

            for (var i = 0; i < newsArticle.ArticleComments.Count; i++)
            {
                insertedItemPosition++;

                articleComment = newsArticle.ArticleComments[i];

                insertedItemPosition += articleComment.ArticleCommentReplies.Count;

                if (ReferenceEquals(articleComment, articleItem))
                {
                    return (insertedItemPosition, articleComment);
                }

                for (var j = 0; j < articleComment.ArticleCommentReplies.Count; j++)
                {
                    var replyItem = articleComment.ArticleCommentReplies[j];
                    if (ReferenceEquals(replyItem, articleItem))
                    {
                        return (insertedItemPosition, articleComment);
                    }
                }
            }

            return (insertedItemPosition, articleComment);
        }

        private void SelectPosition(IArticleItem articleItem)
        {
            _selectedItem = articleItem;
            var message = articleItem as MessageBase;
            InputText = $"@{message.PublisherEmail}: ";
            SetEditModeCommand?.Execute(true);
        }

        private async Task LoadDataAsync(bool withProgress)
        {
            if (withProgress)
            {
                IsBusy = true;
                ChannelName = null;
            }

            _newsArticle = await _newsService.GetNewsDetailsAsync(_articleId).ConfigureAwait(false);

            if (_newsArticle == null)
            {
                DoEmptyPage();
                return;
            }

            var bodyViewModel = _newsViewModelFactory.CreateViewModel(_newsArticle.ArticleBody);
            var isCommentsEnabled = _newsArticle.ArticleBody.IsCommentsEnabled;

            Execute.BeginOnUIThread(() =>
            {
                Items.ReplaceRange(new[] { bodyViewModel });
                CanEdit = _newsArticle.ArticleBody.CanEdit;
                IsCommentsEnabled = isCommentsEnabled;
                IsBusy = false;
                ChannelName = _newsArticle.ArticleBody.ChannelName;
                IsArticleEmpty = false;
                IsArticleLoaded = true;

                MoreActions = CreateMoreActionsList();
            });

            if (isCommentsEnabled)
            {
                await _newsService.LoadArticleCommentsAsync(_newsArticle).ConfigureAwait(false);

                var commentViewModels = _newsArticle
                    .ExtractCommentItems()
                    .Select(_newsViewModelFactory.CreateViewModel)
                    .ToList();

                Execute.BeginOnUIThread(() => { Items.AddRange(commentViewModels); });
            }
        }

        private UndoItems DeleteFromViewModels(DeleteCommentMessage deleteCommentMessage)
        {
            var senderItem = Items[deleteCommentMessage.Index];
            var viewModelsToDelete = new List<IArticleItem> { senderItem };

            if (senderItem is CommentViewModel commentViewModel)
            {
                var repliesCount = commentViewModel.Model.ArticleCommentReplies.Count;
                for (var i = 1; i < repliesCount + 1; i++)
                {
                    viewModelsToDelete.Add(Items[deleteCommentMessage.Index + i]);
                }
            }

            Items.RemoveRange(viewModelsToDelete, System.Collections.Specialized.NotifyCollectionChangedAction.Remove);

            return new UndoItems { Items = viewModelsToDelete, StartIndex = deleteCommentMessage.Index };
        }

        private void DeleteFromModel(IArticleItem articleItem)
        {
            var senderItem = articleItem;
            var entityId = default(string);

            if (senderItem is CommentViewModel commentViewModel)
            {
                _newsArticle.ArticleComments.Remove(commentViewModel.Model);
                entityId = commentViewModel.Model.Id;
            }
            else if (senderItem is ReplyViewModel replyViewModel)
            {
                var result = FindComment(_newsArticle, replyViewModel.Model);
                result.ArticleComment.ArticleCommentReplies.Remove(replyViewModel.Model);
                entityId = replyViewModel.Model.Id;
            }

            _newsService.DeleteMessageAsync(_newsArticle.ArticleId, entityId);
        }

        private void OnDeleteCommentMessage(DeleteCommentMessage deleteCommentMessage)
        {
            var undoItems = DeleteFromViewModels(deleteCommentMessage);
            var cts = new CancellationTokenSource();

            _undoManager.AddOperation(
                source =>
                {
                    source.Cancel();
                    Items.InsertRange(undoItems.Items, undoItems.StartIndex);
                },
                token =>
                {
                    if (token.IsCancellationRequested)
                    {
                        return;
                    }

                    DeleteFromModel(undoItems.Items.First());
                },
                NewsLocalizedStrings.CommentDeletedMessage,
                NewsLocalizedStrings.Undo,
                cts);
        }

        private void UpdateCommentLikeValue(CommentViewModel commentViewModel)
        {
            commentViewModel.LikeViewModel.Toggle();
            _newsService.UpdateCommentLikeValueAsync(_newsArticle.ArticleId, commentViewModel.Model).SafeTaskWrapper();
        }

        private void UpdateReplyLikeValue(ReplyViewModel replyViewModel)
        {
            replyViewModel.LikeViewModel.Toggle();

            _newsService.UpdateReplyLikeValueAsync(
                _newsArticle.ArticleId, replyViewModel.Model).SafeTaskWrapper();
        }

        private void RefreshPage()
        {
            IsRefreshing = true;

            LoadDataAsync(false).ContinueOnUiThread(() => { IsRefreshing = false; });
        }

        private void DoEmptyPage()
        {
            Execute.BeginOnUIThread(() =>
            {
                Items.Clear();
                CanEdit = false;
                IsCommentsEnabled = false;
                IsBusy = false;
                ChannelName = NewsLocalizedStrings.News;
                IsArticleEmpty = true;
                IsArticleLoaded = false;
                MoreActions = null;
            });
        }

        private async void ReportArticle()
        {
            await _reportService.ReportArticleAsync(_newsArticle.ArticleId).ConfigureAwait(false);

            await _dialogsService.ShowDialogAsync(
                NewsLocalizedStrings.Report,
                NewsLocalizedStrings.ReportSent,
                NewsLocalizedStrings.Ok).ConfigureAwait(false);
        }

        private async void ReportComment(string commentId)
        {
            if (string.IsNullOrEmpty(commentId))
            {
                return;
            }

            await _reportService.ReportCommentAsync(commentId).ConfigureAwait(false);

            await _dialogsService.ShowDialogAsync(
                NewsLocalizedStrings.Report,
                NewsLocalizedStrings.ReportSent,
                NewsLocalizedStrings.Ok).ConfigureAwait(false);
        }

        private List<CommandAction> CreateMoreActionsList()
        {
            var actions = new List<CommandAction>();

            if (CanEdit)
            {
                actions.Add(new CommandAction
                {
                    Command = EditCommand,
                    Title = NewsLocalizedStrings.Edit
                });
            }

            actions.Add(new CommandAction
            {
                Command = ReportArticleCommand,
                CommandActionStyle = CommandActionStyle.Destructive,
                Title = NewsLocalizedStrings.Report
            });

            return actions;
        }

        private class UndoItems
        {
            public int StartIndex { get; set; }
            public IList<IArticleItem> Items { get; set; }
        }
    }
}