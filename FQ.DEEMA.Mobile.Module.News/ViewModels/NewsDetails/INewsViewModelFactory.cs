﻿using FQ.DEEMA.Mobile.Module.News.Models;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails
{
    public interface INewsViewModelFactory
    {
        IArticleItem CreateViewModel(IArticleItem newsDetailsItem);
    }
}