﻿using Softeq.XToolkit.WhiteLabel.Interfaces;
using Softeq.XToolkit.WhiteLabel.Mvvm;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails
{
    public interface INewsDetailsPageViewModel : IViewModelBase, IViewModelParameter<string>
    {
    }
}