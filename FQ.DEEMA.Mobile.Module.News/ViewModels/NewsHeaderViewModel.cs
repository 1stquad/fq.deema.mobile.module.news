﻿using FQ.DEEMA.Mobile.Module.News.Models;
using Softeq.XToolkit.WhiteLabel.Interfaces;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using System.Windows.Input;
using Softeq.XToolkit.Common.Command;
using System.Linq;
using System.Collections.Generic;

namespace FQ.DEEMA.Mobile.Module.News.ViewModels
{
    public class NewsHeaderViewModelParameter
    {
        public NewsHeader NewsHeader { get; set; }
        public INewsLocalizedStrings NewsLocalizedStrings { get; set; }
        public RelayCommand<NewsHeaderViewModel> OpenPlayerCommand { get; set; }
        public RelayCommand<IList<ChannelHeader>> OpenChannelsCommand { get; set; }
    }

    public class NewsHeaderViewModel : ObservableObject, IViewModelParameter<NewsHeaderViewModelParameter>
    {
        private INewsLocalizedStrings _newsLocalizedStrings;
        private ICommand _openPlayerCommand;
        private RelayCommand<IList<ChannelHeader>> _openChannelsCommand;

        public NewsHeader NewsHeader { get; private set; }

        public ICommand OpenPlayerCommand { get; private set; }

        public ICommand OpenChannelsCommand { get; private set; }

        public bool HasImage => !string.IsNullOrEmpty(NewsHeader.ImageUrl);

        public bool HasVideo => NewsHeader.ArticleType == ArticleType.Video && !string.IsNullOrEmpty(NewsHeader.VideoUrl);

        public string RelatedChannels => NewsHeader.RelatedChannels.FirstOrDefault()?.Name ?? string.Empty;

        public NewsHeaderViewModelParameter Parameter
        {
            get => null;
            set
            {
                NewsHeader = value.NewsHeader;
                _newsLocalizedStrings = value.NewsLocalizedStrings;
                _openChannelsCommand = value.OpenChannelsCommand;
                _openPlayerCommand = value.OpenPlayerCommand;

                OpenPlayerCommand = new RelayCommand(() => _openPlayerCommand.Execute(this));
                OpenChannelsCommand = new RelayCommand(() => _openChannelsCommand.Execute(NewsHeader.RelatedChannels));
            }
        }
    }
}