﻿using System.IO;
using System.Threading.Tasks;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails;
using Softeq.XToolkit.Common.Models;

namespace FQ.DEEMA.Mobile.Module.News.Services
{
    public interface INewsService
    {
        Task<PagingModel<NewsHeader>> GetNewsAsync(int page, int pageSize, NewsOptions options);

        Task<NewsArticle> GetNewsDetailsAsync(string articleId);

        Task ArticleUpdateReadStatusAsync(BodyViewModel bodyViewModel);

        Task UpdateArticleLikeValueAsync(ArticleBody articleBody);

        Task UpdateCommentLikeValueAsync(string articleId, ArticleComment articleComment);

        Task UpdateReplyLikeValueAsync(string articleId, ArticleCommentReply articleCommentReply);

        Task<bool> CommentArticleAsync(CommentViewModel commentViewModel, string articleId, Stream file,
            string fileExtension);

        Task<bool> ReplyToCommentAsync(ReplyViewModel replyViewModel, string articleId, string commentId, Stream file,
            string fileExtension);

        Task DeleteMessageAsync(string articleId, string commentId);

        Task<NewsHeader> PostArticleAsync(PostArticle postArticle);

        Task<NewsHeader> UpdateArticleAsync(PostArticle postArticle);

        Task<Channel> GetChannelsAsync();

        Task<ChannelInfo> GetChannelInfoAsync(string channelId);

        Task<Channel> FindChannelAsync(string channelId);

        Task SetSubscriptionAsync(string channelId, bool isSubscribe);

        Task LoadArticleCommentsAsync(NewsArticle newsArticle);

        void RefreshOnBackgroundAsync();
    }
}