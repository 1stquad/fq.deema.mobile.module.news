﻿using System.Threading.Tasks;
using FQ.DEEMA.Mobile.Module.News.Models.Report;
using FQ.DEEMA.Mobile.Module.News.Services.Remote;

namespace FQ.DEEMA.Mobile.Module.News.Services
{
    public class ReportService : IReportService
    {
        private readonly IRemoteNewsService _remoteNewsService;

        public ReportService(IRemoteNewsService remoteNewsService)
        {
            _remoteNewsService = remoteNewsService;
        }

        public Task ReportArticleAsync(string articleId)
        {
            return _remoteNewsService.ReportAbuseAsync(articleId, ReportEntityType.Article);
        }

        public Task ReportCommentAsync(string commentId)
        {
            return _remoteNewsService.ReportAbuseAsync(commentId, ReportEntityType.Comment);
        }
    }
}
