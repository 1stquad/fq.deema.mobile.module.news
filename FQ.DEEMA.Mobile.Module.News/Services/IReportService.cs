﻿using System.Threading.Tasks;

namespace FQ.DEEMA.Mobile.Module.News.Services
{
    public interface IReportService
    {
        Task ReportArticleAsync(string articleId);

        Task ReportCommentAsync(string commentId);
    }
}
