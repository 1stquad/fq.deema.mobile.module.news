﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using Softeq.XToolkit.Common.Interfaces;
using System;

namespace FQ.DEEMA.Mobile.Module.News.Services
{
    public interface IMentionService
    {
        IEnumerable<MentionRange> FindMentions(string text);
    }

    public class MentionRange
    {
        public int Start { get; set; }
        public int Length { get; set; }
    }

    public class MentionService : IMentionService
    {
        private readonly ILogger _logger;

        public MentionService(ILogManager logManager)
        {
            _logger = logManager.GetLogger<MentionService>();
        }

        public IEnumerable<MentionRange> FindMentions(string text)
        {
            var result = new List<MentionRange>();

            if (string.IsNullOrEmpty(text))
            {
                _logger.Error(new ArgumentNullException(nameof(text), nameof(FindMentions)));

                yield break;
            }

            var matches = Regex.Matches(text, @"\B@(\w|@|\.)+", RegexOptions.IgnorePatternWhitespace);

            foreach (Match mention in matches)
            {
                yield return new MentionRange
                {
                    Start = mention.Index,
                    Length = mention.Value.Length
                };
            }
        }
    }
}
