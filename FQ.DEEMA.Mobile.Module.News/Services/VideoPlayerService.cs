﻿using System.Text;
using System;
using FQ.DEEMA.Mobile.Module.News.Models.VideoPlayer;
using Softeq.XToolkit.Common.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace FQ.DEEMA.Mobile.Module.News.Services
{
    public interface IVideoPlayerService
    {
        VideoPlayerModel TryDetectPlayerForUrl(string videoUrl);
    }

    public class VideoPlayerService : IVideoPlayerService
    {
        private readonly ILogger _logger;
        private readonly IList<IVideoPlayer> _players;

        public VideoPlayerService(ILogManager logManager)
        {
            _logger = logManager.GetLogger<VideoPlayerService>();
            _players = new List<IVideoPlayer>
            {
                new Html5Player(),
                new YoutubePlayer(),
                new MsStreamPlayer(),
                new DefaultPlayer()
            };
        }

        public VideoPlayerModel TryDetectPlayerForUrl(string videoUrl)
        {
            var player = _players.FirstOrDefault(x => x.CanPlay(videoUrl));

            if (player != null)
            {
                return new VideoPlayerModel
                {
                    PlayerType = player.Type,
                    PlayerPageHtml = player.GetPlayerHtml(videoUrl)
                };
            }

            _logger.Error(new NotSupportedException($"Not supported video for link: {videoUrl}"));

            return new VideoPlayerModel
            {
                PlayerType = VideoPlayerType.Unknown,
                PlayerPageHtml = string.Empty
            };
        }
    }

    public class DefaultPlayer : BaseVideoPlayer
    {
        public override bool CanPlay(string videoUrl)
        {
            return true;
        }

        public override string GetPlayerHtml(string videoUrl)
        {
            return GetEmbeddedPlayer(videoUrl);
        }
    }

    public class Html5Player : BaseVideoPlayer
    {
        public override VideoPlayerType Type => VideoPlayerType.Html5;

        public override bool CanPlay(string videoUrl)
        {
            return videoUrl.Contains(".mp4");
        }

        public override string GetPlayerHtml(string videoUrl)
        {
            var html = $"<video src='{videoUrl}' width='100%' height='360' controls='controls' autoplay></video>";
            return GetHtmlPage(html);
        }
    }

    public class YoutubePlayer : BaseVideoPlayer
    {
        public override VideoPlayerType Type => VideoPlayerType.Youtube;

        public override bool CanPlay(string videoUrl)
        {
            return videoUrl.Contains("youtu");
        }

        public override string GetPlayerHtml(string videoUrl)
        {
            const string embedUrl = "https://www.youtube.com/embed/";

            videoUrl = videoUrl
                .Replace("https://youtu.be/", embedUrl)
                .Replace("https://www.youtube.com/watch?v=", embedUrl);

            var options = "allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'";

            return GetEmbeddedPlayer(videoUrl, options);
        }
    }

    public class MsStreamPlayer : BaseVideoPlayer
    {
        public override VideoPlayerType Type => VideoPlayerType.MsStream;

        public override bool CanPlay(string videoUrl)
        {
            return videoUrl.Contains("microsoftstream.com");
        }

        public override string GetPlayerHtml(string videoUrl)
        {
            videoUrl = string.Concat(videoUrl.Replace(".com/video", ".com/embed/video"), "?autoplay=true&showinfo=false");

            return GetEmbeddedPlayer(videoUrl);
        }
    }

    public interface IVideoPlayer
    {
        VideoPlayerType Type { get; }
        bool CanPlay(string videoUrl);
        string GetPlayerHtml(string videoUrl);
    }

    public abstract class BaseVideoPlayer : IVideoPlayer
    {
        public virtual VideoPlayerType Type => VideoPlayerType.Unknown;

        public abstract bool CanPlay(string videoUrl);

        public abstract string GetPlayerHtml(string videoUrl);

        protected string GetEmbeddedPlayer(string videoUrl, string options = "")
        {
            var html = $"<iframe width='100%' height='360' src='{videoUrl}' frameborder='0' {options}></iframe>";

            return GetHtmlPage(html);
        }

        protected string GetHtmlPage(string htmlContent)
        {
            var sb = new StringBuilder();

            sb.Append("<html>");

            //head
            sb.Append("<head>");
            sb.Append("<meta charset='utf-8'>");
            sb.Append("<meta name='viewport' content='user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1,width=device-width'>");
            sb.Append("<style>body { margin:0; background:#000 }</style>");
            sb.Append("</head>");
            sb.Append("<body>");

            sb.Append(htmlContent);

            sb.Append("</body>");
            sb.Append("</html>");

            return sb.ToString();
        }
    }
}
