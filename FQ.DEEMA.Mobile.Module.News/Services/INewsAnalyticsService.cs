﻿using System;

namespace FQ.DEEMA.Mobile.Module.News.Services
{
    public interface INewsAnalyticsService
    {
        void TrackArticleVisitTime(string articleId, TimeSpan time);
    }
}
