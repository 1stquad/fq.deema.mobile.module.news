﻿using FQ.DEEMA.Mobile.Module.News.ViewModels;
using Softeq.XToolkit.WhiteLabel.Model;
using Softeq.XToolkit.WhiteLabel.Navigation;

namespace FQ.DEEMA.Mobile.Module.News.Services
{
    public interface ILaunchVideoService
    {
        void OpenPlayer(string videoUrl);
    }

    public class LaunchVideoService : ILaunchVideoService
    {
        private readonly IDialogsService _dialogsService;

        public LaunchVideoService(IDialogsService dialogsService)
        {
            _dialogsService = dialogsService;
        }

        public virtual void OpenPlayer(string videoUrl)
        {
            _dialogsService.ShowForViewModel<VideoPlayerViewModel, string>(videoUrl,
                new OpenDialogOptions
                {
                    ShouldShowBackgroundOverlay = true
                });
        }
    }
}
