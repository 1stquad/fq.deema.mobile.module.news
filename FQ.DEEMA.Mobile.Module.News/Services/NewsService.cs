﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.Services.Remote;
using FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos;
using FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails;
using Softeq.XToolkit.Auth;
using Softeq.XToolkit.Common.Extensions;
using Softeq.XToolkit.Common.Interfaces;
using Softeq.XToolkit.Common.Models;

namespace FQ.DEEMA.Mobile.Module.News.Services
{
    public class NewsService : INewsService
    {
        private readonly IAccountService _accountService;
        private readonly IRemoteNewsService _remoteNewsService;
        private readonly ILocalCache _localCache;

        private const string ChannelsCacheKey = "news_channels";

        public NewsService(
            IRemoteNewsService remoteNewsService,
            IAccountService accountService,
            ILocalCache localCache)
        {
            _remoteNewsService = remoteNewsService;
            _accountService = accountService;
            _localCache = localCache;
        }

        public Task<PagingModel<NewsHeader>> GetNewsAsync(int page, int pageSize, NewsOptions options)
        {
            return _remoteNewsService.GetNewsAsync(page, pageSize, options);
        }

        public Task<NewsArticle> GetNewsDetailsAsync(string articleId)
        {
            return _remoteNewsService.GetNewsDetailsAsync(articleId);
        }

        public Task ArticleUpdateReadStatusAsync(BodyViewModel bodyViewModel)
        {
            bodyViewModel.IsReadByUser = !bodyViewModel.IsReadByUser;

            return _remoteNewsService.ArticleUpdateReadStatusAsync(bodyViewModel.Model.ArticleId,
                bodyViewModel.IsReadByUser);
        }

        public Task UpdateArticleLikeValueAsync(ArticleBody articleBody)
        {
            return _remoteNewsService.LikeUnlikeArticleAsync(articleBody.ArticleId, articleBody.IsLikedByCurrentUser);
        }

        //TODO: verify why we need articleId if we have commentId
        public Task UpdateCommentLikeValueAsync(string articleId, ArticleComment articleComment)
        {
            return _remoteNewsService.LikeUnlikeCommentAsync(articleId, articleComment.Id,
                articleComment.IsLikedByCurrentUser);
        }

        public Task UpdateReplyLikeValueAsync(string articleId, ArticleCommentReply articleCommentReply)
        {
            return _remoteNewsService.LikeUnlikeCommentAsync(articleId, articleCommentReply.Id,
                articleCommentReply.IsLikedByCurrentUser);
        }

        public async Task<bool> CommentArticleAsync(CommentViewModel commentViewModel, string articleId, Stream file,
            string fileExtension)
        {
            var result = await _remoteNewsService.CommentArticleAsync(
                articleId, commentViewModel.Model.Message, file, fileExtension).ConfigureAwait(false);

            file?.Dispose();

            if (result == null)
            {
                return false;
            }

            commentViewModel.Model.Id = result.Id;
            commentViewModel.ImageUrl = result.ImageUrl;

            return true;
        }

        public async Task<bool> ReplyToCommentAsync(ReplyViewModel replyViewModel, string articleId, string commentId,
            Stream file, string fileExtension)
        {
            var result = await _remoteNewsService.ReplyToCommentAsync(
                articleId, replyViewModel.Model.Message, commentId, file, fileExtension).ConfigureAwait(false);

            file?.Dispose();

            if (result == null)
            {
                return false;
            }

            replyViewModel.Model.Id = result.Id;
            replyViewModel.ImageUrl = result.ImageUrl;

            return true;
        }

        public Task DeleteMessageAsync(string articleId, string commentId)
        {
            return _remoteNewsService.DeleteMessageAsync(articleId, commentId);
        }

        public Task<NewsHeader> PostArticleAsync(PostArticle postArticle)
        {
            return _remoteNewsService.PostArticleAsync(postArticle);
        }

        public Task<NewsHeader> UpdateArticleAsync(PostArticle postArticle)
        {
            return _remoteNewsService.UpdateArticleAsync(postArticle);
        }

        public async Task<Channel> GetChannelsAsync()
        {
            var dtos = await _localCache.Get<IList<ChannelDto>>(ChannelsCacheKey).ConfigureAwait(false);
            if (dtos == null)
            {
                return null;
            }

            var root = new Channel();
            var roots = new List<Channel>();

            foreach (var channelDto in dtos)
            {
                var channel = new Channel
                {
                    Id = channelDto.Id,
                    Name = channelDto.Name,
                    ParentChannel = root,
                    IsSubscribed = channelDto.IsSubscribed,
                    IsUnSubscribeAvailable = channelDto.IsUnSubscribeAvailable
                };
                FillChilds(channel, channelDto.Childs);
                roots.Add(channel);
            }

            root.Channels = roots;

            return root;
        }

        public async Task<Channel> FindChannelAsync(string channelId)
        {
            var channels = await GetChannelsAsync().ConfigureAwait(false);

            return ChannelTreeSearch(channels, channelId);
        }

        public Task<ChannelInfo> GetChannelInfoAsync(string channelId)
        {
            return _remoteNewsService.GetChannelInfoAsync(channelId);
        }

        public Task SetSubscriptionAsync(string channelId, bool isSubscribe)
        {
            return _remoteNewsService.SetSubscriptionAsync(channelId, isSubscribe);
        }

        public async Task LoadArticleCommentsAsync(NewsArticle newsArticle)
        {
            var comments = await _remoteNewsService.GetArticlecommentsAsync(newsArticle.ArticleId)
                .ConfigureAwait(false);
            if (comments == null)
            {
                return;
            }

            newsArticle.ArticleComments.AddRange(comments);

            //comments
            foreach (var comment in newsArticle.ArticleComments)
            {
                comment.CanDelete = comment.PublisherId == _accountService.UserId;

                //replies
                foreach (var reply in comment.ArticleCommentReplies)
                {
                    reply.CanDelete = reply.PublisherId == _accountService.UserId;
                }
            }
        }

        public void RefreshOnBackgroundAsync()
        {
            UpdateCacheAsync().SafeTaskWrapper();
        }

        private async Task UpdateCacheAsync()
        {
            var dtos = await _remoteNewsService.GetChannelsAsync().ConfigureAwait(false);
            if (dtos == null)
            {
                return;
            }

            await _localCache.Add(ChannelsCacheKey, DateTimeOffset.UtcNow, dtos).ConfigureAwait(false);
        }

        private void FillChilds(Channel model, IList<ChannelDto> dtos)
        {
            foreach (var dto in dtos)
            {
                var channel = new Channel
                {
                    Id = dto.Id,
                    Name = dto.Name,
                    ParentChannel = model,
                    IsSubscribed = dto.IsSubscribed,
                    IsUnSubscribeAvailable = dto.IsUnSubscribeAvailable
                };
                FillChilds(channel, dto.Childs);
                model.Channels.Add(channel);
            }
        }

        private Channel ChannelTreeSearch(Channel channelTree, string channelId)
        {
            if (channelTree.Id == channelId)
            {
                return channelTree;
            }

            foreach (var channel in channelTree.Channels)
            {
                var foundChannel = ChannelTreeSearch(channel, channelId);
                if (foundChannel != null)
                {
                    return foundChannel;
                }
            }

            return null;
        }
    }
}