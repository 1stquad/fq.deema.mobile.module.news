﻿using System;
using System.Collections.Generic;
using Softeq.XToolkit.Auth;
using Softeq.XToolkit.WhiteLabel.Interfaces;

namespace FQ.DEEMA.Mobile.Module.News.Services
{
    public class NewsAnalyticsService : INewsAnalyticsService
    {
        private const string ArticleIdKey = "articleId";
        private const string VisitTimeKey = "visitTime";
        private const string UserIdKey = "userId";

        private readonly IAnalyticsService _analyticsService;
        private readonly IAccountService _accountService;

        public NewsAnalyticsService(IAnalyticsService analyticsService, IAccountService accountService)
        {
            _analyticsService = analyticsService;
            _accountService = accountService;
        }

        public void TrackArticleVisitTime(string articleId, TimeSpan time)
        {
            TrackEvent(NewsAnalyticsEvent.ArticleVisitTime, new Dictionary<string, string>
            {
                { ArticleIdKey, articleId },
                { VisitTimeKey, time.ToString() }
            });
        }

        private void TrackEvent(NewsAnalyticsEvent eventType, Dictionary<string, string> properties)
        {
            properties.Add(UserIdKey, _accountService.UserId);

            _analyticsService.TrackEvent(eventType.ToString(), properties);
        }

        enum NewsAnalyticsEvent
        {
            ArticleVisitTime
        }
    }
}
