﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.Models.Report;
using FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos;
using Softeq.XToolkit.Common.Models;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote
{
    public interface IRemoteNewsService
    {
        Task<PagingModel<NewsHeader>> GetNewsAsync(int page, int pageSize, NewsOptions options);

        Task<NewsArticle> GetNewsDetailsAsync(string articleId);

        Task<bool> ArticleUpdateReadStatusAsync(string id, bool value);

        Task LikeUnlikeArticleAsync(string id, bool value);

        Task LikeUnlikeCommentAsync(string articleId, string commentId, bool value);

        Task<MessageBase> CommentArticleAsync(string articleId, string message, Stream file, string fileExtension);

        Task<MessageBase> ReplyToCommentAsync(string articleId, string message, string commentId, Stream file,
            string fileExtension);

        Task<bool> DeleteMessageAsync(string articleId, string commentId);

        Task<NewsHeader> PostArticleAsync(PostArticle postArticle);

        Task<NewsHeader> UpdateArticleAsync(PostArticle postArticle);

        Task<IList<ChannelDto>> GetChannelsAsync();

        Task<ChannelInfo> GetChannelInfoAsync(string channelId);

        Task SetSubscriptionAsync(string channelId, bool isSubscribe);

        Task<IList<ArticleComment>> GetArticlecommentsAsync(string articleId);

        Task ReportAbuseAsync(string entityId, ReportEntityType entityType);
    }
}