﻿namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos
{
    internal class ArticleImageResponseDto
    {
        public string ArticleImageUrl { get; set; }
    }
}