﻿using System;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos
{
    internal class NewsDetailsDto
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime PublishDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string PictureUrl { get; set; }
        public string VideoUrl { get; set; }
        public int ArticleType { get; set; }
        public int LikesCount { get; set; }
        public bool IsReadByCurrentUser { get; set; }
        public bool IsLikedByCurrentUser { get; set; }
        public string Author { get; set; }
        public string AuthorImageUrl { get; set; }
        public bool IsCurrentUserCanEdit { get; set; }
        public bool IsCommentsEnabled { get; set; }
        public bool IsLikesEnabled { get; set; }
        public string RelatedChannelName { get; set; }
        public string RelatedChannelId { get; set; }
    }
}