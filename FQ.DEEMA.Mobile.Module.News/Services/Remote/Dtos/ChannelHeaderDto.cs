﻿namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos
{
    public class ChannelHeaderDto
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
