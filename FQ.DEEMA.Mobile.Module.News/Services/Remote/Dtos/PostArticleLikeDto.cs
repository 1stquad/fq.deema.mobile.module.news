﻿namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos
{
    internal class PostArticleLikeDto
    {
        public string Id { get; set; }

        public bool Value { get; set; }
    }
}