﻿using System.Collections.Generic;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos
{
    public class ConnectorDto
    {
        public string ConnectorName { get; set; }

        public IList<ChannelDto> Channels { get; set; }
    }
}
