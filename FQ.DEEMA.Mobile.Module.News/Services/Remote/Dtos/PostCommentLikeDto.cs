﻿namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos
{
    internal class PostCommentLikeDto
    {
        public string ArticleId { get; set; }

        public string CommentId { get; set; }

        public bool Value { get; set; }
    }
}