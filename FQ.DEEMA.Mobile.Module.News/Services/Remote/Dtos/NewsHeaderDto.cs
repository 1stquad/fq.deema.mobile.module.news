﻿using System;
using System.Collections.Generic;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos
{
    internal class NewsHeaderDto
    {
        public string Id { get; set; }

        public int ArticleType { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime PublishDate { get; set; }

        public bool IsLikedByCurrentUser { get; set; }

        public string PictureUrl { get; set; }

        public string VideoUrl { get; set; }

        public int LikesCount { get; set; }

        public int CommentsCount { get; set; }

        public bool IsCommentsEnabled { get; set; }

        public bool IsLikesEnabled { get; set; }

        public IEnumerable<ChannelHeaderDto> RelatedChannels { get; set; }
    }
}