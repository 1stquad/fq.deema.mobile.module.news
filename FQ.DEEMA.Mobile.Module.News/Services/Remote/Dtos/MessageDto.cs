﻿using System;
using Newtonsoft.Json;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos
{
    internal class MessageDto
    {
        public string Id { get; set; }
        public string AuthorId { get; set; }
        public string Content { get; set; } = string.Empty;
        public string ParentCommentId { get; set; }
        public string AuthorName { get; set; }
        public string AuthorImageUrl { get; set; }
        public string AuthorEmail { get; set; }

        [JsonProperty(PropertyName = "created")]
        public DateTime DateCreatedUtc { get; set; }

        public string CommentAttachment { get; set; }

        public bool IsLikedByCurrentUser { get; set; }
        public int LikesCount { get; set; }
    }
}