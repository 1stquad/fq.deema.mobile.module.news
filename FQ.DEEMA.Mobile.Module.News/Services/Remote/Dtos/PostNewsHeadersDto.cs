﻿using FQ.DEEMA.Mobile.Module.News.Models;
using Newtonsoft.Json;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos
{
    internal class PostNewsHeadersDto
    {
        public int Page { get; set; }

        public int PageSize { get; set; }

        public SortDto Sort { get; set; }

        [JsonIgnore] public NewsOptions NewsOptions { get; set; }
    }

    internal class SortDto
    {
        public string PropertyName { get; set; }

        public int Order { get; set; }
    }
}