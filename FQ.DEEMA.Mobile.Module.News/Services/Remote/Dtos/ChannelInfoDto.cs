namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos
{
    internal class ChannelInfoDto
    {
        public bool IsChannelReadAvailable { get; set; }
        public bool IsChannelWriteAvailable { get; set; }
        public bool IsSubscribed { get; set; }
        public string ChannelId { get; set; }
    }
}