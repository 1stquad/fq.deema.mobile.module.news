﻿namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos
{
    internal class ReportAbuseDto
    {
        public ReportEntityTypeDto EntityType { get; set; }
        public string EntityId { get; set; }
    }

    internal enum ReportEntityTypeDto
    {
        Article = 0,
        Comment = 1
    }
}
