﻿namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos
{
    public class ChannelDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsSubscribed { get; set; }
        public bool IsUnSubscribeAvailable { get; set; }
        public ChannelDto[] Childs { get; set; }
    }
}