﻿using System;
using Newtonsoft.Json;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos
{
    internal class UpdateArticleDto
    {
        [JsonIgnore] public string Id { get; set; }

        public string Heading { get; set; }
        public string Content { get; set; }
        public DateTime PostDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ArticleImageUrl { get; set; }
        public bool IsCommentsEnabled { get; set; }
        public bool IsLikesEnabled { get; set; }
    }
}