﻿using System.IO;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos
{
    internal class PostReplyDto
    {
        public string ArticleId { get; set; }

        public string CommentId { get; set; }

        public string Content { get; set; }

        public Stream File { get; set; }

        public string FileExtension { get; set; }
    }
}