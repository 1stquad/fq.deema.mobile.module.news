﻿namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos
{
    internal class IdDto
    {
        public string Id { get; set; }
    }
}