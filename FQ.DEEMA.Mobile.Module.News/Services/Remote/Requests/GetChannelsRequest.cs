﻿using System.Net.Http;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests
{
    internal class GetChannelsRequest : BaseRestRequest
    {
        public override string EndpointUrl => "/channel/tree";

        public override HttpMethod Method => HttpMethod.Get;
    }
}