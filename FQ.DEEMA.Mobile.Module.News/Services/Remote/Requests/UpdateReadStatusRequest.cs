﻿using System.Net.Http;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests
{
    internal class UpdateReadStatusRequest : BaseRestRequest
    {
        private readonly string _articleId;
        private readonly bool _value;

        public UpdateReadStatusRequest(string articleId, bool value)
        {
            _articleId = articleId;
            _value = value;
        }

        public override string EndpointUrl => $"/article/{_articleId}/{(_value ? "read" : "unread")}";

        public override HttpMethod Method => HttpMethod.Post;
    }
}