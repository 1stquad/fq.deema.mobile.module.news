﻿using System.IO;
using System.Net.Http;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests
{
    internal class UploadNewsPhotoRequest : BaseRestRequest
    {
        private readonly string _extension;
        private readonly Stream _stream;

        public UploadNewsPhotoRequest(Stream stream, string extension)
        {
            _stream = stream;
            _extension = extension;
        }

        public override HttpMethod Method => HttpMethod.Post;

        public override string EndpointUrl => "/article/image-upload";

        public override HttpContent GetContent()
        {
            var content = new MultipartFormDataContent();
            var imageContent = new StreamContent(_stream);
            imageContent.Headers.Add("Content-Disposition",
                "form-data; name=\"file\"; filename=\"" + $"image{_extension}" + "\"");
            imageContent.Headers.Add("Content-Type", $"image/{_extension.Replace(".", string.Empty)}");
            content.Add(imageContent, "file", $"image{_extension}");

            return content;
        }
    }
}