﻿using System.Net.Http;
using FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos;
using Softeq.XToolkit.Common.Interfaces;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests
{
    internal class ArticleLikeRequest : BasePostRestRequest<PostArticleLikeDto>
    {
        public ArticleLikeRequest(IJsonSerializer jsonSerializer, PostArticleLikeDto dto)
            : base(jsonSerializer, dto)
        {
        }

        public override string EndpointUrl => $"/article/{Dto.Id}/{(Dto.Value ? "like" : "unlike")}";

        public override HttpContent GetContent()
        {
            return null;
        }
    }
}