﻿using Softeq.XToolkit.Common.Interfaces;
using Softeq.XToolkit.RemoteData.HttpClient;
using FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests
{
    internal class PostReportAbuseRequest : BasePostRestRequest<ReportAbuseDto>
    {
        public PostReportAbuseRequest(IJsonSerializer jsonSerializer, ReportAbuseDto dto)
            : base(jsonSerializer, dto)
        {
        }

        public override string EndpointUrl => $"/report/abuse";
    }
}
