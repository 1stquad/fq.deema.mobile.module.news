﻿using System;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos;
using Softeq.XToolkit.Common.Interfaces;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests
{
    internal class NewsFeedRequest : BasePostRestRequest<PostNewsHeadersDto>
    {
        public NewsFeedRequest(IJsonSerializer jsonSerializer, PostNewsHeadersDto dto)
            : base(jsonSerializer, dto)
        {
        }

        public override string EndpointUrl
        {
            get
            {
                if (Dto.NewsOptions.ChannelId != null)
                {
                    return $"/news/{Dto.NewsOptions.ChannelId}";
                }

                if (Dto.NewsOptions.NewsType != NewsType.Unknown)
                {
                    return $"/news/{GetUrlPart(Dto.NewsOptions.NewsType)}";
                }

                throw new Exception("NewsType or ChannelId required");
            }
        }

        private static string GetUrlPart(NewsType newsType)
        {
            switch (newsType)
            {
                case NewsType.Unread:
                    return "unread";
                case NewsType.Read:
                    return "read";
                case NewsType.MyNews:
                    return "my";
                default:
                    return "feed";
            }
        }
    }
}