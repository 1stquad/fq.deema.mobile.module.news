﻿using System;
using System.Globalization;
using System.Net.Http;
using FQ.DEEMA.Mobile.Module.News.Models;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests
{
    internal class PostArticleRequest : BaseRestRequest
    {
        private readonly PostArticle _postArticle;

        public PostArticleRequest(PostArticle postArticle)
        {
            _postArticle = postArticle;
        }

        public override HttpMethod Method => HttpMethod.Post;

        public override string EndpointUrl => "/article";

        public override HttpContent GetContent()
        {
            var content = new MultipartFormDataContent();

            if (_postArticle.PostImage != null)
            {
                var imageContent = new StreamContent(_postArticle.PostImage);
                imageContent.Headers.Add("Content-Type", $"image/{_postArticle.Extension.Replace(".", string.Empty)}");
                content.Add(imageContent, "articleImage", $"image{_postArticle.Extension}");
            }

            var titleContent = new StringContent(_postArticle.Title);
            content.Add(titleContent, "heading");

            var bodyContent = new StringContent(_postArticle.Content);
            content.Add(bodyContent, "content");

            var dateContent = new StringContent(DateTimeOffsetToString(_postArticle.DateFrom));
            content.Add(dateContent, "postDate");

            var endDateContent = new StringContent(DateTimeOffsetToString(_postArticle.DateEnd));
            content.Add(endDateContent, "endDate");

            var channelContent = new StringContent(_postArticle.ChannelId);
            content.Add(channelContent, "channelIds[0]");

            var isCommentsEnabled = new StringContent(_postArticle.SocialFeaturesEnabled.ToString());
            content.Add(isCommentsEnabled, "IsCommentsEnabled");

            return content;
        }

        private static string DateTimeOffsetToString(DateTimeOffset dateTimeOffset)
        {
            return dateTimeOffset.UtcDateTime.ToString("o", CultureInfo.InvariantCulture);
        }
    }
}