﻿using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests
{
    internal class NewsDetailsRequest : BaseRestRequest
    {
        private readonly string _id;

        public NewsDetailsRequest(string id)
        {
            _id = id;
        }

        public override string EndpointUrl => $"/article/{_id}/details";
    }
}