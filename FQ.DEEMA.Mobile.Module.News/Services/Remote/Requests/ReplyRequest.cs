﻿using System.Net.Http;
using FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests
{
    internal class ReplyRequest : BaseRestRequest
    {
        private readonly PostReplyDto _dto;

        public ReplyRequest(PostReplyDto dto)
        {
            _dto = dto;
        }

        public override HttpMethod Method => HttpMethod.Post;

        public override string EndpointUrl => $"/article/{_dto.ArticleId}/comment/{_dto.CommentId}/reply";

        public override HttpContent GetContent()
        {
            var content = new MultipartFormDataContent();

            var idContent = new StringContent(_dto.ArticleId);
            content.Add(idContent, "articleId");

            var commentIdContent = new StringContent(_dto.CommentId);
            content.Add(commentIdContent, "commentId");

            if (_dto.File != null)
            {
                var imageContent = new StreamContent(_dto.File);
                imageContent.Headers.Add("Content-Type", $"image/{_dto.FileExtension.Replace(".", string.Empty)}");
                content.Add(imageContent, "commentAttachment", $"image{_dto.FileExtension}");
            }

            var bodyContent = new StringContent(_dto.Content);
            content.Add(bodyContent, "content");

            return content;
        }
    }
}