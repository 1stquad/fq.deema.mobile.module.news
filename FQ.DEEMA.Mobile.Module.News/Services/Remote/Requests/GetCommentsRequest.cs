﻿using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests
{
    internal class GetCommentsRequest : BaseRestRequest
    {
        private readonly string _articleId;

        public GetCommentsRequest(string articleId)
        {
            _articleId = articleId;
        }

        public override string EndpointUrl => $"/article/{_articleId}/details/comment";
    }
}