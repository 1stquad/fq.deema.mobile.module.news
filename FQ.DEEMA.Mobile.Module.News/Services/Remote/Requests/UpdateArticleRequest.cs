﻿using System.Net.Http;
using FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos;
using Softeq.XToolkit.Common.Interfaces;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests
{
    internal class UpdateArticleRequest : BasePostRestRequest<UpdateArticleDto>
    {
        public UpdateArticleRequest(IJsonSerializer jsonSerializer, UpdateArticleDto dto) : base(jsonSerializer, dto)
        {
        }

        public override HttpMethod Method => HttpMethod.Put;

        public override string EndpointUrl => $"/article/{Dto.Id}";
    }
}