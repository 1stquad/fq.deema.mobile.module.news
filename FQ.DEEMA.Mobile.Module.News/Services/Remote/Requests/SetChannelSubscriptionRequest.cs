using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests
{
    internal class SetChannelSubscriptionRequest : BaseRestRequest
    {
        private readonly string _channelId;
        private readonly bool _value;

        public SetChannelSubscriptionRequest(string channelId, bool value)
        {
            _channelId = channelId;
            _value = value;
        }

        public override string EndpointUrl => $"/channel/{_channelId}/{(_value ? "subscribe" : "unsubscribe")}";
    }
}