﻿using System.Net.Http;
using FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos;
using Softeq.XToolkit.Common.Interfaces;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests
{
    internal class CommentLikeRequest : BasePostRestRequest<PostCommentLikeDto>
    {
        public CommentLikeRequest(IJsonSerializer jsonSerializer, PostCommentLikeDto dto)
            : base(jsonSerializer, dto)
        {
        }

        public override string EndpointUrl =>
            $"/article/{Dto.ArticleId}/comment/{Dto.CommentId}/{(Dto.Value ? "like" : "unlike")}";

        public override HttpContent GetContent()
        {
            return null;
        }
    }
}