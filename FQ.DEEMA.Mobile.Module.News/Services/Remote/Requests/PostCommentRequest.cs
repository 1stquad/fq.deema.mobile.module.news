﻿using System.Net.Http;
using FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests
{
    internal class PostCommentRequest : BaseRestRequest
    {
        private readonly PostCommentDto _dto;

        public PostCommentRequest(PostCommentDto dto)
        {
            _dto = dto;
        }

        public override HttpMethod Method => HttpMethod.Post;

        public override string EndpointUrl => $"/article/{_dto.ArticleId}/comment";

        public override HttpContent GetContent()
        {
            var content = new MultipartFormDataContent();

            var idContent = new StringContent(_dto.ArticleId);
            content.Add(idContent, "articleId");

            if (_dto.File != null)
            {
                var imageContent = new StreamContent(_dto.File);
                imageContent.Headers.Add("Content-Type", $"image/{_dto.FileExtension.Replace(".", string.Empty)}");
                content.Add(imageContent, "commentAttachment", $"image{_dto.FileExtension}");
            }

            if (!string.IsNullOrEmpty(_dto.Content))
            {
                var bodyContent = new StringContent(_dto.Content);
                content.Add(bodyContent, "content");
            }

            return content;
        }
    }
}