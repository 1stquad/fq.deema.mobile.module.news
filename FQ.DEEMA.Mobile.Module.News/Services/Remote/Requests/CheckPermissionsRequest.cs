using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests
{
    internal class CheckPermissionsRequest : BaseRestRequest
    {
        private readonly string _channelId;

        public CheckPermissionsRequest(string channelId)
        {
            _channelId = channelId;
        }

        public override string EndpointUrl => $"/channel/{_channelId}/permissions";
    }
}