﻿using System.Net.Http;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests
{
    internal class DeleteMessageRequest : BaseRestRequest
    {
        private readonly string _articleId;
        private readonly string _commentId;

        public DeleteMessageRequest(string articleId, string commentId)
        {
            _articleId = articleId;
            _commentId = commentId;
        }

        public override HttpMethod Method => HttpMethod.Delete;

        public override string EndpointUrl => $"/article/{_articleId}/comment/{_commentId}";
    }
}