﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.Models.Report;
using FQ.DEEMA.Mobile.Module.News.Services.Remote.Dtos;
using FQ.DEEMA.Mobile.Module.News.Services.Remote.Requests;
using Softeq.XToolkit.Common.Interfaces;
using Softeq.XToolkit.Common.Models;
using Softeq.XToolkit.RemoteData;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.News.Services.Remote
{
    public class RemoteNewsService : IRemoteNewsService
    {
        private readonly IJsonSerializer _jsonSerializer;
        private readonly ILogger _logger;
        private readonly IRestHttpClient _restHttpClient;

        public RemoteNewsService(
            IJsonSerializer jsonSerializer,
            IRestHttpClient restHttpClient,
            ILogManager logManager)
        {
            _jsonSerializer = jsonSerializer;
            _restHttpClient = restHttpClient;
            _logger = logManager.GetLogger<RemoteNewsService>();
        }

        public async Task<PagingModel<NewsHeader>> GetNewsAsync(int page, int pageSize, NewsOptions options)
        {
            var dto = new PostNewsHeadersDto
            {
                Page = page,
                PageSize = pageSize,
                NewsOptions = options,
                Sort = new SortDto { Order = 1, PropertyName = "publishDate" }
            };
            var request = new NewsFeedRequest(_jsonSerializer, dto);

            var model = await _restHttpClient.GetPagingModelAsync<NewsHeader, NewsHeaderDto>(
                request, _logger, ToNewsHeader);

            return model;
        }

        public Task<bool> ArticleUpdateReadStatusAsync(string id, bool value)
        {
            var request = new UpdateReadStatusRequest(id, value);
            return _restHttpClient.TrySendAsync(request, _logger);
        }

        public async Task<NewsArticle> GetNewsDetailsAsync(string articleId)
        {
            var request = new NewsDetailsRequest(articleId);

            var model = await _restHttpClient.GetModelAsync<NewsArticle, NewsDetailsDto>(
                request, _logger, x =>
                {
                    var article = new NewsArticle
                    {
                        ArticleId = x.Id,
                        ArticleBody = new ArticleBody
                        {
                            ArticleId = x.Id,
                            Description = x.Content,
                            ImageUrl = x.PictureUrl,
                            VideoUrl = x.VideoUrl,
                            ArticleType = (ArticleType)x.ArticleType,
                            PublishDate = x.PublishDate.ToLocalTime(),
                            ExpirationDate = x.ExpirationDate.ToLocalTime(),
                            Title = x.Title,
                            IsReadByCurrentUser = x.IsReadByCurrentUser,
                            LikesCount = x.LikesCount,
                            IsLikedByCurrentUser = x.IsLikedByCurrentUser,
                            PublisherName = x.Author,
                            PublisherImageUrl = x.AuthorImageUrl,
                            CanEdit = x.IsCurrentUserCanEdit,
                            IsCommentsEnabled = x.IsCommentsEnabled,
                            IsLikesEnabled = x.IsLikesEnabled,
                            ChannelName = x.RelatedChannelName ?? string.Empty,
                            ChannelId = x.RelatedChannelId
                        }
                    };

                    return article;
                });

            return model;
        }

        public Task LikeUnlikeArticleAsync(string id, bool value)
        {
            var request = new ArticleLikeRequest(_jsonSerializer, new PostArticleLikeDto { Id = id, Value = value });
            return _restHttpClient.TrySendAsync(request, _logger);
        }

        public Task LikeUnlikeCommentAsync(string articleId, string commentId, bool value)
        {
            var request = new CommentLikeRequest(
                _jsonSerializer, new PostCommentLikeDto { ArticleId = articleId, CommentId = commentId, Value = value });
            return _restHttpClient.TrySendAsync(request, _logger);
        }

        public async Task<MessageBase> CommentArticleAsync(string articleId, string message, Stream file,
            string fileExtension)
        {
            var dto = new PostCommentDto
            {
                ArticleId = articleId,
                Content = message,
                File = file,
                FileExtension = fileExtension
            };

            var request = new PostCommentRequest(dto);
            var model = await _restHttpClient.GetModelAsync<MessageBase, MessageDto>(request, _logger, x =>
            {
                return new MessageBase
                {
                    Id = x.Id,
                    ImageUrl = x.CommentAttachment
                };
            }).ConfigureAwait(false);

            return model;
        }

        public async Task<MessageBase> ReplyToCommentAsync(string articleId, string message, string commentId,
            Stream file, string fileExtension)
        {
            var dto = new PostReplyDto
            {
                ArticleId = articleId,
                CommentId = commentId,
                Content = message,
                File = file,
                FileExtension = fileExtension
            };

            var request = new ReplyRequest(dto);
            var model = await _restHttpClient.GetModelAsync<MessageBase, MessageDto>(request, _logger, x =>
            {
                return new MessageBase
                {
                    Id = x.Id,
                    ImageUrl = x.CommentAttachment
                };
            }).ConfigureAwait(false);

            return model;
        }

        public Task<bool> DeleteMessageAsync(string articleId, string commentId)
        {
            var request = new DeleteMessageRequest(articleId, commentId);
            return _restHttpClient.TrySendAsync(request, _logger);
        }

        public async Task<NewsHeader> PostArticleAsync(PostArticle postArticle)
        {
            var request = new PostArticleRequest(postArticle);
            var result = await _restHttpClient.TrySendAndDeserializeAsync<NewsHeaderDto>(request, _logger);

            return result == null ? null : ToNewsHeader(result);
        }

        public async Task<NewsHeader> UpdateArticleAsync(PostArticle postArticle)
        {
            if (postArticle.PostImage != null)
            {
                var request = new UploadNewsPhotoRequest(postArticle.PostImage, postArticle.Extension);
                var articleResponse =
                    await _restHttpClient.TrySendAndDeserializeAsync<ArticleImageResponseDto>(request, _logger);
                if (articleResponse != null)
                {
                    postArticle.ImageUrl = articleResponse.ArticleImageUrl;
                }
            }

            var dto = new UpdateArticleDto
            {
                Id = postArticle.Id,
                ArticleImageUrl = postArticle.ImageUrl,
                Content = postArticle.Content,
                EndDate = postArticle.DateEnd.UtcDateTime,
                Heading = postArticle.Title,
                PostDate = postArticle.DateFrom.UtcDateTime,
                IsCommentsEnabled = postArticle.SocialFeaturesEnabled,
                IsLikesEnabled = postArticle.SocialFeaturesEnabled
            };

            var updateArticleRequest = new UpdateArticleRequest(_jsonSerializer, dto);
            var updateResult = await _restHttpClient.TrySendAndDeserializeAsync<NewsHeaderDto>(updateArticleRequest, _logger);

            return updateResult == null ? null : ToNewsHeader(updateResult);
        }

        public async Task<IList<ChannelDto>> GetChannelsAsync()
        {
            var request = new GetChannelsRequest();
            var connectors = await _restHttpClient.TrySendAndDeserializeAsync<IList<ConnectorDto>>(request, _logger).ConfigureAwait(false);

            if (connectors == null)
            {
                return null;
            }

            return connectors
                .Where(x => x.Channels != null)
                .SelectMany(x => x.Channels).ToList();
        }

        public Task<ChannelInfo> GetChannelInfoAsync(string channelId)
        {
            var request = new CheckPermissionsRequest(channelId);
            return _restHttpClient.GetModelAsync<ChannelInfo, ChannelInfoDto>(request, _logger,
                x => new ChannelInfo
                {
                    IsSubscribed = x.IsSubscribed,
                    IsChannelWriteAvailable = x.IsChannelWriteAvailable
                });
        }

        public Task SetSubscriptionAsync(string channelId, bool isSubscribe)
        {
            var request = new SetChannelSubscriptionRequest(channelId, isSubscribe);
            return _restHttpClient.TrySendAsync(request, _logger);
        }

        public async Task<IList<ArticleComment>> GetArticlecommentsAsync(string articleId)
        {
            var request = new GetCommentsRequest(articleId);
            var dtos = await _restHttpClient.TrySendAndDeserializeAsync<MessageDto[]>(request, _logger)
                .ConfigureAwait(false);
            return dtos == null ? null : ExtractComments(dtos);
        }

        private IList<ArticleComment> ExtractComments(MessageDto[] dtos)
        {
            var comments = dtos
                .Where(x => x.ParentCommentId == null)
                .Select(x => new ArticleComment
                {
                    Id = x.Id,
                    Message = x.Content,
                    DateCreated = x.DateCreatedUtc.ToLocalTime(),
                    PublisherName = x.AuthorName,
                    PublisherImageUrl = x.AuthorImageUrl,
                    PublisherEmail = x.AuthorEmail,
                    PublisherId = x.AuthorId,
                    LikesCount = x.LikesCount,
                    IsLikedByCurrentUser = x.IsLikedByCurrentUser,
                    HasImage = !string.IsNullOrEmpty(x.CommentAttachment),
                    ImageUrl = x.CommentAttachment
                })
                .ToArray();

            var repliesDict = dtos
                .Where(x => x.ParentCommentId != null)
                .GroupBy(x => x.ParentCommentId)
                .ToDictionary(y => y.Key, y => y.ToArray());

            foreach (var comment in comments)
            {
                if (repliesDict.ContainsKey(comment.Id))
                {
                    var replies = repliesDict[comment.Id].Select(x => new ArticleCommentReply
                    {
                        Id = x.Id,
                        Message = x.Content,
                        DateCreated = x.DateCreatedUtc.ToLocalTime(),
                        PublisherName = x.AuthorName,
                        PublisherImageUrl = x.AuthorImageUrl,
                        PublisherEmail = x.AuthorEmail,
                        PublisherId = x.AuthorId,
                        LikesCount = x.LikesCount,
                        IsLikedByCurrentUser = x.IsLikedByCurrentUser,
                        HasImage = !string.IsNullOrEmpty(x.CommentAttachment),
                        ImageUrl = x.CommentAttachment
                    }).ToArray();

                    comment.ArticleCommentReplies.AddRange(replies);
                }
            }

            return comments;
        }

        private NewsHeader ToNewsHeader(NewsHeaderDto dto)
        {
            return new NewsHeader
            {
                Description = dto.Content,
                ImageUrl = dto.PictureUrl,
                VideoUrl = dto.VideoUrl,
                ArticleType = (ArticleType)dto.ArticleType,
                PublishDate = dto.PublishDate.ToLocalTime(),
                Title = dto.Title,
                Id = dto.Id,
                IsLikedByCurrentUser = dto.IsLikedByCurrentUser,
                LikesCount = dto.LikesCount,
                CommentsCount = dto.CommentsCount,
                IsLikesEnabled = dto.IsLikesEnabled,
                IsCommentsEnabled = dto.IsCommentsEnabled,
                RelatedChannels = dto.RelatedChannels
                    .Select(c => new ChannelHeader
                    {
                        Id = c.Id,
                        Name = c.Name
                    })
                    .ToList()
            };
        }

        public async Task ReportAbuseAsync(string entityId, ReportEntityType entityType)
        {
            var dto = new ReportAbuseDto
            {
                EntityId = entityId,
                EntityType = (ReportEntityTypeDto)entityType
            };

            var request = new PostReportAbuseRequest(_jsonSerializer, dto);

            await _restHttpClient.TrySendAsync(request, _logger).ConfigureAwait(false);
        }
    }
}