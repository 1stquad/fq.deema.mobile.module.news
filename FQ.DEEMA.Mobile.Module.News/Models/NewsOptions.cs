namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public class NewsOptions
    {
        public NewsType NewsType { get; set; }
        public string ChannelId { get; set; }
    }
}