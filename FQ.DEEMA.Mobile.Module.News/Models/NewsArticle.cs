﻿using System.Collections.Generic;
using Softeq.XToolkit.Common.Extensions;

namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public class NewsArticle
    {
        public NewsArticle()
        {
            ArticleComments = new List<ArticleComment>();
        }

        public string ArticleId { get; set; }

        public ArticleBody ArticleBody { get; set; }

        public List<ArticleComment> ArticleComments { get; }

        public IList<IArticleItem> ExtractCommentItems()
        {
            var list = new List<IArticleItem>();

            foreach (var comment in ArticleComments.EmptyIfNull())
            {
                list.Add(comment);
                foreach (var reply in comment.ArticleCommentReplies.EmptyIfNull())
                {
                    list.Add(reply);
                }
            }

            return list;
        }
    }
}