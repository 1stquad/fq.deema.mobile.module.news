﻿using System;
using Softeq.XToolkit.Common.Interfaces;

namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public class NumberToMetricConverter : IConverter<string, int>
    {
        public string ConvertValue(int number, object parameter = null, string language = null)
        {
            const int million = 1_000_000;
            const int thousand = 1_000;

            var exponentText = string.Empty;

            if (number >= million)
            {
                number /= million;
                exponentText = "M";
            }
            else if (number >= thousand)
            {
                number /= thousand;
                exponentText = "K";
            }

            return number.ToString() + exponentText;
        }

        public int ConvertValueBack(string value, object parameter = null, string language = null)
        {
            throw new NotImplementedException();
        }
    }
}
