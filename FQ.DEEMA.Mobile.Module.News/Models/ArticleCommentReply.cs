﻿namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public class ArticleCommentReply : MessageBase, IArticleItem
    {
        public ArticleItemType NewsDetailsType => ArticleItemType.Reply;
    }
}