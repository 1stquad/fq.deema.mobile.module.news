﻿namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public enum NewsType
    {
        Unknown = 0,
        Unread = 1,
        Read = 2,
        All = 3,
        MyNews = 4
    }
}