﻿namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public enum ArticleItemType
    {
        Body = 1,
        Comment = 2,
        Reply = 3
    }
}