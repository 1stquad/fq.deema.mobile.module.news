﻿namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public class ChannelHeader
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
