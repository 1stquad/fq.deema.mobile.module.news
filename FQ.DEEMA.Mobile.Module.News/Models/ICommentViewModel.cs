﻿using System.Collections.Generic;
using FQ.DEEMA.Mobile.Module.News.Services;
using Softeq.XToolkit.WhiteLabel.Mvvm;

namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public interface ICommentViewModel<T> where T : MessageBase
    {
        T Model { get; }

        string ImageUrl { get; set; }
        string ImageCacheKey { get; set; }

        INewsLocalizedStrings NewsLocalizedStrings { get; }
        IEnumerable<MentionRange> Mentions { get; }

        IReadOnlyList<CommandAction> Options { get; }
    }
}
