﻿namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public enum ArticleType
    {
        Unknown = 0,
        News = 1,
        Video = 2
    }
}
