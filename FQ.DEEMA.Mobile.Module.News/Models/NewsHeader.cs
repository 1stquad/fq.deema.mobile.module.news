﻿using System;
using System.Collections.Generic;

namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public class NewsHeader
    {
        public string Id { get; set; }

        public ArticleType ArticleType { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string ImageUrl { get; set; }

        public string VideoUrl { get; set; }

        public DateTime PublishDate { get; set; }

        public bool IsLikedByCurrentUser { get; set; }

        public int LikesCount { get; set; }

        public int CommentsCount { get; set; }

        public bool IsCommentsEnabled { get; set; }

        public bool IsLikesEnabled { get; set; }

        public IList<ChannelHeader> RelatedChannels { get; set; }
    }
}