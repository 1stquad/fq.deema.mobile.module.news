namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public class ChannelInfo
    {
        public bool IsChannelWriteAvailable { get; set; }

        public bool IsSubscribed { get; set; }
    }
}