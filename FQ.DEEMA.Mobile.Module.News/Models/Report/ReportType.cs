﻿namespace FQ.DEEMA.Mobile.Module.News.Models.Report
{
    public enum ReportEntityType
    {
        Article = 0,
        Comment = 1
    }
}
