using System;
using System.Globalization;
using Softeq.XToolkit.Common.Extensions;
using Softeq.XToolkit.Common.Interfaces;
using Softeq.XToolkit.WhiteLabel;

namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public class DateTimeToStringConverter : IConverter<string, DateTime>
    {
        private readonly bool _is24HFormat;
        private readonly Lazy<INewsLocalizedStrings> _newsLocalizedStringsLazy;

        public DateTimeToStringConverter(bool is24HFormat)
        {
            _is24HFormat = is24HFormat;
            _newsLocalizedStringsLazy = Dependencies.IocContainer.LazyResolve<INewsLocalizedStrings>();
        }

        public string ConvertValue(DateTime value, object parameter = null, string language = null)
        {
            var timeFormat = _is24HFormat ? "HH:mm" : "hh:mm tt";

            if (value > DateTime.Now)
            {
                return value.ToString($"MMM d, {timeFormat}", CultureInfo.InvariantCulture);
            }

            var ts = DateTime.Now - value;

            if (ts.TotalMinutes < 1)
            {
                var totalSeconds = (int) Math.Ceiling(ts.TotalSeconds);
                return string.Format(_newsLocalizedStringsLazy.Value.SecondsAgoFormatted, totalSeconds);
            }

            if (ts.TotalMinutes < 59)
            {
                var totalMinutes = (int) Math.Ceiling(ts.TotalMinutes);
                return string.Format(_newsLocalizedStringsLazy.Value.MinutesAgoFormatted, totalMinutes);
            }

            if (value.IsToday())
            {
                return value.ToString(timeFormat, CultureInfo.InvariantCulture);
            }

            if (value.IsYesterday())
            {
                return $"{_newsLocalizedStringsLazy.Value.Yesterday}, {value.ToString(timeFormat, CultureInfo.InvariantCulture)}";
            }

            if (value.Year != DateTime.Now.Year)
            {
                return value.ToString("MMM d, yyyy", CultureInfo.InvariantCulture);
            }

            return value.ToString($"MMM d, {timeFormat}", CultureInfo.InvariantCulture);
        }

        public DateTime ConvertValueBack(string value, object parameter = null, string language = null)
        {
            throw new NotImplementedException();
        }
    }
}