﻿using System;
using System.IO;

namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public class PostArticle
    {
        public string Title { get; set; }

        public string Content { get; set; }

        public DateTimeOffset DateFrom { get; set; }

        public DateTimeOffset DateEnd { get; set; }

        public Stream PostImage { get; set; }

        public string ImageUrl { get; set; }

        public string ChannelId { get; set; }

        public string Id { get; set; }

        public string Extension { get; set; }

        public bool SocialFeaturesEnabled { get; set; }
    }
}