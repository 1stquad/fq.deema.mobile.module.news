﻿namespace FQ.DEEMA.Mobile.Module.News.Models.VideoPlayer
{
    public enum VideoPlayerType
    {
        Unknown,
        Html5,
        Youtube,
        MsStream
    }
}
