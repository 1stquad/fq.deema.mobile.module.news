﻿namespace FQ.DEEMA.Mobile.Module.News.Models.VideoPlayer
{
    public class VideoPlayerModel
    {
        public VideoPlayerType PlayerType { get; set; }
        public string PlayerPageHtml { get; set; }
    }
}
