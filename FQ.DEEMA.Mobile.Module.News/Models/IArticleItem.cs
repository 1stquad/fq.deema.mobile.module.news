﻿namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public interface IArticleItem
    {
        ArticleItemType NewsDetailsType { get; }

        bool CanDelete { get; }

        bool IsLikedByCurrentUser { get; set; }

        int LikesCount { get; set; }
    }
}