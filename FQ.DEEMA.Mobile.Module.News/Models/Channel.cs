﻿using System.Collections.Generic;

namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public class Channel
    {
        public Channel()
        {
            Channels = new List<Channel>();
        }

        public string Id { get; set; }

        public string Name { get; set; }

        public bool IsUnSubscribeAvailable { get; set; }

        public bool IsSubscribed { get; set; }

        public bool HasSubscribedChannels
        {
            get
            {
                if (IsSubscribed)
                {
                    return true;
                }

                return CheckIfAnyIsSubscribed(Channels);
            }
        }

        public Channel ParentChannel { get; set; }

        public List<Channel> Channels { get; set; }

        private bool CheckIfAnyIsSubscribed(IList<Channel> channels)
        {
            if (channels == null || channels.Count == 0)
            {
                return false;
            }

            foreach (var channel in channels)
            {
                if (channel.HasSubscribedChannels)
                {
                    return true;
                }
            }

            return false;
        }
    }
}