﻿using System.Collections.Generic;

namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public class ArticleComment : MessageBase, IArticleItem
    {
        public ArticleComment()
        {
            ArticleCommentReplies = new List<ArticleCommentReply>();
        }

        public List<ArticleCommentReply> ArticleCommentReplies { get; }

        public ArticleItemType NewsDetailsType => ArticleItemType.Comment;
    }
}