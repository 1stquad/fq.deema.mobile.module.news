﻿using System;

namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public class MessageBase
    {
        public string Id { get; set; }

        public string PublisherName { get; set; }

        public string PublisherImageUrl { get; set; }

        public string PublisherId { get; set; }

        public string PublisherEmail { get; set; }

        public string Message { get; set; }

        public DateTime DateCreated { get; set; }

        public bool CanDelete { get; set; }

        public bool IsLikedByCurrentUser { get; set; }

        public int LikesCount { get; set; }

        public string ImageUrl { get; set; }

        public bool HasImage { get; set; }
    }
}