﻿using System;

namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public class ArticleBody : IArticleItem
    {
        public string ArticleId { get; set; }

        public ArticleType ArticleType { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string ImageUrl { get; set; }

        public string VideoUrl { get; set; }

        public DateTime PublishDate { get; set; }

        public DateTime ExpirationDate { get; set; }

        public bool CanEdit { get; set; }

        public bool IsReadByCurrentUser { get; set; }

        public string PublisherName { get; set; }

        public string PublisherImageUrl { get; set; }

        public bool IsCommentsEnabled { get; set; }

        public bool IsLikesEnabled { get; set; }

        public ArticleItemType NewsDetailsType => ArticleItemType.Body;

        public bool CanDelete => false;

        public bool IsLikedByCurrentUser { get; set; }

        public int LikesCount { get; set; }

        public string ChannelName { get; set; }

        public string ChannelId { get; set; }
    }
}