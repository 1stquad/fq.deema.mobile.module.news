﻿using Softeq.XToolkit.Common.Interfaces;

namespace FQ.DEEMA.Mobile.Module.News.Models
{
    public class IndexToNewsTypeConverter : IConverter<NewsType, int>
    {
        public NewsType ConvertValue(int value, object parameter = null, string language = null)
        {
            var type = NewsType.Unknown;

            switch (value)
            {
                case 0:
                    type = NewsType.Unread;
                    break;
                case 1:
                    type = NewsType.All;
                    break;
            }

            return type;
        }

        public int ConvertValueBack(NewsType value, object parameter = null, string language = null)
        {
            var index = -1;

            switch (value)
            {
                case NewsType.Unread:
                    index = 0;
                    break;
                case NewsType.All:
                    index = 1;
                    break;
            }

            return index;
        }
    }
}