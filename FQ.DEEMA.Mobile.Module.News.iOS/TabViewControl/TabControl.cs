using System;
using CoreGraphics;
using Foundation;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.WhiteLabel.iOS.Controls;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.News.iOS.TabViewControl
{
    [Register("TabControl")]
    public class TabControl : NotifyingView
    {
        private RelayCommand<UIView> _selectCommand;
        private int _selectedIndex = -1;
        private UIStackView _stackView;

        public TabControl(IntPtr handle) : base(handle)
        {
        }

        public TabControl(CGRect frame) : base(frame)
        {
        }

        public int SelectedIndex
        {
            get => _selectedIndex;
            set
            {
                if (_selectedIndex == value)
                {
                    return;
                }

                DropSelection(_selectedIndex);

                _selectedIndex = value;

                SetSelection(_selectedIndex);

                RaisePropertyChanged();
            }
        }

        public event EventHandler TabReselected;

        public void InitializeView(params string[] labels)
        {
            var separatorView = new UIView
            {
                BackgroundColor = StyleHelper.Style.SeparatorColor,
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            AddSubview(separatorView);

            NSLayoutConstraint.ActivateConstraints(new[]
            {
                separatorView.RightAnchor.ConstraintEqualTo(RightAnchor),
                separatorView.LeftAnchor.ConstraintEqualTo(LeftAnchor),
                separatorView.HeightAnchor.ConstraintEqualTo(1),
                separatorView.BottomAnchor.ConstraintEqualTo(BottomAnchor)
            });

            _stackView = new UIStackView
            {
                TranslatesAutoresizingMaskIntoConstraints = false,
                Axis = UILayoutConstraintAxis.Horizontal,
                Distribution = UIStackViewDistribution.FillEqually,
                Alignment = UIStackViewAlignment.Fill
            };

            AddSubview(_stackView);

            NSLayoutConstraint.ActivateConstraints(new[]
            {
                _stackView.RightAnchor.ConstraintEqualTo(RightAnchor),
                _stackView.LeftAnchor.ConstraintEqualTo(LeftAnchor),
                _stackView.TopAnchor.ConstraintEqualTo(TopAnchor),
                _stackView.BottomAnchor.ConstraintEqualTo(BottomAnchor)
            });

            _selectCommand = new RelayCommand<UIView>(OnViewClicked);

            foreach (var label in labels)
            {
                var view = new TabView(_selectCommand, label);
                _stackView.AddArrangedSubview(view);
            }
        }

        protected override void Initialize()
        {
            //do nothing
        }

        private void DropSelection(int index)
        {
            if (index == -1)
            {
                return;
            }

            var view = _stackView.ArrangedSubviews[index] as TabView;
            view.IsSelected = false;
        }

        private void SetSelection(int index)
        {
            var view = _stackView.ArrangedSubviews[index] as TabView;
            view.IsSelected = true;
        }

        private void OnViewClicked(UIView view)
        {
            var index = -1;
            for (var i = 0; i < _stackView.ArrangedSubviews.Length; i++)
            {
                if (_stackView.ArrangedSubviews[i].Equals(view))
                {
                    index = i;
                    break;
                }
            }

            if (index != -1)
            {
                if (index == SelectedIndex)
                {
                    TabReselected.Invoke(this, EventArgs.Empty);
                }
                else
                {
                    SelectedIndex = index;
                }
            }
        }
    }
}