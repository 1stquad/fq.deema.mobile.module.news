using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common.Command;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.News.iOS.TabViewControl
{
    public class TabView : UIView
    {
        private readonly UIView _bottomLineView;
        private readonly UIButton _button;
        private readonly RelayCommand<UIView> _command;

        public TabView(RelayCommand<UIView> command, string label)
        {
            _command = command;

            //button

            _button = UIButton.FromType(UIButtonType.System);
            _button.TranslatesAutoresizingMaskIntoConstraints = false;
            _button.SetTitle(label, UIControlState.Normal);
            _button.TitleLabel.Font = StyleHelper.Style.MediumFont;
            _button.SetCommand(new RelayCommand(OnButtonClicked));

            AddSubview(_button);

            NSLayoutConstraint.ActivateConstraints(new[]
            {
                _button.RightAnchor.ConstraintEqualTo(RightAnchor),
                _button.LeftAnchor.ConstraintEqualTo(LeftAnchor),
                _button.TopAnchor.ConstraintEqualTo(TopAnchor),
                _button.BottomAnchor.ConstraintEqualTo(BottomAnchor)
            });


            AddSubview(_button);

            //bottom line

            _bottomLineView = new UIView {BackgroundColor = StyleHelper.Style.AccentColor};
            _bottomLineView.TranslatesAutoresizingMaskIntoConstraints = false;

            AddSubview(_bottomLineView);

            NSLayoutConstraint.ActivateConstraints(new[]
            {
                _bottomLineView.RightAnchor.ConstraintEqualTo(RightAnchor),
                _bottomLineView.LeftAnchor.ConstraintEqualTo(LeftAnchor),
                _bottomLineView.HeightAnchor.ConstraintEqualTo(2),
                _bottomLineView.BottomAnchor.ConstraintEqualTo(BottomAnchor)
            });

            IsSelected = false;
        }

        public bool IsSelected
        {
            get => !_bottomLineView.Hidden;
            set
            {
                _bottomLineView.Hidden = !value;
                var color = value ? StyleHelper.Style.AccentColor : StyleHelper.Style.ButtonTintColor;
                _button.TintColor = color;
            }
        }

        private void OnButtonClicked()
        {
            _command.Execute(this);
        }
    }
}