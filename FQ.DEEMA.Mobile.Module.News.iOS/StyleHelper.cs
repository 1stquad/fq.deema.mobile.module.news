using System;
using Softeq.XToolkit.WhiteLabel;
using Softeq.XToolkit.WhiteLabel.iOS.Helpers;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.News.iOS
{
    internal static class StyleHelper
    {
        private static readonly Lazy<INewsIosStyle> StyleLazy = Dependencies.IocContainer.LazyResolve<INewsIosStyle>();

        public static INewsIosStyle Style => StyleLazy.Value;
    }

    public interface INewsIosStyle
    {
        UIColor ContentColor { get; }
        UIColor TextNormalColor { get; }
        UIColor TextSecondaryColor { get; }
        UIColor SeparatorColor { get; }
        UIColor AccentColor { get; }
        UIColor ButtonTintColor { get; }
        UIColor NotificationBackgroundColor { get; }
        UIColor NewsSeparatorColor { get; }
        UIColor NewsSeparatorBackgroundColor { get; }
        UIFont BigSemiboldFont { get; }
        UIFont SmallFont { get; }
        UIFont NormalSemiboldFont { get; }
        UIFont BigFont { get; }
        UIFont NormalFont { get; }
        UIFont MediumFont { get; }
        AvatarImageHelpers.AvatarStyles AvatarStyles { get; }
        AvatarImageHelpers.AvatarStyles ProfileAvatarStyles { get; }

        string CloseButtonImageBoundleName { get; }
        string ImagePlaceholderBoundleName { get; }
        string LikeAccentBoundleName { get; }
        string LikeBoundleName { get; }
        string AccentCheckmarkBoundleName { get; }
        string CheckmarkBoundleName { get; }
        string ChannelBoundleName { get; }
        string ArrowBoundleName { get; }
        string LikeFeedBoundleName { get; }
        string CommentsBoundleName { get; }
        string ReplyBoundleName { get; }
        string AddBoundleName { get; }
        string FeedPlaceholderBoundleName { get; }
        string BackBoundleName { get; }
        string SendBoundleName { get; }
        string ClearButtonBoundleName { get; }
        string AddImageBoundleName { get; }
        string TakePhotoBoundleName { get; }
        string DetailsPlaceholderBoundleName { get; }
        string EmptyPlaceholderBoundleName { get; }
        string AppLogoBoundleName { get; }
        string ChannelsBoundleName { get; }
        string PlayImageBoundleName { get; }
        string MoreBoundleName { get; }
    }
}