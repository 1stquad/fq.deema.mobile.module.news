﻿using System;
using AsyncDisplayKitBindings;
using Foundation;
using UIKit;
using Softeq.XToolkit.Common;
using Softeq.XToolkit.Common.EventArguments;
using Softeq.XToolkit.WhiteLabel.iOS.Shared;
using FQ.DEEMA.Mobile.Module.News.ViewModels;

namespace FQ.DEEMA.Mobile.Module.News.iOS
{
    internal class NewsTableDelegate : ASTableDelegate
    {
        private WeakReferenceEx<ObservableASTableDataSource<NewsHeaderViewModel>> _newsDataSourceRef;
        public event EventHandler LastItemRequested;
        public event EventHandler<GenericEventArgs<NewsHeaderViewModel>> RowSelected;

        public override void WillDisplayNodeForRowAtIndexPath(ASTableView tableView, NSIndexPath indexPath)
        {
            if (_newsDataSourceRef == null)
            {
                var table = tableView;
                _newsDataSourceRef =
                    WeakReferenceEx.Create(
                        table.TableNode.DataSource as ObservableASTableDataSource<NewsHeaderViewModel>);
            }

            var count = _newsDataSourceRef.Target?.DataSource.Count;
            if (indexPath.Row + 1 == count)
            {
                LastItemRequested?.Invoke(this, EventArgs.Empty);
            }
        }

        public override void DidSelectRowAtIndexPath(UITableView tableView, NSIndexPath indexPath)
        {
            var item = _newsDataSourceRef.Target?.DataSource[indexPath.Row];
            var args = new GenericEventArgs<NewsHeaderViewModel>(item);
            RowSelected?.Invoke(this, args);
        }
    }
}