﻿using System;
using FQ.DEEMA.Mobile.Module.News.iOS.Cells;
using FQ.DEEMA.Mobile.Module.News.iOS.ViewControllerComponents;
using FQ.DEEMA.Mobile.Module.News.ViewModels;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Bindings.iOS;
using Softeq.XToolkit.WhiteLabel.iOS;
using Softeq.XToolkit.WhiteLabel.iOS.Extensions;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.News.iOS.ViewControllers
{
    public partial class NewsChannelsViewController : ViewControllerBase<NewsChannelsViewModel>
    {
        public NewsChannelsViewController(IntPtr handle) : base(handle)
        {
            ControllerComponents.Add(new NavigationTransitionComponent("push_animation_key", true));
            ControllerComponents.Add(new NavigationTransitionComponent("pop_animation_key", false));
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            NavigationView.SetCommand(UIImage.FromBundle(StyleHelper.Style.BackBoundleName), ViewModel.BackCommand, true);

            TableView.RegisterNibForCellReuse(ChannelCell.Nib, ChannelCell.Key);
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TableView.RowHeight = 50;

            var source = new ObservableTableViewSource<ChannelViewModel>
            {
                DataSource = ViewModel.Items,
                ReuseId = ChannelCell.Key,
                BindCellDelegate = (cell, item, nsIndexPath) =>
                {
                    var channelCell = (ChannelCell)cell;
                    channelCell.BindCell(item);
                }
            };

            TableView.Source = source;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            TableView.ReloadData();
        }

        protected override void DoAttachBindings()
        {
            base.DoAttachBindings();

            Bindings.Add(this.SetBinding(() => ViewModel.Title, () => NavigationView.Title));
        }
    }
}