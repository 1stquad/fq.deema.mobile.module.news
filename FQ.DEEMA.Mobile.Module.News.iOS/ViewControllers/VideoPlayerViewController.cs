using System;
using FQ.DEEMA.Mobile.Module.News.ViewModels;
using Softeq.XToolkit.WhiteLabel.iOS;
using Softeq.XToolkit.Bindings;
using UIKit;
using Foundation;
using System.Threading.Tasks;
using Softeq.XToolkit.WhiteLabel.Threading;

namespace FQ.DEEMA.Mobile.Module.News.iOS.ViewControllers
{
    public partial class VideoPlayerViewController : ViewControllerBase<VideoPlayerViewModel>
    {
        public VideoPlayerViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            CloseButton.SetImage(UIImage.FromBundle(StyleHelper.Style.CloseButtonImageBoundleName), UIControlState.Normal);
            CloseButton.SetCommand(ViewModel.DialogComponent.CloseCommand, true);

            WebViewPlayer.ScrollView.ScrollEnabled = false;
        }

        protected override void DoAttachBindings()
        {
            base.DoAttachBindings();

            WebViewPlayer.LoadHtmlString(ViewModel.VideoPlayerModel.PlayerPageHtml, NSBundle.MainBundle.BundleUrl);
        }

        public override async void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);

            await Task.Delay(100);

            Execute.BeginOnUIThread(() =>
            {
                UIApplication.SharedApplication.StatusBarHidden = false;
            });
        }
    }
}
