﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AsyncDisplayKitBindings;
using CoreFoundation;
using CoreGraphics;
using FFImageLoading;
using Foundation;
using UIKit;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Common.iOS.Extensions;
using Softeq.XToolkit.WhiteLabel.iOS;
using Softeq.XToolkit.WhiteLabel.iOS.Extensions;
using Softeq.XToolkit.WhiteLabel.iOS.ImagePicker;
using Softeq.XToolkit.WhiteLabel.iOS.Shared;
using Softeq.XToolkit.WhiteLabel.Messages;
using Softeq.XToolkit.WhiteLabel.Messenger;
using Softeq.XToolkit.WhiteLabel.Threading;
using FQ.DEEMA.Mobile.Module.News.iOS.Cells;
using FQ.DEEMA.Mobile.Module.News.Messages;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using Softeq.XToolkit.WhiteLabel;
using System.Linq;
using Softeq.XToolkit.WhiteLabel.Interfaces;

namespace FQ.DEEMA.Mobile.Module.News.iOS.ViewControllers.NewsDetails
{
    public partial class NewsDetailsPageViewController : ViewControllerBase<NewsDetailsPageViewModel>
    {
        private readonly nfloat _maxHeight = 150f;

        private const int DefaultInputHeight = 37;

        private nfloat? _calculatedMaxTextViewHeight;
        private SimpleImagePicker _simpleImagePicker;
        private UITapGestureRecognizer _overlayViewTapRecognizer;
        private UILongPressGestureRecognizer _longTapRecognizer;
        private string _previewImageKey;
        private UITextView _proxyTextView;
        private UIRefreshControl _refreshControl;
        private ASTableNode _tableNode;

        public NewsDetailsPageViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            CustomNavigationView.SetCommand(UIImage.FromBundle(StyleHelper.Style.BackBoundleName), ViewModel.BackCommand, true);

            _refreshControl = new UIRefreshControl();
            _refreshControl.SetCommand(nameof(_refreshControl.ValueChanged), ViewModel.RefreshCommand);
            _refreshControl.TintColor = StyleHelper.Style.AccentColor;

            _tableNode = new ASTableNode(UITableViewStyle.Plain);
            _tableNode.View.AddAsSubviewWithParentSize(ContentView);
            _tableNode.View.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            _tableNode.DataSource = new ObservableASTableDataSource<IArticleItem>(
                ViewModel.Items,
                _tableNode,
                (index, ds) =>
                {
                    var item = ds[index];
                    var type = item.NewsDetailsType;
                    switch (type)
                    {
                        case ArticleItemType.Body:
                            var newsNode = new BodyCell();
                            newsNode.BindCell(item as BodyViewModel);
                            return newsNode;
                        case ArticleItemType.Comment:
                            var commentCell = new CommentCell();
                            commentCell.Bind(item as CommentViewModel);
                            return commentCell;
                        case ArticleItemType.Reply:
                            var replyCell = new ReplyCell();
                            replyCell.Bind(item as ReplyViewModel);
                            return replyCell;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                });
            _tableNode.Delegate = new NewsTableDelegate(ViewModel.Items, ViewModel.NewsLocalizedStrings);
            _tableNode.View.AddSubview(_refreshControl);

            TextInput.SetCommand(nameof(TextInput.Started), new RelayCommand<bool>(SetOverlayHidden), false);
            TextInput.SetCommand(nameof(TextInput.Ended), new RelayCommand<bool>(SetOverlayHidden), true);
            TextInput.Font = StyleHelper.Style.BigFont;

            _proxyTextView = new UITextView { Font = StyleHelper.Style.BigFont };

            SendButton.TintColor = StyleHelper.Style.ButtonTintColor;
            SendButton.SetImage(UIImage.FromBundle(StyleHelper.Style.SendBoundleName), UIControlState.Normal);
            SendButton.SetTitle(null, UIControlState.Normal);
            SendButton.SetCommand(new RelayCommand(() =>
            {
                ViewModel.AddCommentCommand.Execute(_simpleImagePicker.GetPickerData());
            }));

            PlaceholderLabel.Font = StyleHelper.Style.BigFont;
            PlaceholderLabel.TextColor = StyleHelper.Style.SeparatorColor;
            PlaceholderLabel.Text = ViewModel.NewsLocalizedStrings.CommentPlaceholder;

            SeparatorView.BackgroundColor = StyleHelper.Style.SeparatorColor;

            ScrollView.SetCommand(nameof(ScrollView.DraggingStarted), new RelayCommand<bool>(SetEditMode), false);

            ViewModel.SetEditModeCommand = new RelayCommand<bool>(SetEditMode);
            ViewModel.ItemAddedCommand = new RelayCommand<int>(ScrollToRow);

            RemoveImageButton.SetImage(UIImage.FromBundle(StyleHelper.Style.ClearButtonBoundleName), UIControlState.Normal);
            RemoveImageButton.SetCommand(new RelayCommand(OnRemoveCommentImage));

            _simpleImagePicker = new SimpleImagePicker(this, ViewModel.PermissionsManager, false);

            CommentImagePreview.Layer.MasksToBounds = false;
            CommentImagePreview.Layer.CornerRadius = 5;
            CommentImagePreview.ClipsToBounds = true;
            CommentImagePreview.ContentMode = UIViewContentMode.ScaleAspectFill;

            AddImageButton.TintColor = StyleHelper.Style.ButtonTintColor;
            AddImageButton.SetImage(UIImage.FromBundle(StyleHelper.Style.AddImageBoundleName), UIControlState.Normal);
            AddImageButton.SetTitle(null, UIControlState.Normal);
            AddImageButton.SetCommand(new RelayCommand(OnOpenPhotosClick));

            TakePhotoButton.TintColor = StyleHelper.Style.ButtonTintColor;
            TakePhotoButton.SetImage(UIImage.FromBundle(StyleHelper.Style.TakePhotoBoundleName), UIControlState.Normal);
            TakePhotoButton.SetTitle(null, UIControlState.Normal);
            TakePhotoButton.SetCommand(new RelayCommand(OnTakePhotoClick));

            ViewModel.ClearInputCommand = new RelayCommand(() =>
            {
                DispatchQueue.MainQueue.DispatchAsync(() =>
                {
                    ViewModel.InputText = null;
                    OnRemoveCommentImage();
                    HeightConstraint.Constant = DefaultInputHeight;
                });
            });

            BusyView.Hidden = false;
            BusyAnimation.SetAnimationNamed("busy_animation.json");
            BusyAnimation.LoopAnimation = true;
            BusyBackground.Image = UIImage.FromBundle(StyleHelper.Style.DetailsPlaceholderBoundleName);
            BusyBackground.ContentMode = UIViewContentMode.ScaleAspectFill;

            EmptyPlaceholder.Hidden = true;
            EmptyPlaceholderImage.Image = UIImage.FromBundle(StyleHelper.Style.EmptyPlaceholderBoundleName);
            EmptyPlaceholderText.Text = ViewModel.NewsLocalizedStrings.EmptyArticle;
            EmptyPlaceholderText.Font = StyleHelper.Style.BigSemiboldFont;
            EmptyPlaceholderText.TextColor = StyleHelper.Style.TextSecondaryColor;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            Messenger.Default.Register<AppInBackgroundMessage>(this, x => ViewModel.OnDisappearing());
            Messenger.Default.Register<AppInForegroundMessage>(this, x => ViewModel.OnAppearing());
        }

        public override void ViewWillDisappear(bool animated)
        {
            Messenger.Default.Unregister<AppInBackgroundMessage>(this);
            Messenger.Default.Unregister<AppInForegroundMessage>(this);

            base.ViewWillDisappear(animated);
        }

        protected override void DoAttachBindings()
        {
            base.DoAttachBindings();

            _overlayViewTapRecognizer = new UITapGestureRecognizer(() => { SetEditMode(false); });
            _longTapRecognizer = new UILongPressGestureRecognizer(OnLongTapRecognizer);
            OverlayView.AddGestureRecognizer(_overlayViewTapRecognizer);
            _tableNode.View.AddGestureRecognizer(_longTapRecognizer);
            TextInput.ShouldChangeText += OnShouldChangeText;

            Bindings.Add(this.SetBinding(() => ViewModel.InputText, () => TextInput.Text, BindingMode.TwoWay));
            Bindings.Add(this.SetBinding(() => ViewModel.InputText).WhenSourceChanges(() =>
            {
                var text = ViewModel.InputText;
                PlaceholderLabel.Hidden = !string.IsNullOrEmpty(text);
            }));
            Bindings.Add(this.SetBinding(() => ViewModel.IsRefreshing).WhenSourceChanges(() =>
            {
                if (!ViewModel.IsRefreshing)
                {
                    _refreshControl.EndRefreshing();
                }
            }));
            Bindings.Add(this.SetBinding(() => ViewModel.IsArticleLoaded).WhenSourceChanges(() =>
            {
                if (ViewModel.IsArticleLoaded)
                {
                    CustomNavigationView.SetCommand(
                        UIImage.FromBundle(StyleHelper.Style.MoreBoundleName),
                        new RelayCommand(OpenMoreActionSheet),
                        false);
                }
                else
                {
                    CustomNavigationView.SetRightBarButtonItem(null, false);
                }
            }));
            Bindings.Add(this.SetBinding(() => _simpleImagePicker.ViewModel.ImageCacheKey).WhenSourceChanges(() =>
            {
                if (string.IsNullOrEmpty(_simpleImagePicker.ViewModel.ImageCacheKey))
                {
                    Execute.BeginOnUIThread(() =>
                    {
                        _previewImageKey = null;
                        CommentImagePreview.Image?.Dispose();
                        CommentImagePreview.Image = null;

                        UpdateImagePreviewState(false);
                    });
                    return;
                }

                if (_simpleImagePicker.ViewModel.ImageCacheKey != null)
                {
                    var key = _simpleImagePicker.ViewModel.ImageCacheKey;
                    if (key == _previewImageKey)
                    {
                        return;
                    }

                    Execute.BeginOnUIThread(() =>
                    {
                        _previewImageKey = key;
                        UpdateImagePreviewState(true);
                        ImageService.Instance
                            .LoadFile(key)
                            .DownSampleInDip(60, 60)
                            .IntoAsync(CommentImagePreview);
                    });
                }
            }));

            Bindings.Add(this.SetBinding(() => ViewModel.IsBusy).WhenSourceChanges(() =>
            {
                if (ViewModel.IsBusy)
                {
                    BusyView.Hidden = false;
                    BusyAnimation.AnimationProgress = 0;
                    BusyAnimation.Play();
                }
                else
                {
                    BusyView.Hidden = true;
                    BusyAnimation.Stop();
                }
            }));

            Bindings.Add(this.SetBinding(() => ViewModel.IsCommentsEnabled).WhenSourceChanges(() =>
            {
                if (ViewModel.IsCommentsEnabled == true)
                {
                    HeightConstraint.Constant = DefaultInputHeight;
                    CommentInputContainer.Hidden = false;
                }
                else
                {
                    HeightConstraint.Constant = 0;
                    CommentInputContainer.Hidden = true;
                }
            }));

            Bindings.Add(this.SetBinding(() => ViewModel.ChannelName, () => CustomNavigationView.Title));

            Bindings.Add(this.SetBinding(() => ViewModel.IsArticleEmpty).WhenSourceChanges(() =>
            {
                if (ViewModel.IsArticleEmpty)
                {
                    EmptyPlaceholder.Hidden = false;
                    CommentInputContainer.Hidden = false;
                }
            }));
        }

        protected override void DoDetachBindings()
        {
            base.DoDetachBindings();

            OverlayView.RemoveGestureRecognizer(_overlayViewTapRecognizer);
            _tableNode.View.RemoveGestureRecognizer(_longTapRecognizer);
            _overlayViewTapRecognizer.Dispose();
            _overlayViewTapRecognizer = null;

            TextInput.ShouldChangeText -= OnShouldChangeText;
        }

        private bool OnShouldChangeText(UITextView textView, NSRange range, string text)
        {
            var oldString = new NSString(textView.Text);
            var newString = oldString.Replace(range, new NSString(text));
            _proxyTextView.Text = newString.ToString();

            var size = _proxyTextView.SizeThatFits(textView.Bounds.Size);
            if (size.Height > _maxHeight && !_calculatedMaxTextViewHeight.HasValue)
            {
                _calculatedMaxTextViewHeight = (int)Math.Ceiling(_maxHeight / _proxyTextView.Font.LineHeight) *
                                               _proxyTextView.Font.LineHeight;
            }

            var height = size.Height > _maxHeight ? _calculatedMaxTextViewHeight.Value : size.Height;

            var delta = height - HeightConstraint.Constant;
            if (delta != 0)
            {
                HeightConstraint.Constant = height;
                if (delta > 0)
                {
                    _tableNode.View.SetContentOffset(new CGPoint(0, _tableNode.View.ContentOffset.Y + delta), true);
                }
            }

            return true;
        }

        private void ScrollToRow(int row)
        {
            //HACK: this delay needs to wait while row adding to UITableView
            //for more details see https://stackoverflow.com/questions/3832474/uitableview-row-animation-duration-and-completion-callback
            Task.Delay(300).ContinueWith(x =>
            {
                DispatchQueue.MainQueue.DispatchAsync(() =>
                {
                    var indexPath = NSIndexPath.FromRowSection(row, 0);
                    _tableNode.View.ScrollToRow(indexPath, UITableViewScrollPosition.Bottom, true);
                });
            });
        }

        private void SetEditMode(bool enabled)
        {
            if (enabled)
            {
                TextInput.BecomeFirstResponder();
            }
            else
            {
                View.EndEditing(false);
            }
        }

        private void SetOverlayHidden(bool hidden)
        {
            OverlayView.Hidden = hidden;
        }

        private void OnRemoveCommentImage()
        {
            _simpleImagePicker.ViewModel.ImageCacheKey = null;
        }

        private void UpdateImagePreviewState(bool isVisible)
        {
            CommentImagePreview.Hidden = !isVisible;
            RemoveImageButton.Hidden = !isVisible;
            TopTextConstraint.Constant = isVisible ? 72 : 4;
        }

        private void OnTakePhotoClick()
        {
            _simpleImagePicker.OpenCameraAsync();
        }

        private void OnOpenPhotosClick()
        {
            _simpleImagePicker.OpenGalleryAsync();
        }

        private void OnLongTapRecognizer()
        {
            if (_longTapRecognizer == null || _tableNode.View == null)
            {
                return;
            }

            var state = _longTapRecognizer.State;
            var location = _longTapRecognizer.LocationInView(_tableNode.View);
            var indexPath = _tableNode.View.IndexPathForRowAtPoint(location);

            if (indexPath != null && state == UIGestureRecognizerState.Began)
            {
                var item = ViewModel.Items[indexPath.Row];

                switch (item)
                {
                    case CommentViewModel comment:
                        OpenActionSheetWithCancel(comment.Options);
                        break;
                    case ReplyViewModel replyComment:
                        OpenActionSheetWithCancel(replyComment.Options);
                        break;
                }
            }
        }

        private void OpenMoreActionSheet()
        {
            if (ViewModel.MoreActions == null)
            {
                return;
            }

            OpenActionSheetWithCancel(ViewModel.MoreActions);
        }

        private void OpenActionSheetWithCancel(IReadOnlyList<CommandAction> commandActions)
        {
            var actions = commandActions.ToList();

            actions.Add(new CommandAction
            {
                Command = new RelayCommand(() => { }),
                Title = ViewModel.NewsLocalizedStrings.Cancel,
                CommandActionStyle = CommandActionStyle.Cancel
            });

            OpenActionSheet(actions);
        }

        private void OpenActionSheet(IList<CommandAction> actions)
        {
            var actionSheet = Dependencies.IocContainer.Resolve<IActionSheet>();
            actionSheet.SetHeader(null);
            actionSheet.SetActions(actions);
            actionSheet.OpenCommand.Execute(null);
        }

        private class NewsTableDelegate : ASTableDelegate
        {
            private readonly IList<IArticleItem> _items;
            private readonly INewsLocalizedStrings _newsLocalizedStrings;

            public NewsTableDelegate(
                IList<IArticleItem> items,
                INewsLocalizedStrings newsLocalizedStrings)
            {
                _items = items;
                _newsLocalizedStrings = newsLocalizedStrings;
            }

            public override UITableViewRowAction[] EditActionsForRowAtIndexPath(UITableView tableView,
                NSIndexPath indexPath)
            {
                var item = _items[indexPath.Row];
                if (!item.CanDelete)
                {
                    return new UITableViewRowAction[0];
                }

                var button = UITableViewRowAction.Create(
                    UITableViewRowActionStyle.Default, _newsLocalizedStrings.Delete,
                    (row, index) => { Messenger.Default.Send(new DeleteCommentMessage(index.Row)); });

                button.BackgroundColor = StyleHelper.Style.AccentColor;

                return new[] { button };
            }
        }
    }
}