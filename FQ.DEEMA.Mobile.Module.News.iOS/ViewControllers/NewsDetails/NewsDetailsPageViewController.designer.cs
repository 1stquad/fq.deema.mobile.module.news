// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FQ.DEEMA.Mobile.Module.News.iOS.ViewControllers.NewsDetails
{
	[Register ("NewsDetailsPageViewController")]
	partial class NewsDetailsPageViewController
	{
		[Outlet]
		UIKit.UIButton AddImageButton { get; set; }

		[Outlet]
		Airbnb.Lottie.LOTAnimationView BusyAnimation { get; set; }

		[Outlet]
		UIKit.UIImageView BusyBackground { get; set; }

		[Outlet]
		UIKit.UIView BusyView { get; set; }

		[Outlet]
		FFImageLoading.Cross.MvxCachedImageView CommentImagePreview { get; set; }

		[Outlet]
		UIKit.UIView CommentInputContainer { get; set; }

		[Outlet]
		UIKit.UIView ContentView { get; set; }

		[Outlet]
		UIKit.UINavigationItem CustomNavigationView { get; set; }

		[Outlet]
		UIKit.UIView EmptyPlaceholder { get; set; }

		[Outlet]
		UIKit.UIImageView EmptyPlaceholderImage { get; set; }

		[Outlet]
		UIKit.UILabel EmptyPlaceholderText { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint HeightConstraint { get; set; }

		[Outlet]
		UIKit.UIView OverlayView { get; set; }

		[Outlet]
		UIKit.UILabel PlaceholderLabel { get; set; }

		[Outlet]
		UIKit.UIButton RemoveImageButton { get; set; }

		[Outlet]
		TPKeyboardAvoiding.TPKeyboardAvoidingScrollView ScrollView { get; set; }

		[Outlet]
		UIKit.UIButton SendButton { get; set; }

		[Outlet]
		UIKit.UIView SeparatorView { get; set; }

		[Outlet]
		UIKit.UIButton TakePhotoButton { get; set; }

		[Outlet]
		UIKit.UITextView TextInput { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint TopTextConstraint { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (AddImageButton != null) {
				AddImageButton.Dispose ();
				AddImageButton = null;
			}

			if (BusyAnimation != null) {
				BusyAnimation.Dispose ();
				BusyAnimation = null;
			}

			if (BusyBackground != null) {
				BusyBackground.Dispose ();
				BusyBackground = null;
			}

			if (BusyView != null) {
				BusyView.Dispose ();
				BusyView = null;
			}

			if (CommentImagePreview != null) {
				CommentImagePreview.Dispose ();
				CommentImagePreview = null;
			}

			if (CommentInputContainer != null) {
				CommentInputContainer.Dispose ();
				CommentInputContainer = null;
			}

			if (ContentView != null) {
				ContentView.Dispose ();
				ContentView = null;
			}

			if (CustomNavigationView != null) {
				CustomNavigationView.Dispose ();
				CustomNavigationView = null;
			}

			if (EmptyPlaceholder != null) {
				EmptyPlaceholder.Dispose ();
				EmptyPlaceholder = null;
			}

			if (EmptyPlaceholderImage != null) {
				EmptyPlaceholderImage.Dispose ();
				EmptyPlaceholderImage = null;
			}

			if (EmptyPlaceholderText != null) {
				EmptyPlaceholderText.Dispose ();
				EmptyPlaceholderText = null;
			}

			if (HeightConstraint != null) {
				HeightConstraint.Dispose ();
				HeightConstraint = null;
			}

			if (OverlayView != null) {
				OverlayView.Dispose ();
				OverlayView = null;
			}

			if (PlaceholderLabel != null) {
				PlaceholderLabel.Dispose ();
				PlaceholderLabel = null;
			}

			if (RemoveImageButton != null) {
				RemoveImageButton.Dispose ();
				RemoveImageButton = null;
			}

			if (ScrollView != null) {
				ScrollView.Dispose ();
				ScrollView = null;
			}

			if (SendButton != null) {
				SendButton.Dispose ();
				SendButton = null;
			}

			if (SeparatorView != null) {
				SeparatorView.Dispose ();
				SeparatorView = null;
			}

			if (TextInput != null) {
				TextInput.Dispose ();
				TextInput = null;
			}

			if (TopTextConstraint != null) {
				TopTextConstraint.Dispose ();
				TopTextConstraint = null;
			}

			if (TakePhotoButton != null) {
				TakePhotoButton.Dispose ();
				TakePhotoButton = null;
			}
		}
	}
}
