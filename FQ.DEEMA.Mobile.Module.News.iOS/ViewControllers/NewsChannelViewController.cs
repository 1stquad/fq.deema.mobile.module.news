﻿using System;
using FQ.DEEMA.Mobile.Module.News.ViewModels;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common.Extensions;
using Softeq.XToolkit.WhiteLabel.iOS;
using Softeq.XToolkit.WhiteLabel.iOS.Extensions;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.News.iOS.ViewControllers
{
    public partial class NewsChannelViewController : ViewControllerBase<NewsChannelViewModel>
    {        
        public NewsChannelViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            CustomNavigationView.SetCommand(UIImage.FromBundle(StyleHelper.Style.BackBoundleName), ViewModel.BackCommand, true);
            CustomNavigationView.Title = ViewModel.Title;

            NewsView.ViewDidLoad(ViewModel.NewsListViewModel);
            NewsView.EmptyAnimationName = "unread_animation.json";
            
            CustomNavigationView.SetCommand(ViewModel.SubscribeActionText,
                StyleHelper.Style.AccentColor, ViewModel.SubscribeCommand, false);
        }

        protected override void DoAttachBindings()
        {
            base.DoAttachBindings();

            Bindings.AddRange(NewsView.DoAttachBindings());
            Bindings.Add(this.SetBinding(() => ViewModel.SubscribeActionText).WhenSourceChanges(() =>
            {
                if (CustomNavigationView.RightBarButtonItem != null)
                {
                    CustomNavigationView.RightBarButtonItem.Title = ViewModel.SubscribeActionText;
                }
            }));
            Bindings.Add(this.SetBinding(() => ViewModel.IsUnSubscribeAvailable).WhenSourceChanges(() =>
            {
                var color = ViewModel.IsUnSubscribeAvailable
                    ? StyleHelper.Style.AccentColor
                    : StyleHelper.Style.TextNormalColor;
                CustomNavigationView.RightBarButtonItem.Enabled = ViewModel.IsUnSubscribeAvailable;
                CustomNavigationView.RightBarButtonItem.TintColor = color;
            }));
        }
    }
}