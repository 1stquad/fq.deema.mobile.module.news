// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FQ.DEEMA.Mobile.Module.News.iOS.ViewControllers
{
	[Register ("NewPostViewController")]
	partial class NewPostViewController
	{
		[Outlet]
		UIKit.UILabel SocialFeaturesLabel { get; set; }

		[Outlet]
		UIKit.UISwitch SocialFeaturesSwitch { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint BottomConstraint { get; set; }

		[Outlet]
		UIKit.UIView CommentSeparator { get; set; }

		[Outlet]
		UIKit.UINavigationItem CustomNavigationView { get; set; }

		[Outlet]
		FQ.DEEMA.Mobile.Module.News.iOS.Controls.SelectDateButtonControl FromButton { get; set; }

		[Outlet]
		TPKeyboardAvoiding.TPKeyboardAvoidingScrollView KeyboardScroll { get; set; }

		[Outlet]
		UIKit.UITextView PostContentInputText { get; set; }

		[Outlet]
		UIKit.UILabel PostContentPlaceholder { get; set; }

		[Outlet]
		FFImageLoading.Cross.MvxCachedImageView PreviewImageView { get; set; }

		[Outlet]
		UIKit.UIActivityIndicatorView ProgressIndicator { get; set; }

		[Outlet]
		UIKit.UIButton RemoveImageButton { get; set; }

		[Outlet]
		UIKit.UIView SeparatorView { get; set; }

		[Outlet]
		UIKit.UITextView TitleInputText { get; set; }

		[Outlet]
		UIKit.UILabel TitlePlaceholder { get; set; }

		[Outlet]
		FQ.DEEMA.Mobile.Module.News.iOS.Controls.SelectDateButtonControl ToButton { get; set; }

		[Outlet]
		UIKit.UIToolbar ToolbarView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (BottomConstraint != null) {
				BottomConstraint.Dispose ();
				BottomConstraint = null;
			}

			if (CustomNavigationView != null) {
				CustomNavigationView.Dispose ();
				CustomNavigationView = null;
			}

			if (FromButton != null) {
				FromButton.Dispose ();
				FromButton = null;
			}

			if (KeyboardScroll != null) {
				KeyboardScroll.Dispose ();
				KeyboardScroll = null;
			}

			if (PostContentInputText != null) {
				PostContentInputText.Dispose ();
				PostContentInputText = null;
			}

			if (PostContentPlaceholder != null) {
				PostContentPlaceholder.Dispose ();
				PostContentPlaceholder = null;
			}

			if (PreviewImageView != null) {
				PreviewImageView.Dispose ();
				PreviewImageView = null;
			}

			if (ProgressIndicator != null) {
				ProgressIndicator.Dispose ();
				ProgressIndicator = null;
			}

			if (RemoveImageButton != null) {
				RemoveImageButton.Dispose ();
				RemoveImageButton = null;
			}

			if (SeparatorView != null) {
				SeparatorView.Dispose ();
				SeparatorView = null;
			}

			if (TitleInputText != null) {
				TitleInputText.Dispose ();
				TitleInputText = null;
			}

			if (TitlePlaceholder != null) {
				TitlePlaceholder.Dispose ();
				TitlePlaceholder = null;
			}

			if (ToButton != null) {
				ToButton.Dispose ();
				ToButton = null;
			}

			if (ToolbarView != null) {
				ToolbarView.Dispose ();
				ToolbarView = null;
			}

			if (CommentSeparator != null) {
				CommentSeparator.Dispose ();
				CommentSeparator = null;
			}

			if (SocialFeaturesSwitch != null) {
				SocialFeaturesSwitch.Dispose ();
				SocialFeaturesSwitch = null;
			}

			if (SocialFeaturesLabel != null) {
				SocialFeaturesLabel.Dispose ();
				SocialFeaturesLabel = null;
			}
		}
	}
}
