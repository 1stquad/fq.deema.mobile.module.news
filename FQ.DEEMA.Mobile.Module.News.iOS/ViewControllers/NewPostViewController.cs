﻿using System;
using FFImageLoading;
using FQ.DEEMA.Mobile.Module.News.ViewModels;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.WhiteLabel.iOS;
using Softeq.XToolkit.WhiteLabel.iOS.Extensions;
using Softeq.XToolkit.WhiteLabel.iOS.ImagePicker;
using Softeq.XToolkit.WhiteLabel.Threading;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.News.iOS.ViewControllers
{
    public partial class NewPostViewController : ViewControllerBase<NewPostViewModel>
    {
        private SimpleImagePicker _simpleImagePicker;
        private string _previewImageKey;

        public NewPostViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            SeparatorView.BackgroundColor = StyleHelper.Style.SeparatorColor;

            CustomNavigationView.SetCommand(UIImage.FromBundle(StyleHelper.Style.CloseButtonImageBoundleName), ViewModel.CancelCommand, true);
            CustomNavigationView.SetCommand(UIImage.FromBundle(StyleHelper.Style.SendBoundleName), new RelayCommand(OnPostClicked), false);
            CustomNavigationView.Title = ViewModel.Header;

            FromButton.Initialize(ViewModel.NewsLocalizedStrings.DateFrom, ViewModel.DateFrom);
            FromButton.SetCommand(nameof(FromButton.Toggled), new RelayCommand(TryCloseToView));
            FromButton.SetMinTime(ViewModel.MinDateFrom, false);

            ToButton.Initialize(ViewModel.NewsLocalizedStrings.DateTo, ViewModel.DateEnd);
            ToButton.SetCommand(nameof(ToButton.Toggled), new RelayCommand(TryCloseFromView));
            ToButton.SetMinTime(ViewModel.MinDateEnd, false);

            TitlePlaceholder.Font = StyleHelper.Style.BigFont;
            TitlePlaceholder.TextColor = StyleHelper.Style.SeparatorColor;
            TitlePlaceholder.Text = ViewModel.NewsLocalizedStrings.NewPostTitlePlaceholder;

            TitleInputText.Font = StyleHelper.Style.BigFont;
            TitleInputText.TextColor = StyleHelper.Style.TextNormalColor;

            PostContentPlaceholder.Font = StyleHelper.Style.BigFont;
            PostContentPlaceholder.TextColor = StyleHelper.Style.SeparatorColor;
            PostContentPlaceholder.Text = ViewModel.NewsLocalizedStrings.NewPostContentPlaceholder;

            PostContentInputText.Font = StyleHelper.Style.BigFont;
            PostContentInputText.TextColor = StyleHelper.Style.TextNormalColor;

            var barItems = new[]
            {
                new UIBarButtonItem(UIImage.FromBundle(StyleHelper.Style.TakePhotoBoundleName), UIBarButtonItemStyle.Plain, null),
                new UIBarButtonItem(UIImage.FromBundle(StyleHelper.Style.AddImageBoundleName), UIBarButtonItemStyle.Plain, null)
            };

            ToolbarView.SetItems(barItems, false);
            barItems[0].SetCommand("Clicked", new RelayCommand(OnOpenCameraClick));
            barItems[1].SetCommand("Clicked", new RelayCommand(OnOpenPhotosClick));

            RemoveImageButton.SetImage(UIImage.FromBundle(StyleHelper.Style.ClearButtonBoundleName), UIControlState.Normal);
            RemoveImageButton.SetCommand(new RelayCommand(OnRemoveClick));

            _simpleImagePicker = new SimpleImagePicker(this, ViewModel.PermissionsManager, false);

            PreviewImageView.Layer.MasksToBounds = false;
            PreviewImageView.Layer.CornerRadius = 5;
            PreviewImageView.ClipsToBounds = true;
            PreviewImageView.ContentMode = UIViewContentMode.ScaleAspectFill;

            ProgressIndicator.Color = StyleHelper.Style.AccentColor;
            ProgressIndicator.HidesWhenStopped = true;

            ViewModel.EndEditingCommand = new RelayCommand(() => { View.EndEditing(true); });

            KeyboardScroll.KeyboardDismissMode = UIScrollViewKeyboardDismissMode.OnDrag;

            CommentSeparator.BackgroundColor = StyleHelper.Style.SeparatorColor;

            SocialFeaturesLabel.Font = StyleHelper.Style.BigFont;
            SocialFeaturesLabel.TextColor = StyleHelper.Style.TextNormalColor;
            SocialFeaturesLabel.Text = ViewModel.NewsLocalizedStrings.EnableSocialFeatures;

            SocialFeaturesSwitch.OnTintColor = StyleHelper.Style.AccentColor;
        }

        protected override void DoAttachBindings()
        {
            base.DoAttachBindings();

            Bindings.Add(this.SetBinding(() => ViewModel.Title, () => TitleInputText.Text, BindingMode.TwoWay));
            Bindings.Add(this.SetBinding(() => ViewModel.Title).WhenSourceChanges(() =>
            {
                var text = ViewModel.Title;
                TitlePlaceholder.Hidden = !string.IsNullOrEmpty(text);
            }));

            Bindings.Add(this.SetBinding(() => ViewModel.Content, () => PostContentInputText.Text, BindingMode.TwoWay));
            Bindings.Add(this.SetBinding(() => ViewModel.Content).WhenSourceChanges(() =>
            {
                var text = ViewModel.Content;
                PostContentPlaceholder.Hidden = !string.IsNullOrEmpty(text);
            }));

            Bindings.Add(this.SetBinding(() => _simpleImagePicker.ViewModel.ImageCacheKey)
                .WhenSourceChanges(SetPreviewImage));

            Bindings.Add(this.SetBinding(() => FromButton.DateTimeOffset)
                .ObserveSourceEvent(nameof(FromButton.DateValueChanged))
                .WhenSourceChanges(() =>
                {
                    View.EndEditing(true);
                    ToButton.SetMinTime(FromButton.DateTimeOffset, true);
                    ViewModel.DateFrom = FromButton.DateTimeOffset;
                }));

            Bindings.Add(this.SetBinding(() => ToButton.DateTimeOffset)
                .ObserveSourceEvent(nameof(ToButton.DateValueChanged))
                .WhenSourceChanges(() =>
                {
                    View.EndEditing(true);
                    ViewModel.DateEnd = ToButton.DateTimeOffset;
                }));

            Bindings.Add(this.SetBinding(() => ToButton.DateTimeOffset)
                .ObserveSourceEvent(nameof(ToButton.DateValueChanged))
                .WhenSourceChanges(() => { View.EndEditing(true); }));

            Bindings.Add(this.SetBinding(() => ViewModel.IsBusy).WhenSourceChanges(() =>
            {
                if (ViewModel.IsBusy)
                {
                    ProgressIndicator.StartAnimating();
                }
                else
                {
                    ProgressIndicator.StopAnimating();
                }
            }));

            Bindings.Add(this.SetBinding(() => ViewModel.SocialFeaturesEnabled, () => SocialFeaturesSwitch.On,
                BindingMode.TwoWay));

            TitleInputText.Started += OnTextInputGotFocus;
            PostContentInputText.Started += OnTextInputGotFocus;
        }

        protected override void DoDetachBindings()
        {
            base.DoDetachBindings();

            TitleInputText.Started -= OnTextInputGotFocus;
            PostContentInputText.Started -= OnTextInputGotFocus;
        }

        private void SetPreviewImage()
        {
            if (string.IsNullOrEmpty(_simpleImagePicker.ViewModel.ImageCacheKey) && string.IsNullOrEmpty(ViewModel.ImageUrl))
            {
                Execute.BeginOnUIThread(() =>
                {
                    ClearPreviewImage();
                    UpdateImagePreviewState(false);
                });
                return;
            }

            if (_simpleImagePicker.ViewModel.ImageCacheKey != null)
            {
                ViewModel.ImageUrl = null;
                var key = _simpleImagePicker.ViewModel.ImageCacheKey;
                if (key == _previewImageKey)
                {
                    return;
                }

                Execute.BeginOnUIThread(() =>
                {
                    _previewImageKey = key;
                    UpdateImagePreviewState(true);
                    ImageService.Instance
                        .LoadFile(key)
                        .DownSampleInDip(60, 60)
                        .IntoAsync(PreviewImageView);
                });

                return;
            }

            if (ViewModel.ImageUrl != null)
            {
                Execute.BeginOnUIThread(() =>
                {
                    var key = ViewModel.ImageUrl;
                    if (key == _previewImageKey)
                    {
                        return;
                    }

                    _previewImageKey = key;
                    UpdateImagePreviewState(true);
                    ImageService.Instance
                        .LoadUrl(ViewModel.ImageUrl)
                        .DownSampleInDip(60, 60)
                        .IntoAsync(PreviewImageView);
                });
            }
        }

        private void ClearPreviewImage()
        {
            _previewImageKey = null;
            PreviewImageView.Image?.Dispose();
            PreviewImageView.Image = null;
        }

        private void TryCloseFromView()
        {
            if (ToButton.IsInEditMode && FromButton.IsInEditMode)
            {
                FromButton.Toggle();
            }
        }

        private void TryCloseToView()
        {
            if (ToButton.IsInEditMode && FromButton.IsInEditMode)
            {
                ToButton.Toggle();
            }
        }

        private void OnRemoveClick()
        {
            _simpleImagePicker.ViewModel.ImageCacheKey = null;
            ViewModel.ImageUrl = null;
            SetPreviewImage();
        }

        private void UpdateImagePreviewState(bool isVisible)
        {
            PreviewImageView.Hidden = !isVisible;
            RemoveImageButton.Hidden = !isVisible;
            BottomConstraint.Constant = isVisible ? 76 : 16;
        }

        private void OnPostClicked()
        {
            ViewModel.PostCommand.Execute(_simpleImagePicker.StreamFunc);
        }

        private void OnOpenPhotosClick()
        {
            _simpleImagePicker.OpenGalleryAsync();
        }

        private void OnOpenCameraClick()
        {
            _simpleImagePicker.OpenCameraAsync();
        }

        private void OnTextInputGotFocus(object sender, EventArgs e)
        {
            if (ToButton.IsInEditMode)
            {
                ToButton.Toggle(true);
            }

            if (FromButton.IsInEditMode)
            {
                FromButton.Toggle(true);
            }
        }
    }
}