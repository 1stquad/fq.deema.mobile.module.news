// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FQ.DEEMA.Mobile.Module.News.iOS.ViewControllers
{
	[Register ("NewsChannelsViewController")]
	partial class NewsChannelsViewController
	{
		[Outlet]
		UIKit.UINavigationItem NavigationView { get; set; }

		[Outlet]
		UIKit.UITableView TableView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (NavigationView != null) {
				NavigationView.Dispose ();
				NavigationView = null;
			}

			if (TableView != null) {
				TableView.Dispose ();
				TableView = null;
			}
		}
	}
}
