// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FQ.DEEMA.Mobile.Module.News.iOS.ViewControllers
{
	[Register ("NewsViewController")]
	partial class NewsViewController
	{
		[Outlet]
		UIKit.UINavigationItem CustomNavigationItem { get; set; }

		[Outlet]
		FQ.DEEMA.Mobile.Module.News.iOS.Controls.NewsView NewsView { get; set; }

		[Outlet]
		FQ.DEEMA.Mobile.Module.News.iOS.TabViewControl.TabControl TabControlView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (CustomNavigationItem != null) {
				CustomNavigationItem.Dispose ();
				CustomNavigationItem = null;
			}

			if (NewsView != null) {
				NewsView.Dispose ();
				NewsView = null;
			}

			if (TabControlView != null) {
				TabControlView.Dispose ();
				TabControlView = null;
			}
		}
	}
}
