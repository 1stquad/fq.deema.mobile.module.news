﻿using System;
using UIKit;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common.Extensions;
using Softeq.XToolkit.WhiteLabel.iOS;
using Softeq.XToolkit.WhiteLabel.iOS.Extensions;
using Softeq.XToolkit.WhiteLabel.iOS.Shared.Extensions;
using Softeq.XToolkit.WhiteLabel.Messages;
using Softeq.XToolkit.WhiteLabel.Messenger;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.ViewModels;
using System.Collections.Generic;
using Softeq.XToolkit.Bindings.Extensions;

namespace FQ.DEEMA.Mobile.Module.News.iOS.ViewControllers
{
    public partial class NewsViewController : ViewControllerBase<NewsViewModel>
    {
        private readonly IndexToNewsTypeConverter _indexToNewsTypeConverter = new IndexToNewsTypeConverter();

        private UIImageView _titleImage;
        private Binding<int, int> _selectedTabBinding;
        private UITapGestureRecognizer _titleTapRecognizer;

        public NewsViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            NewsView.ViewDidLoad(ViewModel.NewsListViewModel);

            TabControlView.InitializeView(ViewModel.Labels);
            TabControlView.SelectedIndex = _indexToNewsTypeConverter.ConvertValueBack(ViewModel.NewsType);
            TabControlView.SetCommand(nameof(TabControlView.TabReselected), NewsView.ScrollToTopCommand);

            _titleImage = new UIImageView
            {
                Image = UIImage.FromBundle(StyleHelper.Style.AppLogoBoundleName),
                UserInteractionEnabled = true
            };

            CustomNavigationItem.SetCommand(UIImage.FromBundle(StyleHelper.Style.ChannelsBoundleName),
                ViewModel.LeftCommand, true);
            CustomNavigationItem.AddTitleView(_titleImage);
            CustomNavigationItem.SetSettingsButtonAsync(ViewModel.UserName, ViewModel.UserPhotoUrl, ViewModel.RightCommand,
                StyleHelper.Style.ProfileAvatarStyles);
        }

        protected override void DoAttachBindings()
        {
            base.DoAttachBindings();

            _selectedTabBinding = this.SetBinding(() => TabControlView.SelectedIndex).WhenSourceChanges(() =>
            {
                ViewModel.NewsType = _indexToNewsTypeConverter.ConvertValue(TabControlView.SelectedIndex);
                NewsView.EmptyAnimationName = ViewModel.NewsType == NewsType.MyNews
                    ? "read_animation.json"
                    : "unread_animation.json";

                UpdateViewModelForNewsView(ViewModel.NewsListViewModel);
            });

            _titleTapRecognizer = new UITapGestureRecognizer(() => { NewsView.ScrollToTopCommand.Execute(this); });
            _titleImage.AddGestureRecognizer(_titleTapRecognizer);

            Messenger.Default.Register<TabReselectedMessage>(this, OnTabReselectedMessage);
        }

        protected override void DoDetachBindings()
        {
            base.DoDetachBindings();

            Messenger.Default.Unregister(this);

            _selectedTabBinding?.Detach();
            _selectedTabBinding = null;

            _titleImage.RemoveGestureRecognizer(_titleTapRecognizer);
            _titleTapRecognizer?.Dispose();
            _titleTapRecognizer = null;
        }

        private void OnTabReselectedMessage(TabReselectedMessage message)
        {
            NewsView.ScrollToTopCommand.Execute(this);
        }

        private void UpdateViewModelForNewsView(NewsListViewModel newsListViewModel)
        {
            NewsView.SetViewModel(newsListViewModel);
            ReAttachBindingsForNewsView();
        }

        private void ReAttachBindingsForNewsView()
        {
            Bindings.DetachAllAndClear();
            Bindings.AddRange(NewsView.DoAttachBindings());
        }
    }
}