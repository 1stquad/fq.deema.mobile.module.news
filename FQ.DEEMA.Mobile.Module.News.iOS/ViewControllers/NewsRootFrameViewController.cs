﻿using FQ.DEEMA.Mobile.Module.News.ViewModels;
using Softeq.XToolkit.WhiteLabel.iOS.Navigation;

namespace FQ.DEEMA.Mobile.Module.News.iOS.ViewControllers
{
    public class NewsRootFrameViewController : RootFrameNavigationControllerBase<NewsRootFrameViewModel>
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            NavigationBar.Hidden = true;
        }
    }
}