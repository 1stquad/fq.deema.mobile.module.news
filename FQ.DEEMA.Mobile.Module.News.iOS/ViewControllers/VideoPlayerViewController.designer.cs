// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FQ.DEEMA.Mobile.Module.News.iOS.ViewControllers
{
	[Register ("VideoPlayerViewController")]
	partial class VideoPlayerViewController
	{
		[Outlet]
		UIKit.UIButton CloseButton { get; set; }

		[Outlet]
		WebKit.WKWebView WebViewPlayer { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (CloseButton != null) {
				CloseButton.Dispose ();
				CloseButton = null;
			}

			if (WebViewPlayer != null) {
				WebViewPlayer.Dispose ();
				WebViewPlayer = null;
			}
		}
	}
}
