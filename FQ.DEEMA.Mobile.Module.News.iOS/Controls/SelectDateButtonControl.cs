﻿using System;
using System.Globalization;
using Foundation;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Common.Interfaces;
using Softeq.XToolkit.Common.iOS.Extensions;
using Softeq.XToolkit.Common.iOS.Helpers;
using Softeq.XToolkit.WhiteLabel;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.News.iOS.Controls
{
    [Register("SelectDateButtonControl")]
    public class SelectDateButtonControl : UIStackView
    {
        private readonly Lazy<ILogger> _loggerLazy = LogManager.GetLoggerLazy<SelectDateButtonControl>();
        private readonly Lazy<string> _dateFormatLazy;

        private UIDatePicker _datePicker;
        private DateTimeOffset _dateTimeOffset;
        private UIView _separator;
        private UILabel _valueLabel;

        public SelectDateButtonControl(IntPtr handle) : base(handle)
        {
            Axis = UILayoutConstraintAxis.Vertical;

            _dateFormatLazy = new Lazy<string>(() =>
            {
                CultureInfo cultureInfo;
                try
                {
                    cultureInfo = CultureInfo.GetCultureInfo(NSLocale.CurrentLocale.LocaleIdentifier.Replace('_', '-'));
                }
                catch (Exception ex)
                {
                    _loggerLazy.Value.Warn(ex);
                    cultureInfo = CultureInfo.CurrentUICulture;
                }

                var monthDayPattern = cultureInfo.DateTimeFormat.MonthDayPattern;
                var dayPattern = monthDayPattern.StartsWith("d", StringComparison.InvariantCultureIgnoreCase)
                    ? "d MMM"
                    : "MMM d";

                var template = NSDateFormatter.GetDateFormatFromTemplate("j", 0, NSLocale.CurrentLocale);
                var timeFormat = LocaleHelper.Is24HourFormat
                    ? "HH:mm"
                    : "hh:mm tt";

                return $"ddd {dayPattern} {timeFormat}";
            });

        }

        public DateTimeOffset DateTimeOffset
        {
            get => _dateTimeOffset;
            private set
            {
                _dateTimeOffset = value;
                OnDateChanged();
            }
        }

        public bool IsInEditMode { get; private set; }

        public event EventHandler DateValueChanged;

        public event EventHandler Toggled;

        public void SetMinTime(DateTimeOffset dateTimeOffset, bool alignDateValue)
        {
            var newDate = dateTimeOffset.ToNSDateLocal();

            if (alignDateValue)
            {
                var datePickerValue = _datePicker.Date.ToDateTimeOffsetLocal();
                if (dateTimeOffset > datePickerValue)
                {
                    _datePicker.Date = newDate;
                    DateTimeOffset = dateTimeOffset;
                }
            }

            _datePicker.MinimumDate = newDate;
        }

        public void Initialize(string labelText, DateTimeOffset defaultValue)
        {
            var buttonView = new UIView
            {
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            AddArrangedSubview(buttonView);

            NSLayoutConstraint.ActivateConstraints(new[]
            {
                buttonView.HeightAnchor.ConstraintEqualTo(46)
            });

            var button = UIButton.FromType(UIButtonType.System);
            button.AddAsSubviewWithParentSize(buttonView);
            button.SetCommand(new RelayCommand(Toggle));

            var separatorView = new UIView
            {
                BackgroundColor = StyleHelper.Style.SeparatorColor,
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            AddArrangedSubview(separatorView);

            NSLayoutConstraint.ActivateConstraints(new[]
            {
                separatorView.HeightAnchor.ConstraintEqualTo(0.5f)
            });

            _datePicker = new UIDatePicker
            {
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            AddArrangedSubview(_datePicker);

            NSLayoutConstraint.ActivateConstraints(new[]
            {
                _datePicker.HeightAnchor.ConstraintEqualTo(200)
            });

            _datePicker.Hidden = true;

            _separator = new UIView
            {
                BackgroundColor = StyleHelper.Style.SeparatorColor,
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            AddArrangedSubview(_separator);

            NSLayoutConstraint.ActivateConstraints(new[]
            {
                _separator.HeightAnchor.ConstraintEqualTo(0.5f)
            });

            _separator.Hidden = true;

            //label
            var label = new UILabel
            {
                Text = labelText,
                TranslatesAutoresizingMaskIntoConstraints = false,
                Font = StyleHelper.Style.BigFont
            };

            buttonView.AddSubview(label);

            NSLayoutConstraint.ActivateConstraints(new[]
            {
                label.LeftAnchor.ConstraintEqualTo(buttonView.LeftAnchor, 14),
                label.CenterYAnchor.ConstraintEqualTo(buttonView.CenterYAnchor)
            });

            //value
            _valueLabel = new UILabel
            {
                TranslatesAutoresizingMaskIntoConstraints = false,
                Font = StyleHelper.Style.BigFont,
                TextColor = StyleHelper.Style.ButtonTintColor
            };

            buttonView.AddSubview(_valueLabel);

            NSLayoutConstraint.ActivateConstraints(new[]
            {
                _valueLabel.RightAnchor.ConstraintEqualTo(buttonView.RightAnchor, -14),
                _valueLabel.CenterYAnchor.ConstraintEqualTo(buttonView.CenterYAnchor)
            });

            _datePicker.Date = defaultValue.ToNSDateLocal();
            DateTimeOffset = defaultValue;
        }

        public void Toggle()
        {
            Toggle(false);
        }

        public void Toggle(bool skipEndEditing)
        {
            if (!skipEndEditing)
            {
                Superview.EndEditing(true);
            }

            IsInEditMode = !IsInEditMode;

            _datePicker.Hidden = !IsInEditMode;
            _separator.Hidden = !IsInEditMode;
            _valueLabel.TextColor = IsInEditMode ? StyleHelper.Style.AccentColor : StyleHelper.Style.ButtonTintColor;

            if (IsInEditMode)
            {
                _datePicker.ValueChanged += OnDateChanged;
            }
            else
            {
                _datePicker.ValueChanged -= OnDateChanged;
            }

            Toggled?.Invoke(this, EventArgs.Empty);
        }

        private void OnDateChanged(object sender, EventArgs e)
        {
            DateTimeOffset = _datePicker.Date.ToDateTimeOffsetLocal();
        }

        private void OnDateChanged()
        {
            _valueLabel.Text =
                DateTimeOffset.LocalDateTime.ToString(_dateFormatLazy.Value, CultureInfo.InvariantCulture);
            DateValueChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}