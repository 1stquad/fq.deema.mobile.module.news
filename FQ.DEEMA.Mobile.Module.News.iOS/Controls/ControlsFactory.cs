﻿using AsyncDisplayKitBindings;
using CoreGraphics;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.News.iOS.Controls
{
    public static class ControlsFactory
    {
        public static ASControlNode CreatePlayButtonNode()
        {
            var playNode = new ASButtonNode
            {
                BackgroundColor = UIColor.White,
            };

            var playNodeSize = new ASDimension { value = 60, unit = ASDimensionUnit.Points };

            playNode.Style.Height = playNodeSize;
            playNode.Style.Width = playNodeSize;

            // shadow and radius
            playNode.Layer.ShadowColor = UIColor.LightGray.CGColor;
            playNode.Layer.ShadowOffset = CGSize.Empty;
            playNode.Layer.MasksToBounds = false;
            playNode.Layer.ShadowRadius = 10.0f;
            playNode.Layer.ShadowOpacity = 0.8f;
            playNode.Layer.CornerRadius = playNodeSize.value / 2f;

            playNode.SetBackgroundImage(UIImage.FromBundle(StyleHelper.Style.PlayImageBoundleName), UIControlState.Normal);

            return playNode;
        }
    }
}
