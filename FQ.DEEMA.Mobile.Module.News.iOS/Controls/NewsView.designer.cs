// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FQ.DEEMA.Mobile.Module.News.iOS.Controls
{
	partial class NewsView
	{
		[Outlet]
		UIKit.UIButton AddNewsButton { get; set; }

		[Outlet]
		UIKit.UIView AddNewsView { get; set; }

		[Outlet]
		Airbnb.Lottie.LOTAnimationView BusyAnimation { get; set; }

		[Outlet]
		UIKit.UIImageView BusyImageBackground { get; set; }

		[Outlet]
		UIKit.UIView BusyOverlayView { get; set; }

		[Outlet]
		UIKit.UIView ContentView { get; set; }

		[Outlet]
		Airbnb.Lottie.LOTAnimationView EmptyAnimationContainer { get; set; }

		[Outlet]
		UIKit.UILabel NoNewsLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (AddNewsButton != null) {
				AddNewsButton.Dispose ();
				AddNewsButton = null;
			}

			if (AddNewsView != null) {
				AddNewsView.Dispose ();
				AddNewsView = null;
			}

			if (BusyAnimation != null) {
				BusyAnimation.Dispose ();
				BusyAnimation = null;
			}

			if (BusyImageBackground != null) {
				BusyImageBackground.Dispose ();
				BusyImageBackground = null;
			}

			if (BusyOverlayView != null) {
				BusyOverlayView.Dispose ();
				BusyOverlayView = null;
			}

			if (ContentView != null) {
				ContentView.Dispose ();
				ContentView = null;
			}

			if (EmptyAnimationContainer != null) {
				EmptyAnimationContainer.Dispose ();
				EmptyAnimationContainer = null;
			}

			if (NoNewsLabel != null) {
				NoNewsLabel.Dispose ();
				NoNewsLabel = null;
			}
		}
	}
}
