using System;
using System.Collections.Generic;
using AsyncDisplayKitBindings;
using CoreFoundation;
using CoreGraphics;
using Foundation;
using FQ.DEEMA.Mobile.Module.News.iOS.Cells;
using FQ.DEEMA.Mobile.Module.News.ViewModels;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Common.EventArguments;
using Softeq.XToolkit.Common.iOS.Extensions;
using Softeq.XToolkit.WhiteLabel.iOS.Controls;
using Softeq.XToolkit.WhiteLabel.iOS.Shared;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.News.iOS.Controls
{
    [Register("NewsView")]
    public partial class NewsView : CustomViewBase
    {
        private readonly ASTableNode _tableNode = new ASTableNode(UITableViewStyle.Plain);

        private UIRefreshControl _refreshControl;

        public NewsView(IntPtr handle) : base(handle)
        {
        }

        public NewsListViewModel ViewModel { get; private set; }

        public RelayCommand ScrollToTopCommand { get; private set; }

        public string EmptyAnimationName { get; set; }

        public void ViewDidLoad(NewsListViewModel newsListViewModel)
        {
            SetViewModel(newsListViewModel);

            ScrollToTopCommand = new RelayCommand(() =>
            {
                DispatchQueue.MainQueue.DispatchAsync(() =>
                {
                    var indexPath = NSIndexPath.FromRowSection(0, 0);
                    _tableNode.View.ScrollToRow(indexPath, UITableViewScrollPosition.Top, true);
                });
            });

            _refreshControl = new UIRefreshControl();
            _refreshControl.SetCommand(nameof(_refreshControl.ValueChanged), new RelayCommand(() =>
            {
                // ViewModel may be changed
                ViewModel.RefreshCommand.Execute(null);
            }));
            _refreshControl.TintColor = StyleHelper.Style.AccentColor;

            _tableNode.BackgroundColor = UIColor.Clear;
            _tableNode.View.AddAsSubviewWithParentSize(ContentView);
            _tableNode.View.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            _tableNode.View.AddSubview(_refreshControl);

            AddNewsView.BackgroundColor = StyleHelper.Style.ContentColor;
            AddNewsView.Layer.CornerRadius = AddNewsView.Frame.Height / 2;
            AddNewsView.Layer.ShadowColor = UIColor.Black.CGColor;
            AddNewsView.Layer.ShadowOpacity = 0.28f;
            AddNewsView.Layer.ShadowRadius = 3f;
            AddNewsView.Layer.ShadowOffset = new CGSize(0, 2);

            AddNewsButton.SetImage(UIImage.FromBundle(StyleHelper.Style.AddBoundleName), UIControlState.Normal);
            AddNewsButton.TintColor = StyleHelper.Style.ButtonTintColor;
            AddNewsButton.SetCommand(ViewModel.NewPostCommand);

            BusyOverlayView.Hidden = false;
            BusyAnimation.SetAnimationNamed("busy_animation.json");
            BusyAnimation.LoopAnimation = true;
            BusyImageBackground.Image = UIImage.FromBundle(StyleHelper.Style.FeedPlaceholderBoundleName);
            BusyImageBackground.ContentMode = UIViewContentMode.ScaleAspectFill;

            EmptyAnimationContainer.Hidden = true;
            NoNewsLabel.Hidden = true;
            EmptyAnimationContainer.ContentMode = UIViewContentMode.ScaleAspectFit;

            NoNewsLabel.Text = ViewModel.NoNewsText;
            NoNewsLabel.Font = StyleHelper.Style.BigSemiboldFont;
            NoNewsLabel.TextColor = StyleHelper.Style.TextSecondaryColor;
        }

        public void SetViewModel(NewsListViewModel newsListViewModel)
        {
            ViewModel = newsListViewModel;

            _tableNode.DataSource?.Dispose();
            _tableNode.DataSource = null;
            _tableNode.Delegate = null;

            var ds = new ObservableASTableDataSource<NewsHeaderViewModel>(
                ViewModel.News,
                _tableNode,
                (index, dataSource) =>
                {
                    var cell = new NewsHeaderCell();
                    var viewModel = dataSource[index];
                    cell.BindCell(viewModel);
                    return cell;
                });
            ds.SetCommand(nameof(ds.DataReloaded), new RelayCommand(() =>
            {
                _tableNode.View.SetContentOffset(CGPoint.Empty, false);
            }));
            _tableNode.DataSource = ds;

            var newsTableDelegate = new NewsTableDelegate();
            newsTableDelegate.SetCommand(nameof(newsTableDelegate.LastItemRequested), new RelayCommand(OnLastItemRequested));
            newsTableDelegate.SetCommandWithArgs(
                nameof(newsTableDelegate.RowSelected),
                new RelayCommand<GenericEventArgs<NewsHeaderViewModel>>(x =>
                {
                    ViewModel.NavigateToDetails(x.Value);
                }));
            _tableNode.Delegate = newsTableDelegate;

            _tableNode.ReloadData();
        }

        public IList<Binding> DoAttachBindings()
        {
            return new List<Binding>
            {
                this.SetBinding(() => ViewModel.IsRefreshing).WhenSourceChanges(() =>
                {
                    if (!ViewModel.IsRefreshing)
                    {
                        _refreshControl.EndRefreshing();
                    }
                }),
                this.SetBinding(() => ViewModel.IsBusy).WhenSourceChanges(() =>
                {
                    if (ViewModel.IsBusy)
                    {
                        BusyOverlayView.Hidden = false;
                        BusyAnimation.Stop();
                        BusyAnimation.AnimationProgress = 0;
                        BusyAnimation.Play();

                        SetEmptyContainerState(false, EmptyAnimationName);
                    }
                    else
                    {
                        BusyOverlayView.Hidden = true;
                        BusyAnimation.Stop();

                        SetEmptyContainerState(!ViewModel.HasData, EmptyAnimationName);
                    }
                }),
                this.SetBinding(() => ViewModel.HasData).WhenSourceChanges(() =>
                {
                    if (ViewModel.IsBusy)
                    {
                        return;
                    }

                    SetEmptyContainerState(!ViewModel.HasData, EmptyAnimationName);
                }),
                this.SetBinding(() => ViewModel.CanPostNews, () => AddNewsView.Hidden).ConvertSourceToTarget(x => !x)
            };
        }

        private void SetEmptyContainerState(bool isEmpty, string animationName)
        {
            if (animationName == null)
            {
                EmptyAnimationContainer.Hidden = true;
                NoNewsLabel.Hidden = true;
                return;
            }

            if (isEmpty)
            {
                EmptyAnimationContainer.Hidden = false;
                NoNewsLabel.Hidden = false;
                EmptyAnimationContainer.Stop();
                EmptyAnimationContainer.SetAnimationNamed(animationName);
                EmptyAnimationContainer.AnimationProgress = 0;
                EmptyAnimationContainer.Play();
            }
            else
            {
                EmptyAnimationContainer.Hidden = true;
                NoNewsLabel.Hidden = true;
                EmptyAnimationContainer.Stop();
            }
        }

        private void OnLastItemRequested()
        {
            ViewModel.LoadNextPage();
        }
    }
}