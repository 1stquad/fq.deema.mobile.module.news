﻿using System;
using System.Collections.Generic;
using AsyncDisplayKitBindings;
using CoreGraphics;
using Foundation;
using FQ.DEEMA.Mobile.Module.News.Models;
using ObjCRuntime;
using Softeq.XToolkit.Common.iOS.Extensions;
using Softeq.XToolkit.Common.iOS.Helpers;
using Softeq.XToolkit.WhiteLabel;
using Softeq.XToolkit.WhiteLabel.Navigation;
using Softeq.XToolkit.WhiteLabel.ViewModels;
using UIKit;
using System.Linq;
using Softeq.XToolkit.WhiteLabel.iOS.Shared.Extensions;
using System.Threading.Tasks;
using FFImageLoading.Work;
using FFImageLoading;
using Softeq.XToolkit.WhiteLabel.Threading;

namespace FQ.DEEMA.Mobile.Module.News.iOS.Controls
{
    public class CommentNode : ASDisplayNode
    {
        private const int AvatarSize = 30;

        private readonly ASImageNode _avatarImageNode;
        private readonly ASImageNode _imageNode;
        private readonly ASButtonNode _likeButtonNode;
        private readonly ASTextNode _messageTextNode = new ASTextNode();
        private readonly ASTextNode _nameTextNode = new ASTextNode();
        private readonly ASButtonNode _replyButtonNode;
        private readonly ASTextNode _timeSpanTextNode = new ASTextNode();
        private readonly ASDisplayNode _spaceNode = new ASDisplayNode();

        private bool _hasImage;
        private FullScreenImageOptions _imageFullScreenViewerOptions;

        public CommentNode()
        {
            _avatarImageNode = new ASImageNode
            {
                ImageModificationBlock = image =>
                {
                    var size = new CGSize(AvatarSize, AvatarSize);
                    return image.MakeCircularImageWithSize(size);
                }
            };

            _replyButtonNode = new ASButtonNode();
            _replyButtonNode.AddTarget(this, new Selector(nameof(OnReplyPressed)),
                ASControlNodeEvent.TouchUpInside);

            _likeButtonNode = new ASButtonNode();
            _likeButtonNode.AddTarget(this, new Selector(nameof(OnLikePressed)),
                ASControlNodeEvent.TouchUpInside);

            _imageNode = new ASImageNode
            {
                ContentMode = UIViewContentMode.ScaleAspectFill
            };
            _imageNode.AddTarget(this, new Selector(nameof(OnImageTapped)),
                ASControlNodeEvent.TouchUpInside);
        }

        public event EventHandler ReplyPressed;
        public event EventHandler LikePressed;

        public void Bind<T>(ICommentViewModel<T> viewModel) where T : MessageBase
        {
            _avatarImageNode.LoadImageWithTextPlaceholder(
                viewModel.Model.PublisherImageUrl,
                viewModel.Model.PublisherName,
                StyleHelper.Style.AvatarStyles);

            _nameTextNode.AttributedText = viewModel.Model.PublisherName
                .BuildAttributedString()
                .Font(StyleHelper.Style.NormalSemiboldFont)
                .Foreground(StyleHelper.Style.TextNormalColor);

            if (!string.IsNullOrEmpty(viewModel.Model.Message))
            {
                _messageTextNode.AttributedText = viewModel.Model.Message
                    .BuildAttributedString()
                    .Font(StyleHelper.Style.NormalFont)
                    .Foreground(StyleHelper.Style.TextNormalColor)
                    .HighlightStrings(viewModel.Mentions.Select(x => new NSRange(x.Start, x.Length)),
                        StyleHelper.Style.AccentColor);
            }

            var converter = new DateTimeToStringConverter(LocaleHelper.Is24HourFormat);

            _timeSpanTextNode.AttributedText = converter.ConvertValue(viewModel.Model.DateCreated)
                .BuildAttributedString()
                .Font(StyleHelper.Style.SmallFont)
                .Foreground(StyleHelper.Style.TextSecondaryColor);

            _likeButtonNode.SetTitle(
                string.Empty,
                StyleHelper.Style.SmallFont,
                StyleHelper.Style.ButtonTintColor,
                UIControlState.Normal);
            _likeButtonNode.SetImage(UIImage.FromBundle(StyleHelper.Style.LikeBoundleName), UIControlState.Normal);
            _likeButtonNode.ContentVerticalAlignment = ASVerticalAlignment.AlignmentBottom;

            _replyButtonNode.SetImage(UIImage.FromBundle(StyleHelper.Style.ReplyBoundleName), UIControlState.Normal);
            _replyButtonNode.SetTitle(viewModel.NewsLocalizedStrings.Reply,
                StyleHelper.Style.SmallFont, StyleHelper.Style.TextSecondaryColor, UIControlState.Normal);
            _replyButtonNode.ContentVerticalAlignment = ASVerticalAlignment.AlignmentBottom;

            _hasImage = viewModel.Model.HasImage;
            TryLoadImage(viewModel);

            _imageFullScreenViewerOptions = new FullScreenImageOptions
            {
                CloseButtonTintColor = StyleHelper.Style.ButtonTintColor.ToHex(),
                ImagePath = viewModel.ImageCacheKey,
                ImageUrl = viewModel.ImageUrl,
                IosCloseButtonImageBoundleName = StyleHelper.Style.CloseButtonImageBoundleName
            };

            AutomaticallyManagesSubnodes = true;
            SetNeedsLayout();
        }

        public void SetLikesState(bool isLikedByMe, string likeCountText)
        {
            var color = isLikedByMe ? StyleHelper.Style.AccentColor : StyleHelper.Style.ButtonTintColor;
            var image = isLikedByMe
                ? UIImage.FromBundle(StyleHelper.Style.LikeAccentBoundleName)
                : UIImage.FromBundle(StyleHelper.Style.LikeBoundleName);

            _likeButtonNode.SetTitle(
                likeCountText,
                StyleHelper.Style.SmallFont,
                color,
                UIControlState.Normal);

            _likeButtonNode.SetImage(image, UIControlState.Normal);
        }

        public override ASLayoutSpec LayoutSpecThatFits(ASSizeRange constrainedSize)
        {
            _nameTextNode.Style.FlexShrink = 1;
            _messageTextNode.Style.FlexShrink = 1;
            _spaceNode.Style.FlexGrow = 1;

            var actionsLayout = new ASStackLayoutSpec
            {
                Direction = ASStackLayoutDirection.Horizontal,
                Children = new IASLayoutElement[]
                {
                    _timeSpanTextNode,
                    _spaceNode,
                    _likeButtonNode,
                    _replyButtonNode
                },
                AlignItems = ASStackLayoutAlignItems.End,
                Spacing = 16
            };

            var deatilsItems = new List<IASLayoutElement>
            {
                _nameTextNode,
                _messageTextNode,
                actionsLayout
            };

            if (_hasImage)
            {
                _imageNode.Style.Height = new ASDimension { value = 150, unit = ASDimensionUnit.Points };
                var imagePlaceholder = new ASImageNode
                {
                    Image = UIImage.FromBundle(StyleHelper.Style.ImagePlaceholderBoundleName)
                };
                imagePlaceholder.Style.Height = new ASDimension { value = 150, unit = ASDimensionUnit.Points };
                var imageContainer = ASBackgroundLayoutSpec.BackgroundLayoutSpecWithChild(_imageNode, imagePlaceholder);
                imageContainer.Style.FlexGrow = 1;
                imageContainer.Style.SpacingBefore = 4;
                imageContainer.Style.SpacingAfter = 4;
                deatilsItems.Insert(2, imageContainer);
            }

            var detailsLayout = new ASStackLayoutSpec
            {
                Direction = ASStackLayoutDirection.Vertical,
                Children = deatilsItems.ToArray()
            };
            detailsLayout.Style.SpacingBefore = 10;
            detailsLayout.Style.FlexShrink = 1;
            detailsLayout.Style.FlexGrow = 1;

            _avatarImageNode.Style.Height = new ASDimension { value = AvatarSize, unit = ASDimensionUnit.Points };
            _avatarImageNode.Style.Width = new ASDimension { value = AvatarSize, unit = ASDimensionUnit.Points };

            var cellLayout = new ASStackLayoutSpec
            {
                Direction = ASStackLayoutDirection.Horizontal,
                Children = new IASLayoutElement[]
                {
                    _avatarImageNode,
                    detailsLayout
                }
            };

            return ASInsetLayoutSpec.InsetLayoutSpecWithInsets(new UIEdgeInsets(0, 10, 0, 0), cellLayout);
        }

        public void TryLoadImage<T>(ICommentViewModel<T> viewModel) where T : MessageBase
        {
            Task.Run(async () =>
            {
                if (viewModel == null)
                {
                    return;
                }

                var expr = default(TaskParameter);

                if (!string.IsNullOrEmpty(viewModel.ImageCacheKey))
                {
                    expr = ImageService.Instance.LoadFile(viewModel.ImageCacheKey);
                }
                else if (!string.IsNullOrEmpty(viewModel.ImageUrl))
                {
                    expr = ImageService.Instance.LoadUrl(viewModel.ImageUrl);
                }

                expr = expr.DownSampleInDip(90, 90);

                var image = default(UIImage);

                try
                {
                    image = await expr.AsUIImageAsync().ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    LogManager.LogError<CommentNode>(ex);
                }

                if (image == null)
                {
                    return;
                }

                Execute.BeginOnUIThread(() =>
                {
                    _imageNode.Image = image;

                    SetNeedsLayout();
                    Layout();
                });
            });
        }

        [Export("OnReplyPressed")]
        private void OnReplyPressed()
        {
            ReplyPressed?.Invoke(this, EventArgs.Empty);
        }

        [Export("OnLikePressed")]
        private void OnLikePressed()
        {
            LikePressed?.Invoke(this, EventArgs.Empty);
        }

        [Export("OnImageTapped")]
        private void OnImageTapped()
        {
            if (string.IsNullOrEmpty(_imageFullScreenViewerOptions.ImageUrl) &&
                string.IsNullOrEmpty(_imageFullScreenViewerOptions.ImagePath))
            {
                return;
            }

            Dependencies.IocContainer
                .Resolve<IDialogsService>()
                .ShowForViewModel<FullScreenImageViewModel, FullScreenImageOptions>(_imageFullScreenViewerOptions);
        }
    }
}