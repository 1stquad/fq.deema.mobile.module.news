using System;
using AsyncDisplayKitBindings;
using Foundation;
using FQ.DEEMA.Mobile.Module.News.iOS.Cells;
using Softeq.XToolkit.Common;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.WhiteLabel.Threading;
using UIKit;
using WebKit;

namespace FQ.DEEMA.Mobile.Module.News.iOS.Controls
{
    public class WebViewViewNode : ASDisplayNode
    {
        private RelayCommand<string> _navigateToLinkCommand;
        private string _html;
        private float _height;
        private WeakReferenceEx<BodyCell> _parentRef;

        public WebViewViewNode(ASDisplayNodeViewBlock initBlock) : base(initBlock)
        {
        }
        
        private WKWebView WebView => View as WKWebView;

        public void Initialize(BodyCell bodyCell, RelayCommand<string> navigateToUrlCommand)
        {
            _parentRef = WeakReferenceEx.Create(bodyCell);
            _navigateToLinkCommand = navigateToUrlCommand;
        }

        public void SetHtmlContent(string value)
        {
            _html = value;
        }

        private async void SetHeight(float value)
        {
            if (Math.Abs(_height - value) < double.Epsilon)
            {
                return;
            }

            _height = value;
            await _parentRef.Target.NotifySizeChanged(_height).ConfigureAwait(false);
            Execute.BeginOnUIThread(() =>
            {
                WebView.LoadHtmlString(_html, NSBundle.MainBundle.BundleUrl);
            });
        }

        public override void DidLoad()
        {
            WebView.ScrollView.ScrollEnabled = false;
            WebView.NavigationDelegate = new WebViewWkNavigationDelegate(this);
            WebView.ScrollView.Delegate = new WebViewUiScrollViewDelegate();
        }

        public override void DidEnterVisibleState()
        {
            base.DidEnterVisibleState();

            WebView.LoadHtmlString(_html, NSBundle.MainBundle.BundleUrl);
        }

        private class WebViewUiScrollViewDelegate : UIScrollViewDelegate
        {
            public override UIView ViewForZoomingInScrollView(UIScrollView scrollView)
            {
                return null;
            }

            public override void ZoomingStarted(UIScrollView scrollView, UIView view)
            {
                scrollView.PinchGestureRecognizer.Enabled = false;
            }
        }

        private class WebViewWkNavigationDelegate : WKNavigationDelegate
        {
            private readonly WeakReferenceEx<WebViewViewNode> _parentRef;

            public WebViewWkNavigationDelegate(WebViewViewNode parent)
            {
                _parentRef = WeakReferenceEx.Create(parent);
            }

            public override async void DidFinishNavigation(WKWebView webView, WKNavigation navigation)
            {
                await webView.EvaluateJavaScriptAsync("document.readyState");
                var scrollHeightResult = await webView.EvaluateJavaScriptAsync("document.body.scrollHeight");
                var nsNumber = (NSNumber)scrollHeightResult;

                _parentRef.Target.SetHeight(nsNumber.FloatValue);
                _parentRef.Target.WebView.ScrollView.ZoomScale = 1;
            }

            public override void DecidePolicy(WKWebView webView, WKNavigationAction navigationAction, Action<WKNavigationActionPolicy> decisionHandler)
            {
                if (navigationAction.NavigationType == WKNavigationType.LinkActivated)
                {
                    _parentRef.Target.NavigateToUrl(navigationAction.Request.Url.AbsoluteString);
                    decisionHandler(WKNavigationActionPolicy.Cancel);
                    return;
                }

                decisionHandler(WKNavigationActionPolicy.Allow);
            }
        }

        private void NavigateToUrl(string urlAbsoluteString)
        {
            _navigateToLinkCommand.Execute(urlAbsoluteString);
        }
    }
}