﻿using Autofac;

namespace FQ.DEEMA.Mobile.Module.News.iOS
{
    public static class BootstrapperIos
    {
        public static void Configure(ContainerBuilder containerBuilder)
        {
            Bootstrapper.Configure(containerBuilder);
        }
    }
}