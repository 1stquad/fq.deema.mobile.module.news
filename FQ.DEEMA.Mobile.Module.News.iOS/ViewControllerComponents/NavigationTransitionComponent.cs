﻿using CoreAnimation;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.WhiteLabel.iOS;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.News.iOS.ViewControllerComponents
{
    public class NavigationTransitionComponent : IViewControllerComponent
    {
        private readonly bool _fromLeft;
        private WeakReferenceEx<UIViewController> _controller;

        public NavigationTransitionComponent(string key, bool fromLeft)
        {
            Key = key;
            _fromLeft = fromLeft;
        }

        public string Key { get; }

        public void Attach(UIViewController controller)
        {
            var transition = new CATransition
            {
                Duration = 0.5,
                TimingFunction = CAMediaTimingFunction.FromName(CAMediaTimingFunction.EaseInEaseOut),
                Type = CAAnimation.TransitionPush,
                Subtype = _fromLeft ? CAAnimation.TransitionFromLeft : CAAnimation.TransitionFromRight
            };

            _controller = WeakReferenceEx.Create(controller);

            transition.SetCommand<CAAnimationStateEventArgs>(nameof(transition.AnimationStopped),
                new RelayCommand(() => { Detach(_controller.Target); }));

            controller.View.Layer.AddAnimation(transition, null);
        }

        public void Detach(UIViewController controller)
        {
            controller?.View.Layer.RemoveAllAnimations();
        }
    }
}