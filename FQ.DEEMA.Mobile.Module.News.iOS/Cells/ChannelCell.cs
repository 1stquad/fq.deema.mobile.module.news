﻿using System;
using Foundation;
using FQ.DEEMA.Mobile.Module.News.ViewModels;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.News.iOS.Cells
{
    public partial class ChannelCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("ChannelCell");
        public static readonly UINib Nib;

        private ChannelViewModel _channelViewModel;

        static ChannelCell()
        {
            Nib = UINib.FromName("ChannelCell", NSBundle.MainBundle);
        }

        protected ChannelCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public void BindCell(ChannelViewModel channel)
        {
            SelectionStyle = UITableViewCellSelectionStyle.Default;
            ChannelIcon.Image = UIImage.FromBundle(StyleHelper.Style.ChannelBoundleName);
            Label.Font = StyleHelper.Style.BigFont;
            Label.TextColor = StyleHelper.Style.TextNormalColor;
            Label.Text = channel.Name;
            SeparatorView.BackgroundColor = StyleHelper.Style.SeparatorColor;
            NextIcon.Image = UIImage.FromBundle(StyleHelper.Style.ArrowBoundleName);
            NextIcon.Hidden = channel.Channel.Channels.Count == 0;
            ChildrenButton.Hidden = NextIcon.Hidden;

            _channelViewModel = channel;

            ChannelIcon.TintColor = _channelViewModel.Channel.HasSubscribedChannels 
                ? StyleHelper.Style.AccentColor 
                : StyleHelper.Style.ButtonTintColor;
        }

        partial void OnChildrenClick(NSObject sender)
        {
            _channelViewModel.NavigateToChannels();
        }

        partial void OnDetailsClick(NSObject sender)
        {
            _channelViewModel.NavigateToChannel();
        }
    }
}