﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using AsyncDisplayKitBindings;
using CoreGraphics;
using FFImageLoading;
using Foundation;
using FQ.DEEMA.Mobile.Module.News.iOS.Controls;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.Services;
using FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails;
using ObjCRuntime;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common;
using Softeq.XToolkit.Common.iOS.Extensions;
using Softeq.XToolkit.Common.iOS.Helpers;
using Softeq.XToolkit.WhiteLabel;
using Softeq.XToolkit.WhiteLabel.iOS.Shared.Extensions;
using UIKit;
using WebKit;

namespace FQ.DEEMA.Mobile.Module.News.iOS.Cells
{
    public class BodyCell : ASCellNode
    {
        private const int AvatarSize = 30;

        private readonly ASTextNode _commentsLabelNode;
        private readonly WebViewViewNode _webView;
        private readonly ASDisplayNode _emptyView;
        private readonly ASImageNode _imageNode;
        private readonly ASButtonNode _likeButtonNode;
        private readonly ASTextNode _publishDateNode;
        private readonly ASTextNode _delimiterNode;
        private readonly ASButtonNode _channelsNode;
        private readonly ASImageNode _publisherImageNode;
        private readonly ASTextNode _publisherNameNode;
        private readonly ASTextNode _publishTimeSpanNode;
        private readonly ASButtonNode _readButtonNode;
        private readonly ASDisplayNode _separatorNode;
        private readonly ASTextNode _titleNode;

        private Binding _isLikedByMeBinding;
        private Binding _isReadByMeBinding;
        private WeakReferenceEx<BodyViewModel> _viewModelRef;
        private float _webViewHeight;
        private TaskCompletionSource<bool> _completionSource;
        private TaskCompletionSource<bool> _waitingForFirstLayoutCall;

        public Task NotifySizeChanged(float value)
        {
            _completionSource = new TaskCompletionSource<bool>();
            _webViewHeight = value;
            SetNeedsLayout();
            LayoutIfNeeded();
            return _completionSource.Task;
        }

        public BodyCell()
        {
            _titleNode = new ASTextNode();
            _publishDateNode = new ASTextNode();
            _delimiterNode = new ASTextNode();
            _channelsNode = new ASButtonNode();
            _imageNode = new ASImageNode { ContentMode = UIViewContentMode.ScaleAspectFill };
            _publisherImageNode = new ASImageNode
            {
                ImageModificationBlock = image =>
                {
                    var size = new CGSize(AvatarSize, AvatarSize);
                    return image.MakeCircularImageWithSize(size);
                }
            };
            _publisherNameNode = new ASTextNode();
            _publishTimeSpanNode = new ASTextNode();
            _likeButtonNode = new ASButtonNode();
            _likeButtonNode.AddTarget(this, new Selector(nameof(OnLikePressed)),
                ASControlNodeEvent.TouchUpInside);
            _separatorNode = new ASDisplayNode();
            _commentsLabelNode = new ASTextNode();
            _readButtonNode = new ASButtonNode();
            _readButtonNode.AddTarget(this, new Selector(nameof(OnReadPressed)),
                ASControlNodeEvent.TouchUpInside);
            _emptyView = new ASDisplayNode();
            _webView = new WebViewViewNode(() =>
            {
                var view = new WKWebView(CGRect.Empty, new WKWebViewConfiguration());
                return view;
            });
            _waitingForFirstLayoutCall = new TaskCompletionSource<bool>();
        }

        public void BindCell(BodyViewModel viewModel)
        {
            _viewModelRef = WeakReferenceEx.Create(viewModel);

            BackgroundColor = StyleHelper.Style.ContentColor;

            if (viewModel.Model.Title != null)
            {
                _titleNode.AttributedText = viewModel.Model.Title
                    .BuildAttributedString()
                    .Font(StyleHelper.Style.BigSemiboldFont)
                    .Foreground(StyleHelper.Style.TextNormalColor);
            }

            _publishDateNode.AttributedText = viewModel.Model.PublishDate
                .ToString(viewModel.NewsLocalizedStrings.DateFormat, CultureInfo.InvariantCulture)
                .BuildAttributedString()
                .Font(StyleHelper.Style.SmallFont)
                .Foreground(StyleHelper.Style.TextSecondaryColor);

            _delimiterNode.AttributedText = "|"
                .BuildAttributedString()
                .Font(StyleHelper.Style.SmallFont)
                .Foreground(StyleHelper.Style.TextSecondaryColor);

            _channelsNode.SetTitle(
                viewModel.RelatedChannels,
                StyleHelper.Style.SmallFont,
                StyleHelper.Style.AccentColor,
                UIControlState.Normal);

            if (viewModel.Model.Description != null)
            {
                _webView.SetHtmlContent(viewModel.HtmlContent);
                _webView.Initialize(this, viewModel.NavigateToUrlCommand);
            }

            TryLoadImageAsync();

            _publisherImageNode.LoadImageWithTextPlaceholder(
                viewModel.Model.PublisherImageUrl,
                viewModel.Model.PublisherName,
                StyleHelper.Style.AvatarStyles);

            _publisherNameNode.AttributedText = viewModel.Model.PublisherName
                .BuildAttributedString()
                .Font(StyleHelper.Style.NormalSemiboldFont)
                .Foreground(StyleHelper.Style.TextNormalColor);

            var converter = new DateTimeToStringConverter(LocaleHelper.Is24HourFormat);
            _publishTimeSpanNode.AttributedText = converter.ConvertValue(viewModel.Model.PublishDate)
                .BuildAttributedString()
                .Font(StyleHelper.Style.SmallFont)
                .Foreground(StyleHelper.Style.TextSecondaryColor);

            _likeButtonNode.ContentVerticalAlignment = ASVerticalAlignment.AlignmentBottom;
            _likeButtonNode.ContentHorizontalAlignment = ASHorizontalAlignment.AlignmentRight;
            _likeButtonNode.Hidden = !viewModel.IsLikesEnabled;

            _readButtonNode.SetTitle(viewModel.NewsLocalizedStrings.Read, StyleHelper.Style.SmallFont,
                StyleHelper.Style.ButtonTintColor, UIControlState.Normal);
            _readButtonNode.ContentVerticalAlignment = ASVerticalAlignment.AlignmentCenter;

            _commentsLabelNode.Hidden = !viewModel.IsCommentsEnabled;
            _commentsLabelNode.AttributedText = viewModel.NewsLocalizedStrings.Comments
                .BuildAttributedString()
                .Font(StyleHelper.Style.BigSemiboldFont)
                .Foreground(StyleHelper.Style.TextNormalColor);

            _separatorNode.BackgroundColor = StyleHelper.Style.SeparatorColor;

            SelectionStyle = UITableViewCellSelectionStyle.None;

            AutomaticallyManagesSubnodes = true;
            SetNeedsLayout();
        }

        public override ASLayoutSpec LayoutSpecThatFits(ASSizeRange constrainedSize)
        {
            _titleNode.Style.FlexShrink = 1;
            _titleNode.Style.SpacingBefore = 18;

            _channelsNode.Style.FlexShrink = 1;
            _channelsNode.TitleNode.MaximumNumberOfLines = 1;
            _channelsNode.TitleNode.TruncationMode = UILineBreakMode.TailTruncation;

            _webView.Style.Height = new ASDimension { value = _webViewHeight, unit = ASDimensionUnit.Points };
            _webView.Style.SpacingBefore = 10;

            _publisherImageNode.Style.Height = new ASDimension { value = AvatarSize, unit = ASDimensionUnit.Points };
            _publisherImageNode.Style.Width = new ASDimension { value = AvatarSize, unit = ASDimensionUnit.Points };

            _channelsNode.AddTarget(this, new Selector(nameof(OnChannelsPressed)), ASControlNodeEvent.TouchUpInside);

            var nameLayout = new ASStackLayoutSpec
            {
                Direction = ASStackLayoutDirection.Vertical,
                Children = new IASLayoutElement[]
                {
                    _publisherNameNode,
                    _publishTimeSpanNode
                }
            };
            nameLayout.Style.FlexShrink = 1;
            nameLayout.Style.SpacingBefore = 7;

            _likeButtonNode.Style.Width = new ASDimension { value = 64f, unit = ASDimensionUnit.Points };
            _emptyView.Style.FlexGrow = 1;

            var dateStackView = new ASStackLayoutSpec
            {
                Direction = ASStackLayoutDirection.Horizontal,
                Children = new IASLayoutElement[]
                {
                    _publishDateNode,
                    _delimiterNode,
                    _channelsNode
                },
                Spacing = 10
            };
            dateStackView.Style.SpacingBefore = 10;

            var readButtonLayout = ASInsetLayoutSpec.InsetLayoutSpecWithInsets(new UIEdgeInsets(4, 18, 0, 0), _readButtonNode);

            var publisherLayout = new ASStackLayoutSpec
            {
                Direction = ASStackLayoutDirection.Horizontal,
                Children = new IASLayoutElement[]
                {
                    _publisherImageNode,
                    nameLayout,
                    _emptyView,
                    _likeButtonNode,
                    readButtonLayout
                },
                AlignItems = ASStackLayoutAlignItems.Center
            };
            publisherLayout.Style.SpacingBefore = 30;

            _separatorNode.Style.Height = new ASDimension { value = 0.5f, unit = ASDimensionUnit.Points };
            _separatorNode.Style.SpacingBefore = 16;

            _commentsLabelNode.Style.SpacingBefore = 15;

            var bodyLayout = new ASStackLayoutSpec
            {
                Direction = ASStackLayoutDirection.Vertical,
                Children = new IASLayoutElement[]
                {
                    _titleNode,
                    dateStackView,
                    _webView,
                    publisherLayout,
                    _separatorNode,
                    _commentsLabelNode
                }
            };

            _imageNode.Style.Width = new ASDimension { unit = ASDimensionUnit.Points, value = constrainedSize.max.Width };

            var cellLayout = new ASStackLayoutSpec
            {
                Direction = ASStackLayoutDirection.Vertical,
                Children = new IASLayoutElement[]
                {
                    CreateImageLayout(),
                    ASInsetLayoutSpec.InsetLayoutSpecWithInsets(new UIEdgeInsets(0, 10, 20, 10), bodyLayout)
                }
            };

            _waitingForFirstLayoutCall.TrySetResult(true);

            return cellLayout;
        }

        public override void LayoutDidFinish()
        {
            base.LayoutDidFinish();
            _completionSource?.TrySetResult(true);
        }

        public override void DidEnterDisplayState()
        {
            base.DidEnterDisplayState();

            _isLikedByMeBinding = this.SetBinding(() => _viewModelRef.Target.LikeViewModel.IsLiked).WhenSourceChanges(
                () =>
                {
                    SetLikesState(
                        _viewModelRef.Target.LikeViewModel.IsLiked,
                        _viewModelRef.Target.LikeViewModel.LikeCountText);
                });

            _isReadByMeBinding = this.SetBinding(() => _viewModelRef.Target.IsReadByUser).WhenSourceChanges(() =>
            {
                SetReadState(_viewModelRef.Target.IsReadByUser);
            });
        }

        public override void DidExitDisplayState()
        {
            base.DidExitDisplayState();

            _isLikedByMeBinding.Detach();
            _isLikedByMeBinding = null;

            _isReadByMeBinding.Detach();
            _isReadByMeBinding = null;
        }

        private void SetLikesState(bool isLikedByMe, string likeCountText)
        {
            var color = isLikedByMe ? StyleHelper.Style.AccentColor : StyleHelper.Style.ButtonTintColor;
            var image = isLikedByMe
                ? UIImage.FromBundle(StyleHelper.Style.LikeAccentBoundleName)
                : UIImage.FromBundle(StyleHelper.Style.LikeBoundleName);

            _likeButtonNode.SetTitle(
                likeCountText,
                StyleHelper.Style.SmallFont,
                color,
                UIControlState.Normal);

            _likeButtonNode.SetImage(image, UIControlState.Normal);
        }

        private void SetReadState(bool isRead)
        {
            var color = isRead ? StyleHelper.Style.AccentColor : StyleHelper.Style.ButtonTintColor;
            var image = isRead
                ? UIImage.FromBundle(StyleHelper.Style.AccentCheckmarkBoundleName)
                : UIImage.FromBundle(StyleHelper.Style.CheckmarkBoundleName);

            _readButtonNode.SetTitle(
                _viewModelRef.Target.NewsLocalizedStrings.Read,
                StyleHelper.Style.SmallFont,
                color,
                UIControlState.Normal);

            _readButtonNode.SetImage(
                image,
                UIControlState.Normal);
        }

        private async void TryLoadImageAsync()
        {
            if (_viewModelRef.Target.HasImage)
            {
                UIImage newsImage = null;
                try
                {
                    newsImage = await ImageService.Instance
                        .LoadUrl(_viewModelRef.Target.Model.ImageUrl)
                        .AsUIImageAsync();
                }
                catch (Exception ex)
                {
                    LogManager.LogError<BodyCell>(ex);
                }

                if (newsImage == null)
                {
                    return;
                }

                var imageSize = newsImage.Size;

                await _waitingForFirstLayoutCall.Task;

                _imageNode.Style.Height = new ASDimension
                {
                    value = _imageNode.Style.Width.value * imageSize.Height / imageSize.Width,
                    unit = ASDimensionUnit.Points
                };

                _imageNode.Image = newsImage;

                SetNeedsLayout();
                LayoutIfNeeded();
            }
        }

        private IASLayoutElement CreateImageLayout()
        {
            IASLayoutElement imageLayout = _imageNode;

            if (_viewModelRef.Target.HasImage && _viewModelRef.Target.HasVideo)
            {
                return CreatePlayButtonLayout(imageLayout);
            }

            if (_viewModelRef.Target.HasVideo)
            {
                var placeholderLayout = new ASDisplayNode();
                placeholderLayout.BackgroundColor = UIColor.FromRGB(248, 248, 248);
                placeholderLayout.Style.Height = new ASDimension { value = 170, unit = ASDimensionUnit.Points };
                return CreatePlayButtonLayout(placeholderLayout);
            }

            return imageLayout;
        }

        private IASLayoutElement CreatePlayButtonLayout(IASLayoutElement parentNode)
        {
            var playNode = ControlsFactory.CreatePlayButtonNode();
            playNode.AddTarget(this, new Selector(nameof(OnPlayPressed)), ASControlNodeEvent.TouchUpInside);

            var playButtonLayout = ASCenterLayoutSpec.CenterLayoutSpecWithCenteringOptions(
                ASCenterLayoutSpecCenteringOptions.Xy,
                ASCenterLayoutSpecSizingOptions.MinimumXY,
                playNode);

            return ASOverlayLayoutSpec.OverlayLayoutSpecWithChild(parentNode, playButtonLayout);
        }

        [Export(nameof(OnLikePressed))]
        private void OnLikePressed()
        {
            _viewModelRef.Target?.LikeCommand.Execute(this);
        }

        [Export(nameof(OnReadPressed))]
        private void OnReadPressed()
        {
            _viewModelRef.Target?.ReadCommand.Execute(this);
        }

        [Export(nameof(OnPlayPressed))]
        private void OnPlayPressed()
        {
            Dependencies.IocContainer
                .Resolve<ILaunchVideoService>()
                .OpenPlayer(_viewModelRef.Target.Model.VideoUrl);
        }

        [Export(nameof(OnChannelsPressed))]
        private void OnChannelsPressed() => _viewModelRef.Target.OpenChannelsCommand.Execute(null);
    }
}