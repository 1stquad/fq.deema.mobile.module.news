﻿using AsyncDisplayKitBindings;
using FQ.DEEMA.Mobile.Module.News.iOS.Controls;
using FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common;
using Softeq.XToolkit.Common.Command;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.News.iOS.Cells
{
    public class CommentCell : ASCellNode
    {
        private readonly CommentNode _commentNode;
        private Binding _imageBinding;

        private Binding _isLikedByMeBinding;
        private WeakReferenceEx<CommentViewModel> _viewModelRef;

        public CommentCell()
        {
            _commentNode = new CommentNode();
        }

        public void Bind(CommentViewModel viewModel)
        {
            _commentNode.Bind(viewModel);

            _commentNode.SetCommand(nameof(_commentNode.ReplyPressed), new RelayCommand(OnReplyPressed));
            _commentNode.SetCommand(nameof(_commentNode.LikePressed), new RelayCommand(OnLikePressed));

            _viewModelRef = WeakReferenceEx.Create(viewModel);

            SelectionStyle = UITableViewCellSelectionStyle.None;
            AutomaticallyManagesSubnodes = true;
            SetNeedsLayout();
        }

        public override ASLayoutSpec LayoutSpecThatFits(ASSizeRange constrainedSize)
        {
            return ASInsetLayoutSpec.InsetLayoutSpecWithInsets(new UIEdgeInsets(0, 0, 10, 10), _commentNode);
        }

        public override void DidEnterDisplayState()
        {
            base.DidEnterDisplayState();

            _isLikedByMeBinding = this.SetBinding(() => _viewModelRef.Target.LikeViewModel.IsLiked).WhenSourceChanges(
                () =>
                {
                    _commentNode.SetLikesState(
                        _viewModelRef.Target.LikeViewModel.IsLiked,
                        _viewModelRef.Target.LikeViewModel.LikeCountText);
                });

            _imageBinding = this.SetBinding(() => _viewModelRef.Target.ImageUrl).WhenSourceChanges(() =>
            {
                _commentNode.TryLoadImage(_viewModelRef.Target);
            });
        }

        public override void DidExitDisplayState()
        {
            base.DidExitDisplayState();

            _isLikedByMeBinding.Detach();
            _isLikedByMeBinding = null;

            _imageBinding.Detach();
            _imageBinding = null;
        }

        private void OnReplyPressed()
        {
            var vm = _viewModelRef.Target;
            if (vm == null)
            {
                return;
            }

            vm.ReplyCommand.Execute(vm.Model);
        }

        private void OnLikePressed()
        {
            var vm = _viewModelRef.Target;
            if (vm == null)
            {
                return;
            }

            vm.LikeCommand.Execute(vm);
        }
    }
}