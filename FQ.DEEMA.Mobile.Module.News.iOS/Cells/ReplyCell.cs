﻿using AsyncDisplayKitBindings;
using FQ.DEEMA.Mobile.Module.News.iOS.Controls;
using FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common;
using Softeq.XToolkit.Common.Command;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.News.iOS.Cells
{
    public class ReplyCell : ASCellNode
    {
        private readonly CommentNode _commentNode;
        private readonly ASDisplayNode _lineNode = new ASDisplayNode();
        private Binding _imageBinding;

        private Binding _isLikedByMeBinding;
        private WeakReferenceEx<ReplyViewModel> _viewModelRef;

        public ReplyCell()
        {
            _commentNode = new CommentNode();
        }

        public void Bind(ReplyViewModel viewModel)
        {
            _commentNode.Bind(viewModel);
            _commentNode.BackgroundColor = StyleHelper.Style.ContentColor;

            _commentNode.SetCommand(nameof(_commentNode.ReplyPressed), new RelayCommand(OnReplyPressed));
            _commentNode.SetCommand(nameof(_commentNode.LikePressed), new RelayCommand(OnLikePressed));

            _viewModelRef = WeakReferenceEx.Create(viewModel);
            _lineNode.BackgroundColor = StyleHelper.Style.AccentColor;

            SelectionStyle = UITableViewCellSelectionStyle.None;
            AutomaticallyManagesSubnodes = true;
            SetNeedsLayout();
        }

        public override ASLayoutSpec LayoutSpecThatFits(ASSizeRange constrainedSize)
        {
            var cellLayout = ASBackgroundLayoutSpec.BackgroundLayoutSpecWithChild(
                ASInsetLayoutSpec.InsetLayoutSpecWithInsets(new UIEdgeInsets(0, 1f, 0, 0), _commentNode),
                _lineNode);

            return ASInsetLayoutSpec.InsetLayoutSpecWithInsets(new UIEdgeInsets(0, 50, 10, 10), cellLayout);
        }

        public override void DidEnterDisplayState()
        {
            base.DidEnterDisplayState();

            _isLikedByMeBinding = this.SetBinding(() => _viewModelRef.Target.LikeViewModel.IsLiked).WhenSourceChanges(
                () =>
                {
                    _commentNode.SetLikesState(
                        _viewModelRef.Target.LikeViewModel.IsLiked,
                        _viewModelRef.Target.LikeViewModel.LikeCountText);
                });
            _imageBinding = this.SetBinding(() => _viewModelRef.Target.ImageUrl).WhenSourceChanges(() =>
            {
                _commentNode.TryLoadImage(_viewModelRef.Target);
            });
        }

        public override void DidExitDisplayState()
        {
            base.DidExitDisplayState();

            _isLikedByMeBinding.Detach();
            _isLikedByMeBinding = null;

            _imageBinding.Detach();
            _imageBinding = null;
        }

        private void OnReplyPressed()
        {
            var vm = _viewModelRef.Target;
            if (vm == null)
            {
                return;
            }

            vm.ReplyCommand.Execute(vm.Model);
        }

        private void OnLikePressed()
        {
            var vm = _viewModelRef.Target;
            if (vm == null)
            {
                return;
            }

            vm.LikeCommand.Execute(vm);
        }
    }
}