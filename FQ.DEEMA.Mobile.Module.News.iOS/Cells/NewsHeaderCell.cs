﻿using System.Collections.Generic;
using System.Drawing;
using AsyncDisplayKitBindings;
using FQ.DEEMA.Mobile.Module.News.iOS.Extensions;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.ViewModels;
using Softeq.XToolkit.Common.iOS.Helpers;
using UIKit;
using ObjCRuntime;
using Foundation;
using Softeq.XToolkit.Common;
using FQ.DEEMA.Mobile.Module.News.iOS.Controls;
using CoreGraphics;

namespace FQ.DEEMA.Mobile.Module.News.iOS.Cells
{
    public class NewsHeaderCell : ASCellNode
    {
        private readonly ASTextNode _descriptionNode;
        private readonly ASImageNode _imageNode;
        private readonly ASImageNode _imagePlaceholder;
        private readonly ASTextNode _publishDateNode;
        private readonly ASTextNode _delimiterNode;
        private readonly ASButtonNode _channelsNode;
        private readonly ASTextNode _titleNode;
        private readonly ASImageNode _likesImage;
        private readonly ASTextNode _likesCount;
        private readonly ASImageNode _commentsImage;
        private readonly ASTextNode _commentsCount;
        private readonly ASDisplayNode _topSeparator;
        private readonly ASDisplayNode _bottomSeparator;
        private readonly ASDisplayNode _bottomSpaceSeparator;
        private readonly ASDisplayNode _likesSpace;

        private bool _hasImage;
        private bool _hasVideo;

        private WeakReferenceEx<NewsHeaderViewModel> _viewModelRef;

        public NewsHeaderCell()
        {
            _titleNode = new ASTextNode();
            _descriptionNode = new ASTextNode();
            _publishDateNode = new ASTextNode();
            _delimiterNode = new ASTextNode();
            _channelsNode = new ASButtonNode();

            _imageNode = new ASImageNode
            {
                ContentMode = UIViewContentMode.ScaleAspectFill
            };

            _imagePlaceholder = new ASImageNode();
            _imagePlaceholder.Image = UIImage.FromBundle(StyleHelper.Style.ImagePlaceholderBoundleName);
            _imagePlaceholder.BackgroundColor = UIColor.FromRGB(248, 248, 248);

            _likesImage = new ASImageNode
            {
                ContentMode = UIViewContentMode.ScaleAspectFill
            };
            _likesCount = new ASTextNode();

            _commentsImage = new ASImageNode
            {
                ContentMode = UIViewContentMode.ScaleAspectFill
            };
            _commentsCount = new ASTextNode();

            BackgroundColor = StyleHelper.Style.ContentColor;

            _topSeparator = new ASDisplayNode { BackgroundColor = StyleHelper.Style.NewsSeparatorColor };
            _bottomSeparator = new ASDisplayNode { BackgroundColor = StyleHelper.Style.NewsSeparatorColor };
            _bottomSpaceSeparator = new ASDisplayNode { BackgroundColor = StyleHelper.Style.NewsSeparatorBackgroundColor };
            _likesSpace = new ASDisplayNode();
        }

        public void BindCell(NewsHeaderViewModel viewModel)
        {
            _viewModelRef = WeakReferenceEx.Create(viewModel);

            if (viewModel.NewsHeader.Title != null)
            {
                _titleNode.AttributedText = viewModel.NewsHeader.Title
                    .BuildAttributedString()
                    .Font(StyleHelper.Style.BigSemiboldFont)
                    .Foreground(StyleHelper.Style.TextNormalColor);
            }

            var converter = new DateTimeToStringConverter(LocaleHelper.Is24HourFormat);

            _publishDateNode.AttributedText = converter.ConvertValue(viewModel.NewsHeader.PublishDate)
                .BuildAttributedString()
                .Font(StyleHelper.Style.SmallFont)
                .Foreground(StyleHelper.Style.TextSecondaryColor);

            _delimiterNode.AttributedText = "|"
                .BuildAttributedString()
                .Font(StyleHelper.Style.SmallFont)
                .Foreground(StyleHelper.Style.TextSecondaryColor);

            _channelsNode.SetTitle(
                viewModel.RelatedChannels,
                StyleHelper.Style.SmallFont,
                StyleHelper.Style.AccentColor,
                UIControlState.Normal);

            if (viewModel.NewsHeader.Description != null)
            {
                _descriptionNode.AttributedText = viewModel.NewsHeader.Description
                    .BuildAttributedString()
                    .Font(StyleHelper.Style.NormalFont)
                    .Foreground(StyleHelper.Style.TextNormalColor);
            }

            if (viewModel.NewsHeader.IsLikesEnabled)
            {
                _likesImage.Image = UIImage.FromBundle(StyleHelper.Style.LikeFeedBoundleName);
                _likesCount.AttributedText = viewModel.NewsHeader.LikesCount.ToString()
                        .BuildAttributedString()
                        .Font(StyleHelper.Style.SmallFont)
                        .Foreground(StyleHelper.Style.TextSecondaryColor);
            }

            if (viewModel.NewsHeader.IsCommentsEnabled)
            {
                _commentsImage.Image = UIImage.FromBundle(StyleHelper.Style.CommentsBoundleName);
                _commentsCount.AttributedText = viewModel.NewsHeader.CommentsCount.ToString()
                        .BuildAttributedString()
                        .Font(StyleHelper.Style.SmallFont)
                        .Foreground(StyleHelper.Style.TextSecondaryColor);
            }

            if (viewModel.HasImage)
            {
                _imageNode.LoadImageAsync(viewModel.NewsHeader.ImageUrl, new Size(0, 170));
            }

            _hasImage = viewModel.HasImage;
            _hasVideo = viewModel.HasVideo;

            SelectionStyle = UITableViewCellSelectionStyle.None;
            AutomaticallyManagesSubnodes = true;
            SetNeedsLayout();
        }

        public override ASLayoutSpec LayoutSpecThatFits(ASSizeRange constrainedSize)
        {
            var imageLayoutHeight = new ASDimension { value = 170, unit = ASDimensionUnit.Points };

            _imageNode.Style.Height = imageLayoutHeight;
            _imagePlaceholder.Style.Height = imageLayoutHeight;

            _titleNode.Style.FlexShrink = 1;
            _titleNode.Style.SpacingBefore = 14;

            _channelsNode.Style.FlexShrink = 1;
            _channelsNode.TitleNode.MaximumNumberOfLines = 1;
            _channelsNode.TitleNode.TruncationMode = UILineBreakMode.TailTruncation;

            _descriptionNode.Style.FlexShrink = 1;
            _descriptionNode.Style.SpacingBefore = 10;

            _likesImage.Style.Height = new ASDimension { value = 20, unit = ASDimensionUnit.Points };
            _likesImage.Style.Width = new ASDimension { value = 20, unit = ASDimensionUnit.Points };

            _commentsImage.Style.Height = new ASDimension { value = 20, unit = ASDimensionUnit.Points };
            _commentsImage.Style.Width = new ASDimension { value = 20, unit = ASDimensionUnit.Points };
            _commentsImage.Style.SpacingBefore = 8;

            _likesCount.Style.SpacingBefore = 2;
            _likesCount.Style.AlignSelf = ASStackLayoutAlignSelf.Center;

            _commentsCount.Style.SpacingBefore = 2;
            _commentsCount.Style.AlignSelf = ASStackLayoutAlignSelf.Center;

            _likesSpace.Style.FlexGrow = 1;

            var socialNodes = new List<IASLayoutElement>
            {
                _likesSpace
            };

            if (_likesCount.AttributedText != null)
            {
                socialNodes.Add(_likesImage);
                socialNodes.Add(_likesCount);
            }

            if (_commentsCount.AttributedText != null)
            {
                socialNodes.Add(_commentsImage);
                socialNodes.Add(_commentsCount);
            }

            var socialNodesStack = new ASStackLayoutSpec
            {
                Direction = ASStackLayoutDirection.Horizontal,
                Children = socialNodes.ToArray()
            };
            socialNodesStack.Style.SpacingAfter = 14;
            socialNodesStack.Style.SpacingBefore = 4;

            _topSeparator.Style.Height = new ASDimension { value = 1, unit = ASDimensionUnit.Points };
            _bottomSeparator.Style.Height = new ASDimension { value = 1, unit = ASDimensionUnit.Points };
            _bottomSpaceSeparator.Style.Height = new ASDimension { value = 8, unit = ASDimensionUnit.Points };

            _channelsNode.AddTarget(this, new Selector(nameof(OnChannelsPressed)), ASControlNodeEvent.TouchUpInside);

            var dateStackView = new ASStackLayoutSpec
            {
                Direction = ASStackLayoutDirection.Horizontal,
                Children = new IASLayoutElement[]
                {
                    _publishDateNode,
                    _delimiterNode,
                    _channelsNode
                },
                Spacing = 10
            };
            dateStackView.Style.SpacingBefore = 10;

            var stackView = new ASStackLayoutSpec
            {
                Direction = ASStackLayoutDirection.Vertical,
                Children = new IASLayoutElement[]
                {
                    _titleNode,
                    dateStackView,
                    _descriptionNode,
                    socialNodesStack
                }
            };

            var list = new List<IASLayoutElement>
            {
                _topSeparator,
                ASInsetLayoutSpec.InsetLayoutSpecWithInsets(new UIEdgeInsets(0, 10, 0, 10), stackView),
                _bottomSeparator,
                _bottomSpaceSeparator
            };

            var imageLayout = CreateImageLayout();
            if (imageLayout != null)
            {
                list.Insert(1, imageLayout);
            }

            var cellLayout = new ASStackLayoutSpec
            {
                Direction = ASStackLayoutDirection.Vertical,
                Children = list.ToArray()
            };

            return cellLayout;
        }

        private IASLayoutElement CreateImageLayout()
        {
            var imageLayout = ASBackgroundLayoutSpec.BackgroundLayoutSpecWithChild(_imageNode, _imagePlaceholder);

            if (_hasImage && _hasVideo)
            {
                return CreatePlayButtonLayout(imageLayout);
            }

            if (_hasVideo)
            {
                _imagePlaceholder.Image = null;

                return CreatePlayButtonLayout(_imagePlaceholder);
            }

            if (_hasImage)
            {
                return imageLayout;
            }

            return null;
        }

        private IASLayoutElement CreatePlayButtonLayout(IASLayoutElement parentNode)
        {
            var playNode = ControlsFactory.CreatePlayButtonNode();
            playNode.AddTarget(this, new Selector(nameof(OnPlayPressed)), ASControlNodeEvent.TouchUpInside);

            var playButtonLayout = ASCenterLayoutSpec.CenterLayoutSpecWithCenteringOptions(
                ASCenterLayoutSpecCenteringOptions.Xy,
                ASCenterLayoutSpecSizingOptions.MinimumXY,
                playNode);

            return ASOverlayLayoutSpec.OverlayLayoutSpecWithChild(parentNode, playButtonLayout);
        }

        [Export(nameof(OnPlayPressed))]
        private void OnPlayPressed() => _viewModelRef.Target.OpenPlayerCommand.Execute(null);

        [Export(nameof(OnChannelsPressed))]
        private void OnChannelsPressed() => _viewModelRef.Target.OpenChannelsCommand.Execute(null);
    }
}