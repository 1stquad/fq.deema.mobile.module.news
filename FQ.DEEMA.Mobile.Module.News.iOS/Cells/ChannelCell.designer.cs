// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FQ.DEEMA.Mobile.Module.News.iOS.Cells
{
	[Register ("ChannelCell")]
	partial class ChannelCell
	{
		[Outlet]
		UIKit.UIImageView ChannelIcon { get; set; }

		[Outlet]
		UIKit.UIButton ChildrenButton { get; set; }

		[Outlet]
		UIKit.UILabel Label { get; set; }

		[Outlet]
		UIKit.UIImageView NextIcon { get; set; }

		[Outlet]
		UIKit.UIView SeparatorView { get; set; }

		[Action ("OnChildrenClick:")]
		partial void OnChildrenClick (Foundation.NSObject sender);

		[Action ("OnDetailsClick:")]
		partial void OnDetailsClick (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (ChannelIcon != null) {
				ChannelIcon.Dispose ();
				ChannelIcon = null;
			}

			if (Label != null) {
				Label.Dispose ();
				Label = null;
			}

			if (NextIcon != null) {
				NextIcon.Dispose ();
				NextIcon = null;
			}

			if (SeparatorView != null) {
				SeparatorView.Dispose ();
				SeparatorView = null;
			}

			if (ChildrenButton != null) {
				ChildrenButton.Dispose ();
				ChildrenButton = null;
			}
		}
	}
}
