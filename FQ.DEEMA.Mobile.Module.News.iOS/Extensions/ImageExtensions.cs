﻿using System.Drawing;
using AsyncDisplayKitBindings;
using FFImageLoading;
using FFImageLoading.Cross;
using FFImageLoading.Work;
using Softeq.XToolkit.WhiteLabel.Threading;
using UIKit;
using static Softeq.XToolkit.WhiteLabel.iOS.Helpers.AvatarImageHelpers;

namespace FQ.DEEMA.Mobile.Module.News.iOS.Extensions
{
    public static class ImageExtensions
    {
        public static async void LoadImageAsync(this ASImageNode imageNode, string url, Size size,
            string boundleResource = null)
        {
            if (string.IsNullOrEmpty(url))
            {
                if (!string.IsNullOrEmpty(boundleResource))
                {
                    try
                    {
                        var boundleImage = await ImageService.Instance
                            .LoadCompiledResource(boundleResource)
                            .DownSampleInDip(size.Width, size.Height)
                            .AsUIImageAsync().ConfigureAwait(false);
                        imageNode.Image = boundleImage;
                    }
                    catch
                    {
                        // do nothing
                    }
                }
            }

            var expr = ImageService.Instance
                .LoadUrl(url)
                .DownSampleInDip(size.Width, size.Height);

            try
            {
                var image = await expr.AsUIImageAsync().ConfigureAwait(false);
                if (image != null)
                {
                    imageNode.Image = image;
                }
            }
            catch
            {
                // do nothing
            }
        }

        public static void LoadImageAsync(this MvxCachedImageView imageView, string boundleResource, string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                imageView.Image?.Dispose();
                imageView.Image = null;
                if (!string.IsNullOrEmpty(boundleResource))
                {
                    imageView.Image = UIImage.FromBundle(boundleResource);
                }

                return;
            }

            var expr = ImageService.Instance.LoadUrl(url);

            if (!string.IsNullOrEmpty(boundleResource))
            {
                expr = expr.LoadingPlaceholder(boundleResource, ImageSource.CompiledResource)
                    .ErrorPlaceholder(boundleResource, ImageSource.CompiledResource);
            }

            expr.IntoAsync(imageView);
        }
    }
}