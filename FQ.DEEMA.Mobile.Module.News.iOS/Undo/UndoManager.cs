using System;
using System.Threading;
using CoreGraphics;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.WhiteLabel.iOS;
using Softeq.XToolkit.WhiteLabel.iOS.Navigation;
using Softeq.XToolkit.WhiteLabel.Interfaces;
using Softeq.XToolkit.WhiteLabel.Services;
using ToastBindings;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.News.iOS.Undo
{
    //TODO: move this if others module needs undo functionality
    public class UndoManager : IUndoManager
    {
        private const int Margin = 16;
        
        private readonly IViewLocator _viewLocator;

        public UndoManager(IViewLocator viewLocator)
        {
            _viewLocator = viewLocator;
        }
        
        public void AddOperation(Action<CancellationTokenSource> undoAction, Action<CancellationToken> applyAction,
            string infoLabel, string actionLabel, CancellationTokenSource cancellationTokenSource)
        {
            var rootViewController = _viewLocator.GetTopViewController();
            if (rootViewController == null)
            {
                return;
            }

            var view = new UIView
            {
                BackgroundColor = StyleHelper.Style.NotificationBackgroundColor
            };

            //initialize undo button
            var undoButton = UIButton.FromType(UIButtonType.System);
            undoButton.TranslatesAutoresizingMaskIntoConstraints = false;
            undoButton.SetTitle(actionLabel, UIControlState.Normal);
            undoButton.TintColor = StyleHelper.Style.ContentColor;
            undoButton.TitleLabel.Font = StyleHelper.Style.BigSemiboldFont;
            undoButton.SetCommand(new RelayCommand<CancellationTokenSource>(undoAction), cancellationTokenSource);

            view.AddSubview(undoButton);
            NSLayoutConstraint.ActivateConstraints(new[]
            {
                undoButton.RightAnchor.ConstraintEqualTo(view.RightAnchor, -Margin),
                undoButton.TopAnchor.ConstraintEqualTo(view.TopAnchor),
                undoButton.BottomAnchor.ConstraintEqualTo(view.BottomAnchor)
            });

            //initialize label
            var label = new UILabel
            {
                Text = infoLabel,
                Lines = 0,
                TextColor = StyleHelper.Style.ContentColor,
                Font = StyleHelper.Style.BigFont,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            view.AddSubview(label);
            NSLayoutConstraint.ActivateConstraints(new[]
            {
                label.LeftAnchor.ConstraintEqualTo(view.LeftAnchor, Margin),
                label.RightAnchor.ConstraintEqualTo(undoButton.LeftAnchor, Margin),
                label.CenterYAnchor.ConstraintEqualTo(view.CenterYAnchor)
            });
            
            view.Frame = new CGRect(0, 0, rootViewController.View.Bounds.Width, 40);

            undoButton.SetCommand(
                new RelayCommand<(UIView Parent, UIView Toast)>(x =>
                {
                    ToastService.HideToast(x.Parent, x.Toast);
                }), (rootViewController.View, view));

            ToastService.ShowToast(rootViewController.View, view, () => { applyAction(cancellationTokenSource.Token); });
        }
    }
}