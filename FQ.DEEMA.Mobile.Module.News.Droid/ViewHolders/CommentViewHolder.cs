﻿using System;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using FFImageLoading;
using FFImageLoading.Transformations;
using FFImageLoading.Views;
using FFImageLoading.Work;
using FQ.DEEMA.Mobile.Module.News.Droid.Controls;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common;
using Softeq.XToolkit.Common.Droid.Converters;
using Softeq.XToolkit.Common.Droid.Helpers;
using Softeq.XToolkit.WhiteLabel;
using Softeq.XToolkit.WhiteLabel.Droid.Extensions;
using Softeq.XToolkit.WhiteLabel.Navigation;
using Softeq.XToolkit.WhiteLabel.ViewModels;
using Softeq.XToolkit.Common.Droid.Extensions;
using System.Linq;
using Softeq.XToolkit.WhiteLabel.Droid.Controls;

namespace FQ.DEEMA.Mobile.Module.News.Droid.ViewHolders
{
    internal class CommentViewHolder : RecyclerView.ViewHolder
    {
        private readonly ImageViewAsync _circleImageView;
        private readonly View _imageContainer;
        private readonly ImageViewAsync _imageViewAsync;
        private readonly Button _likeButton;
        private readonly TextView _messageTextView;
        private readonly TextView _nameTextView;
        private readonly INewsLocalizedStrings _newsLocalizedStrings;
        private readonly Button _replyButton;
        private readonly TextView _timespanTextView;
        private Binding _imageBinding;
        private Binding _likeBinding;
        private Binding _imageKeyBinding;

        private WeakReferenceEx<CommentViewModel> _viewModelRef;

        public CommentViewHolder(View view, INewsLocalizedStrings newsLocalizedStrings) : base(view)
        {
            _circleImageView =
                view.FindViewById<ImageViewAsync>(Resource.Id.activity_news_details_comment_cell_avatar);
            _nameTextView = view.FindViewById<TextView>(Resource.Id.activity_news_details_comment_cell_name);
            _messageTextView = view.FindViewById<TextView>(Resource.Id.activity_news_details_comment_cell_message);
            _timespanTextView = view.FindViewById<TextView>(Resource.Id.activity_news_details_comment_cell_timespan);
            _likeButton = view.FindViewById<Button>(Resource.Id.activity_news_details_comment_cell_like_button);
            _replyButton = view.FindViewById<Button>(Resource.Id.activity_news_details_comment_cell_reply_button);
            ForegroundView = view.FindViewById<View>(Resource.Id.activity_news_details_foreground_view);
            _imageContainer = view.FindViewById<View>(Resource.Id.activity_news_details_comment_cell_image_container);
            _imageViewAsync = view.FindViewById<ImageViewAsync>(Resource.Id.activity_news_details_comment_cell_image);

            var deleteBackgroundView =
                view.FindViewById<DeleteBackgroundView>(Resource.Id.activity_news_details_comment_cell_delete_bg);
            deleteBackgroundView.SetLabel(newsLocalizedStrings.Delete);

            _newsLocalizedStrings = newsLocalizedStrings;

            _likeButton.Click += OnLikeClick;
            _replyButton.Click += OnReplyClick;
            _imageViewAsync.Click += OnImageClick;

            view.LongClickable = true;
            view.LongClick += OnLongClick;
        }

        public View ForegroundView { get; }

        public void Bind(CommentViewModel vm)
        {
            _viewModelRef = WeakReferenceEx.Create(vm);

            var model = vm.Model;

            _nameTextView.Text = model.PublisherName;
            _messageTextView.Text = model.Message;
            _messageTextView.HighlightStrings(vm.Mentions.Select(x => (x.Start, x.Length)),
               _messageTextView.GetColor(Resource.Color.news_accent_text_color));
            _imageViewAsync.SetImageDrawable(null);

            var converter = new DateTimeToStringConverter(LocaleHelper.Is24HourFormat(_messageTextView.Context));

            _timespanTextView.Text = converter.ConvertValue(model.DateCreated);
            _likeButton.Text = vm.LikeViewModel.LikeCountText;
            _replyButton.Text = _newsLocalizedStrings.Reply;

            LoadPublisherPhoto();

            _likeBinding?.Detach();
            _likeBinding = this.SetBinding(() => _viewModelRef.Target.LikeViewModel.IsLiked).WhenSourceChanges(() =>
            {
                if (_viewModelRef.Target.LikeViewModel.IsLiked)
                {
                    _likeButton.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.news_ic_like_accent, 0, 0, 0);
                    _likeButton.SetTextColor(_likeButton.GetColor(Resource.Color.news_accent_text_color));
                }
                else
                {
                    _likeButton.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.news_ic_like, 0, 0, 0);
                    _likeButton.SetTextColor(_likeButton.GetColor(Resource.Color.news_secondary_text_color));
                }

                _likeButton.Text = _viewModelRef.Target.LikeViewModel.LikeCountText;
            });

            _imageContainer.Visibility = BoolToViewStateConverter.ConvertGone(vm.Model.HasImage);

            _imageBinding?.Detach();
            _imageBinding = this.SetBinding(() => _viewModelRef.Target.ImageUrl).WhenSourceChanges(() =>
            {
                if (string.IsNullOrEmpty(_viewModelRef.Target.ImageUrl))
                {
                    return;
                }

                ImageService.Instance
                    .LoadUrl(_viewModelRef.Target.ImageUrl)
                    .IntoWithDeviceWidthAsync(_imageViewAsync);
            });

            _imageKeyBinding?.Detach();
            _imageKeyBinding = this.SetBinding(() => _viewModelRef.Target.ImageCacheKey).WhenSourceChanges(() =>
            {
                if (string.IsNullOrEmpty(_viewModelRef.Target.ImageCacheKey))
                {
                    return;
                }

                ImageService.Instance
                    .LoadFile(_viewModelRef.Target.ImageCacheKey)
                    .IntoWithDeviceWidthAsync(_imageViewAsync);
            });
        }

        private void OnReplyClick(object sender, EventArgs e)
        {
            _viewModelRef.Target?.ReplyCommand?.Execute(_viewModelRef.Target?.Model);
        }

        private void LoadPublisherPhoto()
        {
            var model = _viewModelRef?.Target?.Model;
            _circleImageView.LoadImageWithTextPlaceholder(
                model.PublisherImageUrl,
                model.PublisherName,
                StyleHelper.Style.AvatarStyles,
                (TaskParameter x) => x.Transform(new CircleTransformation()));
        }

        private void OnLikeClick(object sender, EventArgs e)
        {
            var vm = _viewModelRef.Target;
            vm?.LikeCommand.Execute(vm);
        }

        private void OnImageClick(object sender, EventArgs e)
        {
            if (_viewModelRef.Target != null &&
                string.IsNullOrEmpty(_viewModelRef.Target.ImageUrl) &&
                string.IsNullOrEmpty(_viewModelRef.Target.ImageCacheKey))
            {
                return;
            }

            var service = Dependencies.IocContainer.Resolve<IDialogsService>();
            var options = new FullScreenImageOptions
            {
                ImageUrl = _viewModelRef.Target.ImageUrl,
                ImagePath = _viewModelRef.Target.ImageCacheKey,
                DroidCloseButtonImageResId = Resource.Drawable.core_ic_close
            };
            service.ShowForViewModel<FullScreenImageViewModel, FullScreenImageOptions>(options);
        }

        private void OnLongClick(object sender, View.LongClickEventArgs e)
        {
            if (_viewModelRef.Target == null)
            {
                return;
            }

            new DroidContextMenuComponent(_viewModelRef.Target.Options)
                .BuildMenu(_nameTextView.Context, _nameTextView)
                .Show();
        }
    }
}