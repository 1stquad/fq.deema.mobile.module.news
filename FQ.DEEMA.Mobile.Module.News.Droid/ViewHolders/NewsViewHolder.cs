﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using FFImageLoading;
using FFImageLoading.Views;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.ViewModels;
using Softeq.XToolkit.Common;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Common.Droid.Converters;
using Softeq.XToolkit.Common.Droid.Helpers;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.WhiteLabel.Droid.Extensions;

namespace FQ.DEEMA.Mobile.Module.News.Droid.ViewHolders
{
    internal class NewsViewHolder : RecyclerView.ViewHolder
    {
        private readonly TextView _descriptionTextView;
        private readonly View _imageContainer;
        private readonly ImageView _placeholderImageView;
        private readonly ImageView _likesImageView;
        private readonly ImageViewAsync _imageView;
        private readonly TextView _subTitleTextView;
        private readonly TextView _titleTextView;
        private readonly TextView _likesTextView;
        private readonly ImageView _commentsImageView;
        private readonly TextView _commentsTextView;
        private readonly ImageButton _playVideoButton;
        private readonly TextView _channelsTextView;
        private readonly RelayCommand<NewsHeaderViewModel> _navigateToDetailsCommand;

        private WeakReferenceEx<NewsHeaderViewModel> _viewModelRef;

        public NewsViewHolder(View view, RelayCommand<NewsHeaderViewModel> navigateToDetailsCommand)
            : base(view)
        {
            _navigateToDetailsCommand = navigateToDetailsCommand;

            _imageView = view.FindViewById<ImageViewAsync>(Resource.Id.fragment_news_cell_image);
            _titleTextView = view.FindViewById<TextView>(Resource.Id.fragment_news_cell_title);
            _subTitleTextView = view.FindViewById<TextView>(Resource.Id.fragment_news_cell_publish_date);
            _descriptionTextView = view.FindViewById<TextView>(Resource.Id.fragment_news_cell_description);
            _imageContainer = view.FindViewById<View>(Resource.Id.fragment_news_cell_image_container);
            _placeholderImageView = view.FindViewById<ImageView>(Resource.Id.fragment_news_cell_placeholder);
            _likesImageView = view.FindViewById<ImageView>(Resource.Id.fragment_news_cell_likes_icon);
            _likesTextView = view.FindViewById<TextView>(Resource.Id.fragment_news_cell_likes);
            _commentsImageView = view.FindViewById<ImageView>(Resource.Id.fragment_news_cell_comments_icon);
            _commentsTextView = view.FindViewById<TextView>(Resource.Id.fragment_news_cell_comments);
            _playVideoButton = view.FindViewById<ImageButton>(Resource.Id.fragment_news_cell_play);
            _channelsTextView = view.FindViewById<TextView>(Resource.Id.fragment_news_cell_channels);

            _playVideoButton.SetCommand(nameof(_playVideoButton.Click), new RelayCommand(OnPlayVideoClick));
            _channelsTextView.SetCommand(nameof(view.Click), new RelayCommand(OnChannelsClick));
            view.SetCommand(nameof(view.Click), new RelayCommand(OnItemClick));
        }

        public void Bind(NewsHeaderViewModel vm)
        {
            _viewModelRef = WeakReferenceEx.Create(vm);
            _imageView.SetImageResource(Android.Resource.Color.Transparent);
            _titleTextView.Text = vm.NewsHeader.Title;
            _channelsTextView.Text = vm.RelatedChannels;

            var converter = new DateTimeToStringConverter(LocaleHelper.Is24HourFormat(_titleTextView.Context));

            _subTitleTextView.Text = converter.ConvertValue(vm.NewsHeader.PublishDate);
            _descriptionTextView.Text = vm.NewsHeader.Description;

            if (vm.HasImage)
            {
                _imageContainer.Visibility = ViewStates.Visible;

                ImageService.Instance
                    .LoadUrl(vm.NewsHeader.ImageUrl)
                    .IntoWithDeviceWidthAsync(_imageView);
            }
            else if (vm.HasVideo)
            {
                _imageContainer.Visibility = ViewStates.Visible;

                _placeholderImageView.SetImageDrawable(null);
            }
            else
            {
                _imageContainer.Visibility = ViewStates.Gone;
            }

            if (vm.NewsHeader.IsLikesEnabled)
            {
                _likesTextView.Text = vm.NewsHeader.LikesCount.ToString();
                _likesTextView.Visibility = ViewStates.Visible;
                _likesImageView.Visibility = ViewStates.Visible;
            }
            else
            {
                _likesTextView.Visibility = ViewStates.Gone;
                _likesImageView.Visibility = ViewStates.Gone;
            }

            if (vm.NewsHeader.IsCommentsEnabled)
            {
                _commentsTextView.Text = vm.NewsHeader.CommentsCount.ToString();
                _commentsTextView.Visibility = ViewStates.Visible;
                _commentsImageView.Visibility = ViewStates.Visible;
            }
            else
            {
                _commentsTextView.Visibility = ViewStates.Gone;
                _commentsImageView.Visibility = ViewStates.Gone;
            }

            _playVideoButton.Visibility = BoolToViewStateConverter.ConvertGone(vm.HasVideo);
        }

        private void OnItemClick()
        {
            _navigateToDetailsCommand.Execute(_viewModelRef.Target);
        }

        private void OnPlayVideoClick()
        {
            if (_viewModelRef != null && _viewModelRef.Target != null)
            {
                _viewModelRef.Target.OpenPlayerCommand.Execute(null);
            }
        }

        private void OnChannelsClick()
        {
            if (_viewModelRef != null && _viewModelRef.Target != null)
            {
                _viewModelRef.Target.OpenChannelsCommand.Execute(null);
            }
        }
    }
}