﻿using System;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using FQ.DEEMA.Mobile.Module.News.ViewModels;
using Softeq.XToolkit.Common;
using Softeq.XToolkit.Common.Droid.Converters;
using Softeq.XToolkit.WhiteLabel.Droid.Extensions;

namespace FQ.DEEMA.Mobile.Module.News.Droid.ViewHolders
{
    internal class ChannelViewHolder : RecyclerView.ViewHolder
    {
        private readonly ImageView _imageView;
        private readonly TextView _textView;
        private readonly View _nextView;
        private readonly View _detailsView;

        private WeakReferenceEx<ChannelViewModel> _itemRef;

        public ChannelViewHolder(View view) : base(view)
        {
            _imageView = view.FindViewById<ImageView>(Resource.Id.activity_news_channels_cell_image);
            _nextView = view.FindViewById<View>(Resource.Id.activity_news_channels_cell_next_button);
            _detailsView = view.FindViewById<View>(Resource.Id.activity_news_channels_cell_details_button);
            _textView = view.FindViewById<TextView>(Resource.Id.activity_news_channels_cell_label);

            _nextView.Click += OnNextClick;
            _detailsView.Click += OnDetailsClick;
        }

        private void OnNextClick(object sender, EventArgs e)
        {
            _itemRef.Target?.NavigateToChannels();
        }

        private void OnDetailsClick(object sender, EventArgs e)
        {
            _itemRef.Target?.NavigateToChannel();
        }

        public void Bind(ChannelViewModel item)
        {
            _textView.Text = item.Name;
            _itemRef = WeakReferenceEx.Create(item);

            _nextView.Visibility = BoolToViewStateConverter.ConvertGone(item.Channel.Channels.Count > 0);

            var resId = item.Channel.HasSubscribedChannels 
                                ? Resource.Color.news_accent_text_color 
                                : Resource.Color.news_button_color;
            _imageView.SetColorFilter(_imageView.GetColor(resId));
        }
    }
}