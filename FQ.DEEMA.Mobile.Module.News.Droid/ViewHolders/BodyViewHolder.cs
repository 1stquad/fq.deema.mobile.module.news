﻿using System;
using System.Globalization;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using FFImageLoading;
using FFImageLoading.Transformations;
using FFImageLoading.Views;
using FQ.DEEMA.Mobile.Module.News.Droid.Controls;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common;
using Softeq.XToolkit.Common.Droid.Converters;
using Softeq.XToolkit.Common.Droid.Helpers;
using Softeq.XToolkit.WhiteLabel.Droid.Extensions;

namespace FQ.DEEMA.Mobile.Module.News.Droid.ViewHolders
{
    internal class BodyViewHolder : RecyclerView.ViewHolder
    {
        private readonly ImageViewAsync _avatarImage;
        private readonly TextView _commentTextView;
        private readonly LocalWebView _descriptionWebView;
        private readonly ImageViewAsync _imageView;
        private readonly Button _likeButton;
        private readonly TextView _nameTextView;
        private readonly Button _readButton;
        private readonly LocalWebView _videoPlayerView;
        private readonly TextView _channelsTextView;
        private readonly TextView _subTitleTextView;
        private readonly TextView _timeSpanTextView;
        private readonly TextView _titleTextView;
        private readonly INewsLocalizedStrings _newsLocalizedStrings;

        private Binding _likeBinding;
        private Binding _readBinding;

        private WeakReferenceEx<BodyViewModel> _viewModelRef;

        public BodyViewHolder(View view, INewsLocalizedStrings newsLocalizedStrings) : base(view)
        {
            _imageView = view.FindViewById<ImageViewAsync>(Resource.Id.activity_news_details_body_cell_image);
            _titleTextView = view.FindViewById<TextView>(Resource.Id.activity_news_details_body_cell_title);
            _subTitleTextView = view.FindViewById<TextView>(Resource.Id.activity_news_details_body_cell_publish_date);
            _descriptionWebView = view.FindViewById<LocalWebView>(Resource.Id.activity_news_details_body_cell_description);
            _nameTextView = view.FindViewById<TextView>(Resource.Id.activity_news_details_body_cell_name);
            _timeSpanTextView = view.FindViewById<TextView>(Resource.Id.activity_news_details_body_cell_timespan);
            _likeButton = view.FindViewById<Button>(Resource.Id.activity_news_details_body_cell_like_button);
            _avatarImage = view.FindViewById<ImageViewAsync>(Resource.Id.activity_news_details_body_cell_avatar);
            _commentTextView = view.FindViewById<TextView>(Resource.Id.activity_news_details_body_cell_comment_label);
            _readButton = view.FindViewById<Button>(Resource.Id.activity_news_details_body_cell_read_button);
            _videoPlayerView = view.FindViewById<LocalWebView>(Resource.Id.activity_news_details_body_cell_video);
            _channelsTextView = view.FindViewById<TextView>(Resource.Id.activity_news_details_body_cell_channels);

            _imageView.SetAdjustViewBounds(true);

            _newsLocalizedStrings = newsLocalizedStrings;

            _likeButton.Click += OnLikeClick;
            _readButton.Click += OnReadClick;
            _channelsTextView.Click += OnChannelsClick;

            _descriptionWebView.Init(UrlLoadingHandler, null);
            _descriptionWebView.EnableCookies();

            _videoPlayerView.Init(UrlLoadingHandler, null);
            _videoPlayerView.EnableCookies();
        }

        public void Bind(BodyViewModel vm)
        {
            _viewModelRef = WeakReferenceEx.Create(vm);

            _titleTextView.Text = vm.Model.Title;
            _subTitleTextView.Text =
                vm.Model.PublishDate.ToString(vm.NewsLocalizedStrings.DateFormat, CultureInfo.InvariantCulture);
            _channelsTextView.Text = vm.RelatedChannels;
            _descriptionWebView.SetBodyHtml(vm.HtmlContent);

            if (vm.HasImage)
            {
                ImageService.Instance
                    .LoadUrl(vm.Model.ImageUrl)
                    .IntoAsync(_imageView);
            }

            _nameTextView.Text = vm.Model.PublisherName;

            var converter = new DateTimeToStringConverter(LocaleHelper.Is24HourFormat(_timeSpanTextView.Context));

            _timeSpanTextView.Text = converter.ConvertValue(vm.Model.PublishDate);
            _commentTextView.Text = _newsLocalizedStrings.Comments;
            _commentTextView.Visibility = BoolToViewStateConverter.ConvertGone(vm.IsCommentsEnabled);

            LoadPublisherPhoto();

            _likeBinding?.Detach();
            _likeBinding = null;
            _likeBinding = this.SetBinding(() => _viewModelRef.Target.LikeViewModel.IsLiked).WhenSourceChanges(() =>
            {
                if (_viewModelRef.Target.LikeViewModel.IsLiked)
                {
                    _likeButton.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.news_ic_like_accent, 0, 0, 0);
                    _likeButton.SetTextColor(_likeButton.GetColor(Resource.Color.news_accent_text_color));
                }
                else
                {
                    _likeButton.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.news_ic_like, 0, 0, 0);
                    _likeButton.SetTextColor(_likeButton.GetColor(Resource.Color.news_secondary_text_color));
                }

                _likeButton.Text = _viewModelRef.Target.LikeViewModel.LikeCountText;
            });
            _likeButton.Visibility = BoolToViewStateConverter.ConvertGone(vm.IsLikesEnabled);

            _readButton.Text = _viewModelRef.Target.NewsLocalizedStrings.Read;

            _readBinding?.Detach();
            _readBinding = null;
            _readBinding = this.SetBinding(() => _viewModelRef.Target.IsReadByUser).WhenSourceChanges(() =>
            {
                if (_viewModelRef.Target.IsReadByUser)
                {
                    _readButton.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.news_check_mark_accent, 0, 0, 0);
                    _readButton.SetTextColor(_readButton.GetColor(Resource.Color.news_accent_text_color));
                }
                else
                {
                    _readButton.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.news_check_mark, 0, 0, 0);
                    _readButton.SetTextColor(_readButton.GetColor(Resource.Color.news_secondary_text_color));
                }
            });

            if (vm.HasVideo)
            {
                _videoPlayerView.Visibility = ViewStates.Visible;
                _imageView.Visibility = ViewStates.Gone;

                if (vm.VideoPlayerModel != null)
                {
                    _videoPlayerView.SetBodyHtml(vm.VideoPlayerModel.PlayerPageHtml);
                }
            }
        }

        private void LoadPublisherPhoto()
        {
            var model = _viewModelRef.Target?.Model;
            if (model == null)
            {
                return;
            }

            _avatarImage.LoadImageWithTextPlaceholder(
                model.PublisherImageUrl,
                model.PublisherName,
                StyleHelper.Style.AvatarStyles,
                x => x.Transform(new CircleTransformation()));
        }

        private void OnLikeClick(object sender, EventArgs e)
        {
            var vm = _viewModelRef.Target;
            if (vm == null)
            {
                return;
            }

            vm.LikeCommand.Execute(this);
        }

        private void OnReadClick(object sender, EventArgs e)
        {
            var vm = _viewModelRef.Target;
            if (vm == null)
            {
                return;
            }

            vm.ReadCommand.Execute(this);
        }

        private void OnChannelsClick(object sender, EventArgs e)
        {
            var vm = _viewModelRef.Target;
            if (vm == null)
            {
                return;
            }

            vm.OpenChannelsCommand.Execute(null);
        }

        private bool UrlLoadingHandler(string url)
        {
            var vm = _viewModelRef.Target;
            if (vm != null)
            {
                vm.NavigateToUrlCommand.Execute(url);
            }
            return true;
        }
    }
}