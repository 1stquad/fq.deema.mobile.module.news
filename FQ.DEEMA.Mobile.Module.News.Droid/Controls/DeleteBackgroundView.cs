﻿using System;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Widget;

namespace FQ.DEEMA.Mobile.Module.News.Droid.Controls
{
    [Register("com.fq.deema.mobile.module.news.droid.DeleteBackgroundView")]
    public class DeleteBackgroundView : LinearLayout
    {
        public DeleteBackgroundView(Context context) : base(context)
        {
            Init(context);
        }

        public DeleteBackgroundView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Init(context);
        }

        public DeleteBackgroundView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
            Init(context);
        }

        public DeleteBackgroundView(IntPtr handle, JniHandleOwnership owner) : base(handle, owner)
        {
        }

        public void SetLabel(string text)
        {
            var view = FindViewById<TextView>(Resource.Id.control_delete_background_delete_label);
            view.Text = text;
        }

        private void Init(Context context)
        {
            Inflate(context, Resource.Layout.control_delete_background, this);
        }
    }
}