﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Text.Format;
using Android.Util;
using Android.Widget;
using Java.Util;
using Plugin.CurrentActivity;

namespace FQ.DEEMA.Mobile.Module.News.Droid.Controls
{
    [Register("com.fq.deema.mobile.module.news.droid.DateTimeButtonView")]
    public class DateTimeButtonView : LinearLayout, DatePickerDialog.IOnDateSetListener, TimePickerDialog.IOnTimeSetListener
    {
        private Button _dateButton;
        private string _dateFormat;
        private TextView _label;
        private DateTime _minDateTime;
        private DateTime _selectedDate;
        private Button _timeButton;
        private string _timeFormat;

        public DateTimeButtonView(Context context) : base(context)
        {
            Init(context);
        }

        public DateTimeButtonView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Init(context);
        }

        public DateTimeButtonView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
            Init(context);
        }

        public DateTimeButtonView(IntPtr handle, JniHandleOwnership owner) : base(handle, owner)
        {
        }

        public DateTime SelectedDate
        {
            get => _selectedDate;
            set
            {
                _selectedDate = value;
                OnDateChanged();
            }
        }

        public void OnDateSet(DatePicker view, int year, int month, int dayOfMonth)
        {
            SelectedDate = new DateTime(year, month + 1, dayOfMonth, SelectedDate.Hour, SelectedDate.Minute, 0, DateTimeKind.Local);
        }
        
        public void OnTimeSet(TimePicker view, int hourOfDay, int minute)
        {
            SelectedDate = new DateTime(SelectedDate.Year, SelectedDate.Month, SelectedDate.Day, hourOfDay, minute, 0,
                DateTimeKind.Local);
        }

        public event EventHandler DateValueChanged;

        public void Initialize(DateTime dt, string dateLabel, string dateFormat)
        {
            _dateFormat = dateFormat;

            _timeFormat = DateFormat.Is24HourFormat(Context)
                ? "HH:mm"
                : "hh:mm tt";

            _label.Text = dateLabel;

            SelectedDate = new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, 0, DateTimeKind.Local);
        }

        public void SetMinTime(DateTime dt, bool alignDateValue)
        {
            _minDateTime = dt;

            if (alignDateValue)
            {
                if (dt > SelectedDate)
                {
                    SelectedDate = new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, 0, DateTimeKind.Local);
                }
            }
        }

        private void Init(Context context)
        {
            Inflate(context, Resource.Layout.control_date_time_button, this);

            _dateButton = FindViewById<Button>(Resource.Id.control_date_time_button_date);
            _dateButton.Click += (sender, e) => { ShowDatePicker(); };

            _timeButton = FindViewById<Button>(Resource.Id.control_date_time_button_time);
            _timeButton.Click += (sender, e) => { ShowTimePicker(); };

            _label = FindViewById<TextView>(Resource.Id.control_date_time_button_label);
        }

        private void ShowDatePicker()
        {
            var datePicker = new DatePickerFragment();
            datePicker.Initialize(this, SelectedDate.Year, SelectedDate.Month - 1, SelectedDate.Day);

            var cal = Calendar.Instance;
            cal.Set(CalendarField.Year, _minDateTime.Year);
            cal.Set(CalendarField.Month, _minDateTime.Month - 1);
            cal.Set(CalendarField.DayOfMonth, _minDateTime.Day);
            cal.Set(CalendarField.HourOfDay, _minDateTime.Hour);
            cal.Set(CalendarField.Minute, _minDateTime.Minute);
            cal.Set(CalendarField.Second, 0);

            datePicker.MinDate = cal;

            var activity = CrossCurrentActivity.Current.Activity as AppCompatActivity;
            datePicker.Show(activity.SupportFragmentManager, "Datepickerdialog");
        }

        private void ShowTimePicker()
        {
            var timePicker = new TimePickerFragment();
            timePicker.Initialize(this, DateTime.Now.Hour, DateTime.Now.Minute, !_timeFormat.Contains("tt"));

            var activity = CrossCurrentActivity.Current.Activity as AppCompatActivity;
            timePicker.Show(activity.SupportFragmentManager, "Timepickerdialog");
        }

        private void OnDateChanged()
        {
            _dateButton.Text = SelectedDate.ToString(_dateFormat);
            _timeButton.Text = SelectedDate.ToString(_timeFormat);

            DateValueChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}