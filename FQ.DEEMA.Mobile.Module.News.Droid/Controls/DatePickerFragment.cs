﻿using Android.App;
using Android.OS;
using Softeq.XToolkit.Common;
using Java.Util;

namespace FQ.DEEMA.Mobile.Module.News.Droid.Controls
{
    public class DatePickerFragment : Android.Support.V4.App.DialogFragment
    {
        private WeakReferenceEx<DatePickerDialog.IOnDateSetListener> _listenerRef;
        private int _year;
        private int _month;
        private int _day;

        public void Initialize(DatePickerDialog.IOnDateSetListener listener, int year, int month, int day)
        {
            _listenerRef = WeakReferenceEx.Create(listener);
            _year = year;
            _month = month;
            _day = day;
        }

        public Calendar MinDate { get; set; }

        public override Dialog OnCreateDialog(Bundle savedInstanceState)
        {
            var dialog = new DatePickerDialog(
                Activity,
               _listenerRef.Target,
               _year,
               _month,
               _day);

            if (MinDate != null)
            {
                dialog.DatePicker.MinDate = MinDate.Time.Time;
            }

            return dialog;
        }
    }
}
