using System;
using System.Collections.Generic;
using System.Windows.Input;
using Android.Content;
using Android.Runtime;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Airbnb.Lottie;
using FQ.DEEMA.Mobile.Module.News.Droid.ViewHolders;
using FQ.DEEMA.Mobile.Module.News.ViewModels;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Bindings.Droid;
using Softeq.XToolkit.Common.Droid.Converters;

namespace FQ.DEEMA.Mobile.Module.News.Droid.Controls
{
    [Register("com.fq.deema.mobile.module.news.droid.NewsView")]
    public class NewsView : RelativeLayout
    {
        private NewsAdapter _adapter;
        private LottieAnimationView _busyAnimationView;
        private View _busyContainer;
        private LottieAnimationView _emptyAnimationView;
        private TextView _emptyText;
        private RecyclerView _listView;
        private ImageButton _newPostButton;
        private SwipeRefreshLayout _swipeRefreshLayout;

        public NewsView(Context context) : base(context)
        {
            Init(context);
        }

        public NewsView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Init(context);
        }

        public NewsView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
            Init(context);
        }

        public NewsView(IntPtr handle, JniHandleOwnership owner) : base(handle, owner)
        {
        }

        private void Init(Context context)
        {
            Inflate(context, Resource.Layout.control_news_view, this);
        }

        public ICommand ScrollToTopCommand { get; private set; }

        public NewsListViewModel ViewModel { get; private set; }

        public string EmptyAnimationName { get; set; }

        public void OnCreate(NewsListViewModel newsListViewModel)
        {
            _listView = FindViewById<RecyclerView>(Resource.Id.control_news_view_list_view);
            _listView.SetLayoutManager(new LinearLayoutManager(Context));

            SetViewModel(newsListViewModel);

            ScrollToTopCommand = new RelayCommand(() =>
            {
                var linearLayoutManager = _listView.GetLayoutManager() as LinearLayoutManager;
                linearLayoutManager?.ScrollToPositionWithOffset(0, 0);
            });

            _swipeRefreshLayout = FindViewById<SwipeRefreshLayout>(Resource.Id.control_news_view_swiperefresh);
            _newPostButton = FindViewById<ImageButton>(Resource.Id.control_news_view_new_post_button);

            _busyAnimationView = FindViewById<LottieAnimationView>(Resource.Id.control_news_view_busy_animation);
            _busyAnimationView.SetAnimation("busy_animation.json");
            _busyAnimationView.SetScaleType(ImageView.ScaleType.FitXy);

            _busyContainer = FindViewById<View>(Resource.Id.control_news_view_busy_animation_container);

            var imageView = FindViewById<ImageView>(Resource.Id.control_news_view_busy_bg);
            imageView.SetImageResource(StyleHelper.Style.NewsFeedPlaceholder);

            _emptyAnimationView =
                FindViewById<LottieAnimationView>(Resource.Id.control_news_view_empty_animation_container);

            _emptyText = FindViewById<TextView>(Resource.Id.control_news_view_empty_text);
            _emptyText.Text = ViewModel.NoNewsText;

            _swipeRefreshLayout.Refresh += OnSwipeRefreshLayoutRefresh;
            _newPostButton.Click += OnNewPostButtonClick;
        }

        public void SetViewModel(NewsListViewModel newsListViewModel)
        {
            ViewModel = newsListViewModel;

            DestroyAdapter();

            _adapter = new NewsAdapter(ViewModel.News, new RelayCommand<NewsHeaderViewModel>(OnItemClick));
            _adapter.SetCommand(nameof(_adapter.LastItemRequested), new RelayCommand(ViewModel.LoadNextPage));
            _adapter.SetCommand(nameof(_adapter.DataReloaded), new RelayCommand(() =>
            {
                var manager = (LinearLayoutManager)_listView.GetLayoutManager();
                manager.ScrollToPositionWithOffset(0, 0);
            }));
            _listView.SetAdapter(_adapter);
        }

        public void OnDestroy()
        {
            DestroyAdapter();

            _swipeRefreshLayout.Refresh -= OnSwipeRefreshLayoutRefresh;
            _newPostButton.Click -= OnNewPostButtonClick;
        }

        private void DestroyAdapter()
        {
            _listView?.SetAdapter(null);
            _adapter?.Dispose();
            _adapter = null;
        }

        public IList<Binding> DoAttachBindings()
        {
            return new List<Binding>
            {
                this.SetBinding(() => ViewModel.HasData).WhenSourceChanges(() =>
                {
                    if (ViewModel.IsBusy)
                    {
                        return;
                    }

                    SetEmptyContainerState(!ViewModel.HasData, EmptyAnimationName);
                }),
                this.SetBinding(() => ViewModel.IsRefreshing).WhenSourceChanges(() =>
                {
                    if (ViewModel.IsRefreshing == false && _swipeRefreshLayout.Refreshing)
                    {
                        _swipeRefreshLayout.Refreshing = false;
                    }
                }),
                this.SetBinding(() => ViewModel.IsBusy).WhenSourceChanges(() =>
                {
                    if (ViewModel.IsBusy)
                    {
                        _busyContainer.Visibility = ViewStates.Visible;
                        _busyAnimationView.CancelAnimation();
                        _busyAnimationView.Progress = 0;
                        _busyAnimationView.PlayAnimation();

                        SetEmptyContainerState(false, EmptyAnimationName);
                    }
                    else
                    {
                        _busyContainer.Visibility = ViewStates.Gone;
                        _busyAnimationView.CancelAnimation();

                        SetEmptyContainerState(!ViewModel.HasData, EmptyAnimationName);
                    }
                }),
                this.SetBinding(() => ViewModel.CanPostNews).WhenSourceChanges(() =>
                {
                    _newPostButton.Visibility = BoolToViewStateConverter.ConvertGone(ViewModel.CanPostNews);
                })
            };
        }

        public void TryScrollToPrevState()
        {
            if (ViewModel.CurrentItem == 0)
            {
                return;
            }

            ViewModel.IsBusy = true;

            _listView.PostDelayed(() =>
            {
                _listView.GetLayoutManager().ScrollToPosition(ViewModel.CurrentItem);
                ViewModel.CurrentItem = 0;
                ViewModel.IsBusy = false;
            }, 300);
        }

        private void OnNewPostButtonClick(object sender, EventArgs e)
        {
            ViewModel.NewPostCommand.Execute(null);
        }

        private void OnSwipeRefreshLayoutRefresh(object sender, EventArgs e)
        {
            ViewModel.RefreshCommand.Execute(null);
        }

        private void OnItemClick(NewsHeaderViewModel newsHeaderViewModel)
        {
            ViewModel.NavigateToDetails(newsHeaderViewModel);
        }

        private void SetEmptyContainerState(bool isEmpty, string animationName)
        {
            if (isEmpty && !string.IsNullOrEmpty(animationName))
            {
                _emptyAnimationView.Visibility = ViewStates.Visible;
                _emptyText.Visibility = ViewStates.Visible;
                _emptyAnimationView.CancelAnimation();
                _emptyAnimationView.SetAnimation(animationName);
                _emptyAnimationView.Progress = 0;
                _emptyAnimationView.PlayAnimation();
            }
            else
            {
                _emptyAnimationView.Visibility = ViewStates.Gone;
                _emptyText.Visibility = ViewStates.Gone;
                _emptyAnimationView.CancelAnimation();
            }
        }

        private class NewsAdapter : ObservableRecyclerViewAdapter<NewsHeaderViewModel>
        {
            private readonly RelayCommand<NewsHeaderViewModel> _navigateToDetailsCommand;

            public NewsAdapter(
                IList<NewsHeaderViewModel> items,
                RelayCommand<NewsHeaderViewModel> navigateToDetailsCommand)
                : base(items, null, (holder, index, item) =>
                {
                    var newsViewHolder = (NewsViewHolder)holder;
                    newsViewHolder.Bind(item);
                })
            {
                _navigateToDetailsCommand = navigateToDetailsCommand;
            }

            public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                var cellView = LayoutInflater.From(parent.Context)
                    .Inflate(Resource.Layout.fragment_news_cell, parent, false);

                return new NewsViewHolder(cellView, _navigateToDetailsCommand);
            }
        }
    }
}