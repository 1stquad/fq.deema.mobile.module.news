﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

using System;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Webkit;
using Android.Widget;
using Android.Views;
using Android.Graphics;
using Java.Interop;
using Softeq.XToolkit.WhiteLabel.Threading;
using System.Text;

namespace FQ.DEEMA.Mobile.Module.News.Droid.Controls
{
    [Register("com.fq.deema.mobile.module.news.droid.LocalWebView")]
    public class LocalWebView : WebView
    {
        public LocalWebView(Context context) : base(context) { }

        public LocalWebView(Context context, IAttributeSet attrs) : base(context, attrs) { }

        public void Init(Func<string, bool> urlLoadingHandler, Action<string> pageFinishedHandler)
        {
            VerticalScrollBarEnabled = false;
            HorizontalScrollBarEnabled = false;

            Settings.JavaScriptEnabled = true;
            Settings.BuiltInZoomControls = true;
            Settings.DisplayZoomControls = false;
            Settings.SetLayoutAlgorithm(WebSettings.LayoutAlgorithm.SingleColumn);

#if DEBUG
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat)
            {
                SetWebContentsDebuggingEnabled(true);
            }
#endif
            SetWebViewClient(new LocalWebViewClient(urlLoadingHandler, pageFinishedHandler));

            AddJavascriptInterface(new JSBridge(ResizeHandler), nameof(JSBridge));
        }

        public void EnableCookies()
        {
            CookieManager.Instance.SetAcceptCookie(true);
            CookieManager.Instance.SetAcceptThirdPartyCookies(this, true);
        }

        private void ResizeHandler(float contentHeight)
        {
            Execute.BeginOnUIThread(() =>
            {
                var webViewHeight = (int)(contentHeight * Resources.DisplayMetrics.Density);

                var oldLayoutParams = (LinearLayout.LayoutParams)LayoutParameters;
                var newLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, webViewHeight);

                newLayoutParams.TopMargin = oldLayoutParams.TopMargin;
                newLayoutParams.RightMargin = oldLayoutParams.RightMargin;
                newLayoutParams.LeftMargin = oldLayoutParams.LeftMargin;
                newLayoutParams.BottomMargin = oldLayoutParams.BottomMargin;
                newLayoutParams.Gravity = oldLayoutParams.Gravity;

                LayoutParameters = newLayoutParams;
            });
        }

        public void SetBodyHtml(string html)
        {
            if (string.IsNullOrEmpty(html))
            {
                return;
            }

            var bytes = Encoding.UTF8.GetBytes(html);
            var encodedHtml = Base64.EncodeToString(bytes, Base64Flags.NoPadding);

            LoadData(encodedHtml, "text/html", "base64");
        }

        #region disable_scroll

        protected override bool OverScrollBy(
            int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX, int scrollRangeY,
            int maxOverScrollX, int maxOverScrollY, bool isTouchEvent)
        {
            return false;
        }

        public override void ScrollTo(int x, int y) { }

        public override void ComputeScroll() { }

        #endregion

        internal class LocalWebViewClient : WebViewClient
        {
            private readonly Func<string, bool> _shouldUrlLoadingHandler;
            private readonly Action<string> _pageFinishedHandler;

            private string _localUrl;

            public LocalWebViewClient(
                Func<string, bool> shouldUrlLoadingHandler,
                Action<string> pageFinishedHandler)
            {
                _shouldUrlLoadingHandler = shouldUrlLoadingHandler;
                _pageFinishedHandler = pageFinishedHandler;
            }

            public override void OnPageStarted(WebView view, string url, Bitmap favicon)
            {
                // HACK YP: for Nexus and other devices, because ShouldOverrideUrlLoading() not called
                // https://stackoverflow.com/q/5129112/5925490

                if (url.StartsWith("http://", StringComparison.CurrentCultureIgnoreCase) ||
                    url.StartsWith("https://", StringComparison.CurrentCultureIgnoreCase))
                {
                    _shouldUrlLoadingHandler(url);

                    view.LoadUrl(_localUrl);

                    return;
                }

                _localUrl = url;

                base.OnPageStarted(view, _localUrl, favicon);
            }

            public override bool ShouldOverrideUrlLoading(WebView view, IWebResourceRequest request)
            {
                var url = request.Url.ToString();

                if (string.IsNullOrEmpty(url))
                {
                    return false;
                }

                return _shouldUrlLoadingHandler(url);
            }

            public override void OnPageFinished(WebView view, string url)
            {
                base.OnPageFinished(view, url);

                view.LoadUrl($"javascript:window.{JSBridge.ResizeFuncName}(document.body.scrollHeight);");

                _pageFinishedHandler?.Invoke(url);
            }
        }

        internal class JSBridge : Java.Lang.Object
        {
            public const string ResizeFuncName = nameof(JSBridge) + "." + nameof(Resize);

            private readonly Action<float> _resizeHandler;

            public JSBridge(Action<float> resizeHandler)
            {
                _resizeHandler = resizeHandler;
            }

            [Export]
            [JavascriptInterface]
            public void Resize(Java.Lang.String height)
            {
                _resizeHandler(float.Parse(height.ToString()));
            }
        }
    }
}
