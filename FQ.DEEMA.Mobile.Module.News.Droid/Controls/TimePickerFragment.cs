using Android.App;
using Android.OS;
using Softeq.XToolkit.Common;
using DialogFragment = Android.Support.V4.App.DialogFragment;

namespace FQ.DEEMA.Mobile.Module.News.Droid.Controls
{
    public class TimePickerFragment : DialogFragment
    {
        private WeakReferenceEx<TimePickerDialog.IOnTimeSetListener> _listenerRef;
        private int _h;
        private int _m;
        private bool _is24format;

        public void Initialize(TimePickerDialog.IOnTimeSetListener listener, int h, int m, bool is24format)
        {
            _listenerRef = WeakReferenceEx.Create(listener);
            _h = h;
            _m = m;
            _is24format = is24format;
        }

        public override Dialog OnCreateDialog(Bundle savedInstanceState)
        {
            return new TimePickerDialog(Context, _listenerRef.Target, _h, _m, _is24format);
        }
    }
}