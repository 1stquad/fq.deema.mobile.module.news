﻿using Android.OS;
using Android.Support.Design.Widget;
using Android.Views;
using FQ.DEEMA.Mobile.Module.News.Droid.Controls;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.ViewModels;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common.Extensions;
using Softeq.XToolkit.WhiteLabel.Droid;
using Softeq.XToolkit.WhiteLabel.Droid.Controls;
using Softeq.XToolkit.WhiteLabel.Droid.Shared.Extensions;
using Softeq.XToolkit.WhiteLabel.Messages;
using Messenger = Softeq.XToolkit.WhiteLabel.Messenger.Messenger;
using Softeq.XToolkit.Bindings.Extensions;

namespace FQ.DEEMA.Mobile.Module.News.Droid.Views
{
    public class NewsFragment : FragmentBase<NewsViewModel>
    {
        private readonly IndexToNewsTypeConverter _indexToNewsTypeConverter = new IndexToNewsTypeConverter();

        private NavigationBarView _navigationBarView;
        private NewsView _newsView;
        private TabLayout _tabLayout;
        private Binding<int, int> _selectedTabBinding;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(Resource.Layout.fragment_news, container, false);
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            _newsView = View.FindViewById<NewsView>(Resource.Id.fragment_news_news_view);
            _newsView.OnCreate(ViewModel.NewsListViewModel);

            _tabLayout = View.FindViewById<TabLayout>(Resource.Id.fragment_news_tab_layout);
            foreach (var label in ViewModel.Labels)
            {
                _tabLayout.AddTab(_tabLayout.NewTab().SetText(label));
            }

            _tabLayout.TabGravity = TabLayout.GravityFill;
            _tabLayout.GetTabAt(_indexToNewsTypeConverter.ConvertValueBack(ViewModel.NewsType)).Select();
            _tabLayout.SetCommand<TabLayout.TabReselectedEventArgs>(nameof(_tabLayout.TabReselected),
                _newsView.ScrollToTopCommand);

            _navigationBarView = View.FindViewById<NavigationBarView>(Resource.Id.fragment_news_navigation_bar);
            _navigationBarView.SetLeftButton(StyleHelper.Style.NavigationLeftMenuIcon, ViewModel.LeftCommand);
            _navigationBarView.SetCenterImage(StyleHelper.Style.NavigationLogoIcon, _newsView.ScrollToTopCommand);
            _navigationBarView.SetBackground(Resource.Color.news_content_color);
            _navigationBarView.SetSettingsButtonAsync(ViewModel.UserName, ViewModel.UserPhotoUrl, ViewModel.RightCommand,
                StyleHelper.Style.ProfileAvatarStyles);

            _newsView.TryScrollToPrevState();
        }

        public override void OnDestroy()
        {
            base.OnDestroy();

            _newsView.OnDestroy();
        }

        protected override void DoAttachBindings()
        {
            base.DoAttachBindings();

            _selectedTabBinding = this.SetBinding(() => _tabLayout.SelectedTabPosition)
                .ObserveSourceEvent<TabLayout.TabSelectedEventArgs>(nameof(_tabLayout.TabSelected))
                .WhenSourceChanges(() =>
                {
                    ViewModel.NewsType = _indexToNewsTypeConverter.ConvertValue(_tabLayout.SelectedTabPosition);
                    _newsView.EmptyAnimationName = ViewModel.NewsType == NewsType.MyNews
                        ? "read_animation.json"
                        : "unread_animation.json";

                    UpdateViewModelForNewsView(ViewModel.NewsListViewModel);
                });

            Messenger.Default.Register<TabReselectedMessage>(this, OnTabReselectedMessage);
        }

        protected override void DoDetachBindings()
        {
            base.DoDetachBindings();

            _selectedTabBinding?.Detach();
            _selectedTabBinding = null;

            Messenger.Default.Unregister(this);
        }

        private void OnTabReselectedMessage(TabReselectedMessage message)
        {
            _newsView.ScrollToTopCommand.Execute(this);
        }

        private void UpdateViewModelForNewsView(NewsListViewModel newsListViewModel)
        {
            _newsView.SetViewModel(newsListViewModel);
            ReAttachBindingsForNewsView();
        }

        private void ReAttachBindingsForNewsView()
        {
            Bindings.DetachAllAndClear();
            Bindings.AddRange(_newsView.DoAttachBindings());
        }
    }
}