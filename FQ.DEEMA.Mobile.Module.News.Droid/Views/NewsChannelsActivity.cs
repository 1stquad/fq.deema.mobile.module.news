﻿using Android.App;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using FQ.DEEMA.Mobile.Module.News.Droid.ViewHolders;
using FQ.DEEMA.Mobile.Module.News.ViewModels;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Bindings.Droid;
using Softeq.XToolkit.WhiteLabel.Droid;
using Softeq.XToolkit.WhiteLabel.Droid.Controls;

namespace FQ.DEEMA.Mobile.Module.News.Droid.Views
{
    [Activity]
    public class NewsChannelsActivity : ActivityBase<NewsChannelsViewModel>
    {
        private ObservableRecyclerViewAdapter<ChannelViewModel> _adapter;
        private NavigationBarView _navigationBarView;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            OverridePendingTransition(Resource.Animation.news_enter_left_to_right,
                Resource.Animation.news_enter_right_to_left);

            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_news_channels);

            _navigationBarView = FindViewById<NavigationBarView>(Resource.Id.activity_news_channels_navigation_bar);
            var recyclerView = FindViewById<RecyclerView>(Resource.Id.activity_news_channels_list_view);

            _navigationBarView.SetLeftButton(StyleHelper.Style.NavigationBarBackButtonIcon, ViewModel.BackCommand);

            recyclerView.SetLayoutManager(new LinearLayoutManager(this));
            _adapter = new ObservableRecyclerViewAdapter<ChannelViewModel>(
                ViewModel.Items,
                (parent, index) =>
                {
                    var view = LayoutInflater.From(parent.Context)
                        .Inflate(Resource.Layout.activity_news_channels_cell, parent, false);
                    return new ChannelViewHolder(view);
                },
                (view, index, item) =>
                {
                    var viewHolder = (ChannelViewHolder)view;
                    viewHolder.Bind(item);
                });
            recyclerView.SetAdapter(_adapter);
        }

        public override void OnBackPressed()
        {
            ViewModel.BackCommand.Execute(this);
        }

        public override void Finish()
        {
            base.Finish();
            OverridePendingTransition(Resource.Animation.news_exit_left_to_right,
                Resource.Animation.news_exit_right_to_left);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            _adapter.Dispose();
        }

        protected override void OnStart()
        {
            base.OnStart();
            
            _adapter?.NotifyDataSetChanged();
        }

        protected override void DoAttachBindings()
        {
            base.DoAttachBindings();

            Bindings.Add(this.SetBinding(() => ViewModel.Title).WhenSourceChanges(() =>
            {
                _navigationBarView.SetTitle(ViewModel.Title);
            }));
        }
    }
}