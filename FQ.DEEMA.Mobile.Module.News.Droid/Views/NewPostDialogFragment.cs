﻿using System;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using FFImageLoading;
using FFImageLoading.Views;
using FQ.DEEMA.Mobile.Module.News.Droid.Controls;
using FQ.DEEMA.Mobile.Module.News.ViewModels;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Common.Droid.Converters;
using Softeq.XToolkit.WhiteLabel;
using Softeq.XToolkit.WhiteLabel.Droid.Controls;
using Softeq.XToolkit.WhiteLabel.Droid.Dialogs;
using Softeq.XToolkit.WhiteLabel.Threading;

namespace FQ.DEEMA.Mobile.Module.News.Droid.Views
{
    public class NewPostDialogFragment : DialogFragmentBase<NewPostViewModel>
    {
        private BusyOverlayView _busyOverlayView;
        private EditText _contentEditText;
        private DateTimeButtonView _fromButton;
        private ImagePicker _imagePicker;
        private ImageViewAsync _imageView;
        private NavigationBarView _navigationBarView;
        private ImageButton _openCameraButton;
        private ImageButton _openGalleryButton;
        private string _previewImageKey;
        private ImageButton _removePhotoButton;
        private SwitchCompat _socialFeaturesSwitchCompat;
        private EditText _titleEditText;
        private DateTimeButtonView _toButton;

        protected override int ThemeId => Resource.Style.CoreDialogTheme;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return LayoutInflater.Inflate(Resource.Layout.dialog_new_post, container, true);
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            _imagePicker = new ImagePicker(ViewModel.PermissionsManager, Dependencies.IocContainer.Resolve<IImagePickerService>());

            _navigationBarView = View.FindViewById<NavigationBarView>(Resource.Id.dialog_new_post_nav_bar);
            _navigationBarView.SetLeftButton(Resource.Drawable.core_ic_close, ViewModel.CancelCommand, Resource.Color.news_button_color);
            _navigationBarView.SetRightButton(StyleHelper.Style.SendIcon,
                new RelayCommand(() => { ViewModel.PostCommand.Execute(_imagePicker.GetStreamFunc()); }));
            _navigationBarView.SetTitle(ViewModel.Header);

            _titleEditText = View.FindViewById<EditText>(Resource.Id.dialog_new_post_title);
            _titleEditText.Hint = ViewModel.NewsLocalizedStrings.NewPostTitlePlaceholder;

            _contentEditText = View.FindViewById<EditText>(Resource.Id.dialog_new_post_content);
            _contentEditText.Hint = ViewModel.NewsLocalizedStrings.NewPostContentPlaceholder;

            _fromButton = View.FindViewById<DateTimeButtonView>(Resource.Id.dialog_new_post_from_button);
            _fromButton.Initialize(
                ViewModel.DateFrom.DateTime,
                ViewModel.NewsLocalizedStrings.DateFrom,
                ViewModel.NewsLocalizedStrings.DateFormatPart);
            _fromButton.SetMinTime(ViewModel.MinDateFrom.DateTime, false);

            _toButton = View.FindViewById<DateTimeButtonView>(Resource.Id.dialog_new_post_to_button);
            _toButton.Initialize(
                ViewModel.DateEnd.DateTime,
                ViewModel.NewsLocalizedStrings.DateTo,
                ViewModel.NewsLocalizedStrings.DateFormatPart);
            _toButton.SetMinTime(ViewModel.MinDateEnd.DateTime, false);

            _openGalleryButton = View.FindViewById<ImageButton>(Resource.Id.dialog_new_post_open_gallery);
            _openGalleryButton.SetImageResource(StyleHelper.Style.AddImageIcon);
            _openGalleryButton.SetCommand(nameof(_openGalleryButton.Click), new RelayCommand(OnOpenPhotosClick));

            _openCameraButton = View.FindViewById<ImageButton>(Resource.Id.dialog_new_post_open_camera);
            _openCameraButton.SetImageResource(StyleHelper.Style.AddPhotoIcon);
            _openCameraButton.SetCommand(nameof(_openCameraButton.Click), new RelayCommand(OnOpenCameraClick));

            _imageView = View.FindViewById<ImageViewAsync>(Resource.Id.dialog_new_post_preview_image);
            _imageView.SetScaleType(ImageView.ScaleType.FitCenter);

            _removePhotoButton = View.FindViewById<ImageButton>(Resource.Id.dialog_new_post_remove_image);
            _removePhotoButton.SetImageResource(StyleHelper.Style.RemoveImageIcon);
            _removePhotoButton.SetCommand(nameof(_removePhotoButton.Click), new RelayCommand(OnRemoveClick));

            _busyOverlayView = View.FindViewById<BusyOverlayView>(Resource.Id.dialog_new_post_busy_overlay);

            var socialFeaturesLabel = View.FindViewById<TextView>(Resource.Id.dialog_new_post_social_features_label);
            socialFeaturesLabel.Text = ViewModel.NewsLocalizedStrings.EnableSocialFeatures;

            _socialFeaturesSwitchCompat = View.FindViewById<SwitchCompat>(Resource.Id.dialog_new_post_social_features_switch);
        }

        protected override void DoAttachBindings()
        {
            base.DoAttachBindings();

            Bindings.Add(this.SetBinding(() => ViewModel.Title, () => _titleEditText.Text, BindingMode.TwoWay));
            Bindings.Add(this.SetBinding(() => ViewModel.Content, () => _contentEditText.Text, BindingMode.TwoWay));

            Bindings.Add(this.SetBinding(() => _fromButton.SelectedDate)
                .ObserveSourceEvent(nameof(_fromButton.DateValueChanged))
                .WhenSourceChanges(() => { _toButton.SetMinTime(_fromButton.SelectedDate, true); }));

            Bindings.Add(this.SetBinding(() => _imagePicker.ViewModel.ImageCacheKey)
                .WhenSourceChanges(SetPreviewImage));

            Bindings.Add(this.SetBinding(() => _fromButton.SelectedDate)
                .ObserveSourceEvent(nameof(_fromButton.DateValueChanged))
                .WhenSourceChanges(() => { ViewModel.DateFrom = new DateTimeOffset(_fromButton.SelectedDate); }));

            Bindings.Add(this.SetBinding(() => _toButton.SelectedDate)
                .ObserveSourceEvent(nameof(_toButton.DateValueChanged))
                .WhenSourceChanges(() => { ViewModel.DateEnd = new DateTimeOffset(_toButton.SelectedDate); }));

            Bindings.Add(this.SetBinding(() => ViewModel.IsBusy).WhenSourceChanges(() =>
            {
                _busyOverlayView.Visibility = BoolToViewStateConverter.ConvertGone(ViewModel.IsBusy);
            }));

            Bindings.Add(
                this.SetBinding(() => ViewModel.SocialFeaturesEnabled, () => _socialFeaturesSwitchCompat.Checked, BindingMode.TwoWay));
        }

        private void OnOpenPhotosClick()
        {
            _imagePicker.OpenGallery();
        }

        private void OnOpenCameraClick()
        {
            _imagePicker.OpenCamera();
        }

        private void SetPreviewImage()
        {
            if (string.IsNullOrEmpty(_imagePicker.ViewModel.ImageCacheKey) && string.IsNullOrEmpty(ViewModel.ImageUrl))
            {
                Execute.BeginOnUIThread(() =>
                {
                    ClearPreviewImage();
                    UpdateImagePreviewState(false);
                });
                return;
            }

            if (_imagePicker.ViewModel.ImageCacheKey != null)
            {
                ViewModel.ImageUrl = null;
                var key = _imagePicker.ViewModel.ImageCacheKey;
                if (key == _previewImageKey)
                {
                    return;
                }

                Execute.BeginOnUIThread(() =>
                {
                    _previewImageKey = key;
                    UpdateImagePreviewState(true);
                    ImageService.Instance
                        .LoadFile(_imagePicker.ViewModel.ImageCacheKey)
                        .DownSampleInDip(60, 60)
                        .Error(e => ClearPreviewImage())
                        .IntoAsync(_imageView);
                });
            }

            if (ViewModel.ImageUrl != null)
            {
                Execute.BeginOnUIThread(() =>
                {
                    var key = ViewModel.ImageUrl;
                    if (key == _previewImageKey)
                    {
                        return;
                    }

                    _previewImageKey = key;
                    UpdateImagePreviewState(true);
                    ImageService.Instance
                        .LoadUrl(ViewModel.ImageUrl)
                        .DownSampleInDip(60, 60)
                        .Error(e => ClearPreviewImage())
                        .IntoAsync(_imageView);
                });
            }
        }

        private void ClearPreviewImage()
        {
            Execute.BeginOnUIThread(() =>
            {
                _previewImageKey = null;
                _imageView.SetImageDrawable(null);
            });
        }

        private void OnRemoveClick()
        {
            _imagePicker.ViewModel.ImageCacheKey = null;
            ViewModel.ImageUrl = null;
            SetPreviewImage();
        }

        private void UpdateImagePreviewState(bool isVisible)
        {
            _imageView.Visibility = BoolToViewStateConverter.ConvertInvisible(isVisible);
            _removePhotoButton.Visibility = BoolToViewStateConverter.ConvertInvisible(isVisible);
        }
    }
}