﻿using Android.App;
using Android.OS;
using FQ.DEEMA.Mobile.Module.News.Droid.Controls;
using FQ.DEEMA.Mobile.Module.News.ViewModels;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.WhiteLabel.Droid;
using Softeq.XToolkit.WhiteLabel.Droid.Controls;
using Softeq.XToolkit.WhiteLabel.Droid.Extensions;

namespace FQ.DEEMA.Mobile.Module.News.Droid.Views
{
    [Activity]
    public class NewsChannelActivity : ActivityBase<NewsChannelViewModel>
    {
        private NavigationBarView _navigationBarView;
        private NewsView _newsView;

        public override void OnBackPressed()
        {
            ViewModel.BackCommand.Execute(this);
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_news_channel);

            _navigationBarView = FindViewById<NavigationBarView>(Resource.Id.activity_news_channel_navigation_bar);
            _navigationBarView.SetLeftButton(StyleHelper.Style.NavigationBarBackButtonIcon, ViewModel.BackCommand);
            _navigationBarView.SetRightButton(string.Empty, ViewModel.SubscribeCommand);
            _navigationBarView.SetTitle(ViewModel.Title);

            _newsView = FindViewById<NewsView>(Resource.Id.activity_news_channel_news_view);
            _newsView.OnCreate(ViewModel.NewsListViewModel);
            _newsView.EmptyAnimationName = "unread_animation.json";
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            _newsView.OnDestroy();
        }

        protected override void DoAttachBindings()
        {
            base.DoAttachBindings();

            Bindings.AddRange(_newsView.DoAttachBindings());
            Bindings.Add(this.SetBinding(() => ViewModel.SubscribeActionText, () => _navigationBarView.RightTextButton.Text));
            Bindings.Add(this.SetBinding(() => ViewModel.IsUnSubscribeAvailable).WhenSourceChanges(() =>
            {
                var color = ViewModel.IsUnSubscribeAvailable
                    ? Resource.Color.news_accent_color
                    : Resource.Color.news_inactive_color;
                _navigationBarView.RightTextButton.SetTextColor(_navigationBarView.GetColor(color));
                _navigationBarView.RightTextButton.Enabled = ViewModel.IsUnSubscribeAvailable;
            }));
        }
    }
}