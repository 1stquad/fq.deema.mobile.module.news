﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.Widget.Helper;
using Android.Views;
using Android.Widget;
using Com.Airbnb.Lottie;
using FFImageLoading;
using FFImageLoading.Views;
using FQ.DEEMA.Mobile.Module.News.Droid.ViewHolders;
using FQ.DEEMA.Mobile.Module.News.Messages;
using FQ.DEEMA.Mobile.Module.News.Models;
using FQ.DEEMA.Mobile.Module.News.ViewModels.NewsDetails;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Bindings.Droid;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Common.Droid.Converters;
using Softeq.XToolkit.WhiteLabel;
using Softeq.XToolkit.WhiteLabel.Droid;
using Softeq.XToolkit.WhiteLabel.Droid.Controls;
using Softeq.XToolkit.WhiteLabel.Droid.Services;
using Softeq.XToolkit.WhiteLabel.Threading;
using Messenger = Softeq.XToolkit.WhiteLabel.Messenger.Messenger;

namespace FQ.DEEMA.Mobile.Module.News.Droid.Views.NewsDetails
{
    [Activity]
    public class NewsDetailsPageActivity : ActivityBase<NewsDetailsPageViewModel, INewsDetailsPageViewModel>
    {
        private NewsDetailsAdapter _adapter;
        private ImageButton _addImageButton;
        private ImagePicker _imagePicker;
        private View _imagePreviewContainer;
        private EditText _messageText;
        private NavigationBarView _navigationBarView;
        private string _previewImageKey;
        private ImageViewAsync _previewImageViewAsync;
        private RecyclerView _recyclerView;
        private ImageButton _removeImageButton;
        private ImageButton _takePhotoButton;
        private ImageButton _sendButton;
        private SwipeRefreshLayout _swipeRefreshLayout;
        private View _busyView;
        private LottieAnimationView _busyAnimation;
        private LinearLayout _emptyPlaceholder;
        private LinearLayout _inputContainer;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_news_details);

            _adapter = new NewsDetailsAdapter(ViewModel.Items, ViewModel.NewsLocalizedStrings);

            _recyclerView = FindViewById<RecyclerView>(Resource.Id.activity_news_details_list_view);
            _recyclerView.SetLayoutManager(new NewsLinearLayoutManager(this));
            _recyclerView.SetAdapter(_adapter);

            var newsDetailsSimpleCallback = new ItemTouchHelper(new NewsDetailsSimpleCallback(ViewModel.Items));
            newsDetailsSimpleCallback.AttachToRecyclerView(_recyclerView);

            _navigationBarView = FindViewById<NavigationBarView>(Resource.Id.activity_news_details_navigation_bar);
            _navigationBarView.SetLeftButton(StyleHelper.Style.NavigationBarBackButtonIcon, new RelayCommand(HideKeyboardAndGoBack));
            _navigationBarView.SetRightButton(Resource.Drawable.news_ic_more, new RelayCommand(OpenMoreContextMenu));

            _swipeRefreshLayout = FindViewById<SwipeRefreshLayout>(Resource.Id.activity_news_details_swiperefresh);
            _swipeRefreshLayout.SetCommand(nameof(_swipeRefreshLayout.Refresh), ViewModel.RefreshCommand);

            _messageText = FindViewById<EditText>(Resource.Id.activity_news_details_edit_text);
            _messageText.Hint = ViewModel.NewsLocalizedStrings.CommentPlaceholder;

            _sendButton = FindViewById<ImageButton>(Resource.Id.activity_news_details_send_button);
            _sendButton.SetImageResource(StyleHelper.Style.SendIcon);
            _sendButton.SetCommand(nameof(_sendButton.Click),
                new RelayCommand(() => { ViewModel.AddCommentCommand.Execute(_imagePicker.GetPickerData()); }));

            _imagePicker = new ImagePicker(ViewModel.PermissionsManager, Dependencies.IocContainer.Resolve<IImagePickerService>());
            _imagePreviewContainer = FindViewById<View>(Resource.Id.activity_news_details_image_preview_container);
            _previewImageViewAsync = FindViewById<ImageViewAsync>(Resource.Id.activity_news_details_preview_image);

            _removeImageButton = FindViewById<ImageButton>(Resource.Id.activity_news_details_remove_image_button);
            _removeImageButton.SetImageResource(StyleHelper.Style.RemoveImageIcon);
            _removeImageButton.SetCommand(nameof(_removeImageButton.Click), new RelayCommand(OnRemoveClick));

            _addImageButton = FindViewById<ImageButton>(Resource.Id.activity_news_details_add_image_button);
            _addImageButton.SetImageResource(StyleHelper.Style.AddImageIcon);
            _addImageButton.SetCommand(nameof(_addImageButton.Click), new RelayCommand(OnOpenPhotosClick));

            _takePhotoButton = FindViewById<ImageButton>(Resource.Id.activity_news_details_take_photo_button);
            _takePhotoButton.SetImageResource(StyleHelper.Style.AddPhotoIcon);
            _takePhotoButton.SetCommand(nameof(_takePhotoButton.Click), new RelayCommand(OnTakePhotoClick));

            ViewModel.SetEditModeCommand = new RelayCommand<bool>(OnSetEditMode);
            ViewModel.ItemAddedCommand = new RelayCommand<int>(OnItemAdded);
            ViewModel.ClearInputCommand = new RelayCommand(() =>
            {
                Execute.BeginOnUIThread(() =>
                {
                    ViewModel.InputText = null;
                    OnRemoveClick();
                });
            });

            _busyView = FindViewById<View>(Resource.Id.activity_news_details_busy_container);

            _busyAnimation = FindViewById<LottieAnimationView>(Resource.Id.activity_news_details_busy_animation);
            _busyAnimation.SetAnimation("busy_animation.json");
            _busyAnimation.SetScaleType(ImageView.ScaleType.FitXy);

            var imageView = FindViewById<ImageView>(Resource.Id.activity_news_details_busy_placeholder);
            imageView.SetImageResource(StyleHelper.Style.DetailsPlaceholder);

            _inputContainer = FindViewById<LinearLayout>(Resource.Id.activity_news_details_input_container);
            _emptyPlaceholder = FindViewById<LinearLayout>(Resource.Id.activity_news_details_empty_placeholder);

            var emptyTextView = FindViewById<TextView>(Resource.Id.activity_news_details_no_data_text);
            emptyTextView.Text = ViewModel.NewsLocalizedStrings.EmptyArticle;
        }

        protected override void DoAttachBindings()
        {
            base.DoAttachBindings();

            _recyclerView.LayoutChange += OnListLayoutChanged;

            Bindings.Add(this.SetBinding(() => ViewModel.InputText, () => _messageText.Text, BindingMode.TwoWay));
            Bindings.Add(this.SetBinding(() => ViewModel.IsRefreshing).WhenSourceChanges(() =>
            {
                if (ViewModel.IsRefreshing == false && _swipeRefreshLayout.Refreshing)
                {
                    _swipeRefreshLayout.Refreshing = false;
                }
            }));
            Bindings.Add(this.SetBinding(() => ViewModel.IsArticleLoaded, () => _navigationBarView.RightImageButton.Visibility)
                .ConvertSourceToTarget(BoolToViewStateConverter.ConvertGone));

            Bindings.Add(this.SetBinding(() => _imagePicker.ViewModel.ImageCacheKey)
                .WhenSourceChanges(SetPreviewImage));

            Bindings.Add(this.SetBinding(() => ViewModel.IsBusy).WhenSourceChanges(() =>
            {
                if (ViewModel.IsBusy)
                {
                    _busyView.Visibility = ViewStates.Visible;
                    _busyAnimation.Progress = 0;
                    _busyAnimation.PlayAnimation();
                }
                else
                {
                    _busyView.Visibility = ViewStates.Gone;
                    _busyAnimation.CancelAnimation();
                }
            }));

            Bindings.Add(this.SetBinding(() => ViewModel.ChannelName).WhenSourceChanges(() =>
            {
                _navigationBarView.SetTitle(ViewModel.ChannelName);
            }));

            Bindings.Add(this.SetBinding(() => ViewModel.IsArticleEmpty).WhenSourceChanges(() =>
            {
                var emptyPlaceholderVisible = ViewModel.IsArticleEmpty;

                _emptyPlaceholder.Visibility = BoolToViewStateConverter.ConvertGone(emptyPlaceholderVisible);
                _swipeRefreshLayout.Visibility = BoolToViewStateConverter.ConvertGone(!emptyPlaceholderVisible);
                _inputContainer.Visibility = BoolToViewStateConverter.ConvertGone(!emptyPlaceholderVisible);
            }));

            Bindings.Add(this.SetBinding(() => ViewModel.IsCommentsEnabled).WhenSourceChanges(() =>
            {
                switch (ViewModel.IsCommentsEnabled)
                {
                    case null:
                        _inputContainer.Visibility = ViewStates.Invisible;
                        break;
                    case true:
                        _inputContainer.Visibility = ViewStates.Visible;
                        break;
                    case false:
                        _inputContainer.Visibility = ViewStates.Gone;
                        break;
                }
            }));
        }

        protected override void DoDetachBindings()
        {
            base.DoDetachBindings();

            _recyclerView.LayoutChange -= OnListLayoutChanged;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            _adapter.Dispose();
        }

        private void OnListLayoutChanged(object sender, View.LayoutChangeEventArgs e)
        {
            if (e.Bottom < e.OldBottom)
            {
                _recyclerView.PostDelayed(() => { _recyclerView.ScrollBy(0, e.OldBottom - e.Bottom); }, 100);
            }
        }

        private void OnItemAdded(int position)
        {
            _recyclerView.GetLayoutManager().ScrollToPosition(position);
        }

        private void OnSetEditMode(bool enabled)
        {
            if (enabled)
            {
                KeyboardService.ShowSoftKeyboard(_messageText);

                var text = _messageText.Text;
                if (!string.IsNullOrEmpty(text))
                {
                    _messageText.SetSelection(text.Length);
                }
            }
            else
            {
                KeyboardService.HideSoftKeyboard(_messageText);
                _messageText.ClearFocus();
            }
        }

        private void OnTakePhotoClick()
        {
            _imagePicker.OpenCamera();
        }

        private void OnOpenPhotosClick()
        {
            _imagePicker.OpenGallery();
        }

        private void SetPreviewImage()
        {
            if (string.IsNullOrEmpty(_imagePicker.ViewModel.ImageCacheKey))
            {
                Execute.BeginOnUIThread(() =>
                {
                    ClearPreviewImage();
                    UpdateImagePreviewState(false);
                });
                return;
            }

            if (_imagePicker.ViewModel.ImageCacheKey != null)
            {
                var key = _imagePicker.ViewModel.ImageCacheKey;
                if (key == _previewImageKey)
                {
                    return;
                }

                Execute.BeginOnUIThread(() =>
                {
                    _previewImageKey = key;
                    UpdateImagePreviewState(true);

                    ImageService.Instance
                        .LoadFile(_imagePicker.ViewModel.ImageCacheKey)
                        .DownSampleInDip(60, 60)
                        .Error(e => ClearPreviewImage())
                        .IntoAsync(_previewImageViewAsync);
                });
            }
        }

        private void ClearPreviewImage()
        {
            Execute.BeginOnUIThread(() =>
            {
                _previewImageKey = null;
                _previewImageViewAsync.SetImageDrawable(null);
            });
        }

        private void OnRemoveClick()
        {
            _imagePicker.ViewModel.ImageCacheKey = null;
            SetPreviewImage();
        }

        private void UpdateImagePreviewState(bool isVisible)
        {
            _imagePreviewContainer.Visibility = BoolToViewStateConverter.ConvertGone(isVisible);
        }

        private void HideKeyboardAndGoBack()
        {
            KeyboardService.HideSoftKeyboard(_messageText);
            ViewModel.BackCommand.Execute(this);
        }

        private void OpenMoreContextMenu()
        {
            if (ViewModel.MoreActions == null)
            {
                return;
            }

            new DroidContextMenuComponent(ViewModel.MoreActions)
                .BuildMenu(_navigationBarView.Context, _navigationBarView.RightTextButton)
                .Show();
        }

        private class NewsDetailsAdapter : ObservableRecyclerViewAdapter<IArticleItem>
        {
            private readonly INewsLocalizedStrings _newsLocalizedStrings;

            public NewsDetailsAdapter(
                IList<IArticleItem> items,
                INewsLocalizedStrings newsLocalizedStrings)
                : base(items, null, null)
            {
                _newsLocalizedStrings = newsLocalizedStrings;

                ShouldNotifyByAction = true;
            }

            public override int GetItemViewType(int position)
            {
                var item = DataSource[position];
                return (int)item.NewsDetailsType;
            }

            public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                var type = (ArticleItemType)viewType;
                switch (type)
                {
                    case ArticleItemType.Body:
                        var bodyView = LayoutInflater.From(parent.Context)
                            .Inflate(Resource.Layout.activity_news_details_body_cell, parent, false);
                        return new BodyViewHolder(bodyView, _newsLocalizedStrings);
                    case ArticleItemType.Comment:
                        var commentView = LayoutInflater.From(parent.Context)
                            .Inflate(Resource.Layout.activity_news_details_comment_cell, parent, false);
                        return new CommentViewHolder(commentView, _newsLocalizedStrings);
                    case ArticleItemType.Reply:
                        var replyView = LayoutInflater.From(parent.Context)
                            .Inflate(Resource.Layout.activity_news_details_reply_cell, parent, false);
                        return new ReplyViewHolder(replyView, _newsLocalizedStrings);
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                var item = DataSource[position];
                var type = item.NewsDetailsType;
                switch (type)
                {
                    case ArticleItemType.Body:
                        var bodyViewHolder = (BodyViewHolder)holder;
                        bodyViewHolder.Bind(item as BodyViewModel);
                        break;
                    case ArticleItemType.Comment:
                        var commentViewHolder = (CommentViewHolder)holder;
                        commentViewHolder.Bind(item as CommentViewModel);
                        break;
                    case ArticleItemType.Reply:
                        var replyViewHolder = (ReplyViewHolder)holder;
                        replyViewHolder.Bind(item as ReplyViewModel);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private class NewsLinearLayoutManager : LinearLayoutManager
        {
            public NewsLinearLayoutManager(Context context) : base(context)
            {
            }

            public override bool RequestChildRectangleOnScreen(RecyclerView parent, View child, Rect rect,
                                                               bool immediate, bool focusedChildVisible)
            {
                return false;
            }
        }

        private class NewsDetailsSimpleCallback : ItemTouchHelper.SimpleCallback
        {
            private readonly IList<IArticleItem> _items;

            public NewsDetailsSimpleCallback(IList<IArticleItem> items) : base(0, ItemTouchHelper.Left)
            {
                _items = items;
            }

            public override int GetSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder)
            {
                var position = viewHolder.AdapterPosition;
                var isEditable = _items[position].CanDelete;

                return isEditable ? base.GetSwipeDirs(recyclerView, viewHolder) : 0;
            }

            public override bool OnMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                RecyclerView.ViewHolder target)
            {
                return false;
            }

            public override void OnSwiped(RecyclerView.ViewHolder viewHolder, int direction)
            {
                var view = GetView(viewHolder);
                if (view != null)
                {
                    Messenger.Default.Send(new DeleteCommentMessage(viewHolder.AdapterPosition));
                }
            }

            public override void ClearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder)
            {
                var view = GetView(viewHolder);
                if (view != null)
                {
                    DefaultUIUtil.ClearView(view);
                }
            }

            public override void OnChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                float dX, float dY, int actionState, bool isCurrentlyActive)
            {
                var view = GetView(viewHolder);
                if (view != null)
                {
                    DefaultUIUtil.OnDraw(c, recyclerView, view, dX, dY, actionState, isCurrentlyActive);
                }
            }

            private View GetView(RecyclerView.ViewHolder viewHolder)
            {
                View view = null;

                if (viewHolder is CommentViewHolder commentViewHolder)
                {
                    view = commentViewHolder.ForegroundView;
                }

                if (viewHolder is ReplyViewHolder replyViewHolder)
                {
                    view = replyViewHolder.ForegroundView;
                }

                return view;
            }
        }
    }
}