﻿using Android.Views;
using Android.OS;
using Android.Graphics;
using Softeq.XToolkit.WhiteLabel.Droid.Dialogs;
using FQ.DEEMA.Mobile.Module.News.Droid.Controls;
using FQ.DEEMA.Mobile.Module.News.ViewModels;

namespace FQ.DEEMA.Mobile.Module.News.Droid.Views
{
    public class VideoPlayerDialogFragment : DialogFragmentBase<VideoPlayerViewModel>
    {
        private LocalWebView _player;

        protected override int ThemeId => Resource.Style.NewsVideoPlayerTheme;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return LayoutInflater.Inflate(Resource.Layout.dialog_video_player, container, true);
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            _player = view.FindViewById<LocalWebView>(Resource.Id.dialog_video_player_player);
            _player.Init(x =>
            {
                return true;
            },
            x =>
            {
            });
            _player.EnableCookies();

            _player.SetBodyHtml(ViewModel.VideoPlayerModel.PlayerPageHtml);
        }

        public override void OnResume()
        {
            StretchDialogByWidth();

            base.OnResume();
        }

        private void StretchDialogByWidth()
        {
            var window = Dialog.Window;
            var display = window.WindowManager.DefaultDisplay;
            var size = new Point();

            display.GetSize(size);
            window.SetLayout(size.X, ViewGroup.LayoutParams.WrapContent);
            window.SetGravity(GravityFlags.Center);
        }
    }
}
