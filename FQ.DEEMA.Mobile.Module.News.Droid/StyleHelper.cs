﻿using System;
using Softeq.XToolkit.WhiteLabel;
using Softeq.XToolkit.WhiteLabel.Droid.Controls;

namespace FQ.DEEMA.Mobile.Module.News.Droid
{
    internal static class StyleHelper
    {
        private static readonly Lazy<INewsDroidStyle> StyleLazy = Dependencies.IocContainer.LazyResolve<INewsDroidStyle>();

        public static INewsDroidStyle Style => StyleLazy.Value;
    }

    public interface INewsDroidStyle
    {
        int NavigationBarBackButtonIcon { get; }
        int NavigationLeftMenuIcon { get; }
        int NavigationLogoIcon { get; }

        int AddImageIcon { get; }
        int SendIcon { get; }
        int AddPhotoIcon { get; }
        int RemoveImageIcon { get; }
        int NewsFeedPlaceholder { get; }
        int DetailsPlaceholder { get; }

        AvatarPlaceholderDrawable.AvatarStyles AvatarStyles { get; }
        AvatarPlaceholderDrawable.AvatarStyles ProfileAvatarStyles { get; }
    }
}
