﻿using Autofac;

namespace FQ.DEEMA.Mobile.Module.News.Droid
{
    public static class BootstrapperDroid
    {
        public static void Configure(ContainerBuilder containerBuilder)
        {
            Bootstrapper.Configure(containerBuilder);
        }
    }
}