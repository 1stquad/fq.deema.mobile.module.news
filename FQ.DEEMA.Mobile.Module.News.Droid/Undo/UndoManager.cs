using System;
using System.Threading;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.WhiteLabel.Droid;
using Softeq.XToolkit.WhiteLabel.Droid.Services;
using Softeq.XToolkit.WhiteLabel.Interfaces;
using Softeq.XToolkit.WhiteLabel.Mvvm;
using Softeq.XToolkit.WhiteLabel.Services;

namespace FQ.DEEMA.Mobile.Module.News.Droid.Undo
{
    public class UndoManager : IUndoManager
    {
        private readonly DroidToastService _droidToastService;

        public UndoManager(DroidToastService droidToastService)
        {
            _droidToastService = droidToastService;
        }

        public void AddOperation(
            Action<CancellationTokenSource> undoAction,
            Action<CancellationToken> applyAction,
            string infoLabel,
            string actionLabel,
            CancellationTokenSource cancellationTokenSource)
        {
            var model = new ToastModel
            {
                Label = infoLabel,
                CommandAction = new CommandAction
                {
                    Title = actionLabel,
                    Command = new RelayCommand(() => { undoAction(cancellationTokenSource); })
                },
                AltCommandAction = new CommandAction
                {
                    Title = actionLabel,
                    Command = new RelayCommand(() => { applyAction(cancellationTokenSource.Token); })
                }
            };

            _droidToastService.Enqueue(model);
        }
    }
}